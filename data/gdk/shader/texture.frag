#version 450

layout(rgba16f) uniform readonly image2D u_render;
// uniform sampler2D u_render;
uniform ivec2 u_viewport_offset;

out vec3 color;
in vec2 tex_coord;

void main() {
    ivec2 coord = ivec2(gl_FragCoord.xy) - u_viewport_offset;
    color = imageLoad(u_render, coord).rgb;
}