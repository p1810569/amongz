#version 450

in vec4 p3d_Vertex;
flat out uint v_id;

uniform uint u_instance_offset;
uniform usamplerBuffer u_ids;

void main() {
    
    // we need to update this light's transform in the buffer
    // if (gl_VertexID == 0)
    //     light_datas[u_id].transform = inverse(p3d_ModelViewMatrix);
    // gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
    
    // v_id = texelFetch(u_ids, int(gl_InstanceID + u_instance_offset)).r;

    gl_Position = p3d_Vertex;
}