#version 450

uniform mat4 p3d_ModelViewProjectionMatrix;
in vec2 p3d_MultiTexCoord0;
in vec4 p3d_Vertex;
out vec2 tex_coord;

void main() {
  // gl_Position = vec4(p3d_Vertex.xy, 1.0, 1.0);
  gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
  tex_coord = p3d_MultiTexCoord0;
}