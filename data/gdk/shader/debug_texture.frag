#version 450


coherent readonly layout(r32ui) uniform uimage2D u_counter;
coherent readonly layout(r32ui) uniform uimage2DArray u_tile_id;
coherent readonly layout(r32f) uniform image2DArray u_tile_min;
coherent readonly layout(r32f) uniform image2DArray u_tile_max;

// uniform usampler2D render;
uniform ivec2 u_viewport_offset;

out vec3 color;
in vec2 tex_coord;

void main() {
    ivec2 coord = ivec2(gl_FragCoord.xy) / 16;
    // color = vec3(imageLoad(render, coord/16).rgb)*0.3;
    uint count = imageLoad(u_counter, coord).r;

    for (int i=0; i<1; ++i) {
        ivec3 coord3 = ivec3(coord, i);

        uint tile_index = uint(imageLoad(u_tile_id, coord3).r);

        // if (tile_index == 0) {
            float min_data = imageLoad(u_tile_min, coord3).r;
            float max_data = imageLoad(u_tile_max, coord3).r;


            // float min_depth = intBitsToFloat(min_data);
            // float max_depth = intBitsToFloat(max_data);
            color = vec3(min_data*0.3, max_data*0.3, 0);
        // }
    }
    color = vec3(0,0,float(count));
}