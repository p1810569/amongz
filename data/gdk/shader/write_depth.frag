#version 450

layout(r32f) uniform image2D u_input_depth;
uniform vec2 u_near_far;

out vec4 output_depth;

#pragma include "transforms.inc.glsl"


void main() {

    float z = imageLoad(u_input_depth, ivec2(gl_FragCoord.xy)).r;
    output_depth.r = delinearize_depth(z, u_near_far.x, u_near_far.y);
}