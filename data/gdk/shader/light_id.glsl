#version 450

// uniform uint u_id;
// layout(rgba8) uniform writeonly coherent image2D u_output;
in vec4 v_aabb;
in vec4 v_pos;
out vec3 color;


void main() {


    vec2 pos = (vec2(gl_FragCoord.xy) / 1024) * 2.0 - 1.0;

    // vec2 pos = v_pos.xy / v_pos.w;
	if (pos.x < v_aabb.x || pos.y < v_aabb.y || pos.x > v_aabb.z || pos.y > v_aabb.w)
	{
		discard;

	} 
    color = vec3(1,1,1);
        // imageStore(u_output, ivec2(gl_FragCoord.xy), data);
}