sync-video true
# show-frame-rate-meter true
want-pstats false
pstats-gpu-timing 1
want-directtools #t
framebuffer-multisample #f
multisamples 0
clock-mode limited
clock-frame-rate 60
load-file-type p3assimp
framebuffer-stencil true
#threading-model Cull/Draw
show-buffers #t
hardware-animated-vertices true
bullet-filter-algorithm groups-mask
textures-power-2 none

texture-anisotropic-degree 16

# bullet-filter-algorithm callback
# bullet-enable-contact-events true

assert-abort #f
gl-check-errors #t
gl-debug #t
gl-debug-abort-level error
gl-enable-memory-barriers 1

gl-version 4 6

preload-textures #t
texture-minfilter mipmap

# notify-level-Interval spam

lock-to-one-cpu 0
support-threads 1

# notify-level-dgraph spam

# notify-level-glgsg spam
# notify-level debug

# notify-level-chan debug

# default-directnotify-level spam

# notify-level-event spam
# notify-level-device spam

load-display pandagl
framebuffer-hardware #t
framebuffer-software #f
basic-shaders-only #f