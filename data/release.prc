clock-frame-rate 60
sync-video #t
# framebuffer-multisample 0
# multisamples 0
# framebuffer-stencil true
# threading-model cull/draw
pstats-gpu-timing #t
want-pstats #t
# show-buffers #f
# audio-library-name p3fmod_audio
# notify-level-fmodAudio debug
bullet-filter-algorithm groups-mask
textures-power-2 none

texture-anisotropic-degree 16
hardware-animated-vertices true

assert-abort #t
gl-check-errors #t
gl-debug #t
gl-debug-abort-level error
gl-enable-memory-barriers 1

gl-version 4 6

# notify-level-glgsg spam
# notify-level debug

# default-directnotify-level spam
# model-path data/model/