#version 450

uniform sampler2D color_buffer;
// uniform sampler2D depth_buffer;

// Input from vertex shader
in vec2 texcoord;
out vec4 fragcolor;

void main() {
  vec4 color = texture(color_buffer, texcoord);
  fragcolor = vec4(color.r, 0.0, 0.3, 1.0);
}