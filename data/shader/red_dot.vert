#version 450

uniform mat4 p3d_ModelViewProjectionMatrix;
uniform mat4 p3d_ModelViewMatrix;
uniform mat3 p3d_NormalMatrix;

in vec4 p3d_Vertex;
in vec2 p3d_MultiTexCoord0;
in vec3 p3d_Normal;

out vec2 texcoord;
out vec2 crosshair_coord;
out vec3 normal;

void main() {

    gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
    texcoord = p3d_MultiTexCoord0;

    vec4 coord = p3d_ModelViewMatrix * p3d_Vertex;
    vec3 dir = normalize(coord.xyz) * mat3(p3d_ModelViewMatrix);
    crosshair_coord = dir.xz;
}