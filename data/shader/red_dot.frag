#version 450

uniform sampler2D u_crosshair;
uniform vec4 u_color;

in vec2 texcoord;
in vec2 crosshair_coord;
in vec3 normal;

layout(location=1) out vec3 fragcolor;

void main() {
    vec2 coord = crosshair_coord * 35.0 + 0.5;
    float value = texture(u_crosshair, coord).r;
    fragcolor = value * u_color.rgb;
}