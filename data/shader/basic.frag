#version 450

layout(location=0) out vec4 diffuse_matID;
layout(location=1) out vec4 spec_rough;
layout(location=2) out vec4 ssc_trans;
layout(location=3) out vec4 acc_ao;
layout(location=4) out vec4 normal_vel;

uniform sampler2D p3d_Texture0;
in vec2 texcoord;

// out vec4 color;

void main() {

    acc_ao = vec4(texture(p3d_Texture0, texcoord).rgb, 1.0);
}