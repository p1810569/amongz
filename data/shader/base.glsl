#version 430

// Set the number of invocations in the work group.
// In this case, we operate on the image in 16x16 pixel tiles.
layout(local_size_x = 16, local_size_y = 16) in;

// Declare the texture inputs
layout(binding=0, rgba8) uniform coherent image2D color;
uniform sampler2D depth;


void main() {
  // Acquire the coordinates to the texel we are to process.
  ivec2 texelCoords = ivec2(gl_GlobalInvocationID.xy);

  // Read the pixel from the first texture.
  vec4 pixel = imageLoad(color, texelCoords);


  // Now write the modified pixel to the second texture.
  imageStore(color, texelCoords, vec4(pixel.r, 0.0, 0.0, 1.0));
}