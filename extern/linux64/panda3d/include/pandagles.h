#ifndef _METALIB_INIT_pandagles
#define _METALIB_INIT_pandagles

#include "dtoolbase.h"

IMPORT_CLASS void init_libpandagles();

extern "C" IMPORT_CLASS int get_pipe_type_pandagles();

#endif
