// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <animBundleNode.h>
#include <texturePool.h>

#include "eventAnim.h"
#include "redDotSightFormater.h"


using namespace std;


void RedDotSightFormater::fillin(const AssetData& data) {

    Sight::_data->_bg_fov = data.get_float("background_fov", 65.0);
    Sight::_data->_fg_fov = data.get_float("foreground_fov", 70.0);
    Sight::_data->_height = data.get_float("height");
    
    load_textures();

    string tex_name = data.get_string("default_texture");
    
    _data->_default_color = data.get_color("default_color");
    _data->_default_texture = get_crosshair_texture(tex_name);

    NodePath np(this);
    _lens_np = np.find("**/lens");
}


void RedDotSightFormater::register_formater() {
    TypeHandle type = RedDotSight::get_class_type();
    FormaterManager::get_factory()->register_factory(type, make);
}


PandaNode* RedDotSightFormater::make(const FactoryParams& params) {

    return new RedDotSightFormater;
}