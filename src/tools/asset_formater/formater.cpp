// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <bamFile.h>
#include <nodePath.h>

#include "assetManager.h"
#include "defines.h"
#include "formater.h"

using namespace std;

Singleton<FormaterFactory> FormaterManager::_formaters;
DEFINE_TYPEHANDLE(FormaterParam)


bool FormaterManager::format_asset(const string& source_file) {

    Loader* loader = Loader::get_global_ptr();

    PT(PandaNode) root_node = loader->load_sync(source_file);

    if (root_node == nullptr) {
        nout << "error while loading source file " << source_file << endl;
        return false;
    }

    if (root_node->get_num_children() == 0) {
        nout << "source asset is empty" << endl;
        return false;
    }

    NodePath np(root_node);

    NodePath root_override = np.find("root");
    
    if (!root_override.is_empty()) {
        root_override.detach_node();
        NodePathCollection children = np.get_children();

        for (int i=0; i<children.get_num_paths(); ++i) {
            NodePath path = children.get_path(i);
            path.reparent_to(root_override);
        }
        root_override.reparent_to(np);
    }

    NodePathCollection paths = np.find_all_matches("**/=class");
    FormaterFactory* factory = get_factory();

    for (size_t i=0; i<paths.size(); ++i) {
        NodePath path = paths.get_path(i);

        string class_name = path.get_tag("class");
        path.clear_tag("class");

        FactoryParams params;
        params.add_param(new FormaterParam(path.node()));
        
        if (TypeRegistry::ptr()->find_type(class_name) == TypeHandle::none())
            continue;
        
        PandaNode* node = factory->make_instance(class_name, params);

        if (node == nullptr) {
            nout << "no formater found for class " << class_name << endl;
            continue;
        }
        node->ref();
        node->set_name(path.get_name());
        node->replace_node(path.node());
        node->unref();
    }

    return write(root_node->get_child(0), source_file);
}


bool FormaterManager::write(PandaNode* node, const string& source_path) {

    string location = AssetManager::get_location(node->get_type());

    Filename path(source_path);
    path.set_dirname(location);
    path.set_extension("bam");

    BamFile file;
    bool success = false;

    if (!file.open_write(path))
        cerr << "could not open file " << path << endl;
    else {
        file.get_writer()->set_root_node(node);

        if (!file.write_object(node))
            cerr << "could not write asset" << endl;
        else
            success = true;

        file.close();
    }
    return success;
}


FormaterFactory* FormaterManager::get_factory() {
    
    if (!_formaters)
        _formaters = new FormaterFactory;
    
    return _formaters;
}


string AssetData::get_string(const string& name, 
                            const string& default_value) const
{    
    if (!_node->has_tag(name))
        return default_value;
    
    _to_remove.insert(name);

    return _node->get_tag(name);
}


int AssetData::get_int(const string& name, int default_value) const {
    
    if (!_node->has_tag(name))
        return default_value;

    _to_remove.insert(name);

    return stoi(_node->get_tag(name));
}


float AssetData::get_float(const string& name, float default_value) const {
    
    if (!_node->has_tag(name))
        return default_value;
    
    _to_remove.insert(name);

    return stof(_node->get_tag(name));
}


void AssetData::clear_tags() {

    for (const string& tag: _to_remove)
        _node->clear_tag(tag);    
}


FormaterParam::FormaterParam(PandaNode* node):
    _node(node)
{
    
}


PandaNode* FormaterParam::get_node() const {
    return _node;
}


Color AssetData::get_color(const string& name, Color default_value) const {

    if (!_node->has_tag(name))
        return default_value;

    string tag = _node->get_tag(name);
    _node->clear_tag(name);

    string token;
    Color color;
    uint8_t count = 0;

    if (tag[0] != '[' || tag.back() != ']')
        return default_value;

    tag = tag.substr(1, tag.size() - 2);
    istringstream ss(tag);

    while(getline(ss, token, ',') && count < 4)
        color[count++] = stof(token);
    
    return color;
}


AssetData::AssetData(PandaNode* node): _node(node) {
    
}
