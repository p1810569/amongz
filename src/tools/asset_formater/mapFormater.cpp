// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "defines.h"
#include "bulletWorld.h"
#include "bulletRigidBodyNode.h"
#include "zombie.h"
#include "spawn.h"
#include "mapFormater.h"


using namespace std;


void MapFormater::fillin(const AssetData& data) {

    NodePathCollection objects = find_all_matches("**/+BulletBodyNode");

    for (int i=0; i<objects.get_num_paths(); ++i) {

        NodePath np = objects.get_path(i);
        BulletBodyNode* node = DCAST(BulletBodyNode, np.node());

        if (node->is_of_type(BulletRigidBodyNode::get_class_type())) {
    
            BulletRigidBodyNode* body = DCAST(BulletRigidBodyNode, node);
            body->set_mass(0.0);
            body->set_gravity(0.0);
            body->set_into_collide_mask(CollideMask::bit(DEFAULT_COLLISION_MASK));
        }
    }

    NodePathCollection spawns = find_all_matches("**/+Spawn");
    
    for (int i=0; i<spawns.get_num_paths(); ++i) {
        NodePath np = spawns.get_path(i);
        Spawn* spawn = DCAST(Spawn, np.node());

        TypeHandle spawn_type = spawn->get_spawn_type();
        TypeHandle zombie_type = Zombie::get_class_type();
        
        if (spawn_type == zombie_type || spawn_type.is_derived_from(zombie_type))
            _spawns.push_back(spawn);
    }
}


void MapFormater::register_formater() {
    
    TypeHandle type = Map::get_class_type();
    FormaterManager::get_factory()->register_factory(type, make);
}



PandaNode* MapFormater::make(const FactoryParams& params) {
    
    return new MapFormater;
}
