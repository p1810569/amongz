// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <animBundleNode.h>

#include "eventAnim.h"
#include "ammoData.h"
#include "magFormater.h"


using namespace std;


void MagFormater::fillin(const AssetData& data) {

    _data->_ammo = data.get_string("ammo");
    _data->_capacity = data.get_float("capacity");
}

void MagFormater::register_formater() {
    
    TypeHandle type = Mag::get_class_type();
    FormaterManager::get_factory()->register_factory(type, make);
}


PandaNode* MagFormater::make(const FactoryParams& params) {

    return new MagFormater;
}
