// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <animBundleNode.h>

#include "eventAnim.h"
#include "ammoData.h"
#include "gunFormater.h"


using namespace std;



Gun::FireMode get_fire_mode(string name) {

    string delim = "/";
    size_t start = 0;
    size_t end;

    int fire_mode = Gun::FM_bolt_action;

    do {
        end = name.find(delim, start);
        string mode = name.substr(start, end - start);
        start = end + 1;
    
        if (mode == "bolt_action") {
            if (fire_mode != Gun::FM_bolt_action) {
                cerr << "cannot combine bolt-action mode with other modes" 
                    << endl;
                exit(EXIT_FAILURE); 
            }
            break;
        } 
        else if (mode == "semi") fire_mode |= Gun::FM_semi_auto;
        else if (mode == "full") fire_mode |= Gun::FM_full_auto;
        else if (mode == "burst") fire_mode |= Gun::FM_burst;
                    
    } while (end != string::npos);

    return static_cast<Gun::FireMode>(fire_mode);
}


Gun::Class get_gun_class(string name) {

    if      (name == "handgun")         return Gun::C_handgun;
    else if (name == "assault")         return Gun::C_assault;
    else if (name == "submachine_gun")  return Gun::C_submachine_gun;
    else if (name == "lightmachine_gun")return Gun::C_lightmachine_gun;
    else if (name == "sniper")          return Gun::C_sniper;
    else if (name == "launcher")        return Gun::C_launcher;
    else if (name == "shotgun")         return Gun::C_shotgun;

    return Gun::C_unknown;
}


class AnimWriter: private EventAnim {

    typedef map<string, double> Tags;

    AnimWriter(AnimBundle* bundle, const Tags& tags): 
            EventAnim(bundle, "events")
    {
        for (Tags::const_iterator it=tags.begin(); it != tags.end(); ++it)
            _tags.push_back({it->first, it->second});
    }
    
    friend class GunFormater;
};


void GunFormater::fillin(const AssetData& data) {

    NodePath viewarms = NodePath(this).find("**/viewarms");
    viewarms.remove_node();

    _data->_class           = get_gun_class(data.get_string("gun_class"));
    _data->_mass            = data.get_float("mass");
    _data->_recoil          = data.get_float("recoil");
    _data->_muzzle_velocity = data.get_float("muzzle_velocity");
    _data->_fire_rate       = data.get_float("fire_rate");
    _data->_fire_mode       = ::get_fire_mode(data.get_string("fire_modes"));
    _data->_ammo_data       = AmmoData::get_ammo_data(data.get_string("ammo"));

    format_anims(data);
}


void GunFormater::format_anims(const AssetData& data) {
    
    NodePath np(this);
    NodePathCollection anims = np.find_all_matches("**/-AnimBundleNode");

    float reload_timestamp = data.get_float("reload_timestamp");

    for (size_t i=0; i<anims.size(); ++i) {

        NodePath path = anims.get_path(i);
        AnimBundleNode* node = DCAST(AnimBundleNode, path.node());
        
        map<string, double> tags;
        string anim_name = node->get_bundle()->get_name();

        if (anim_name == "reload" || anim_name == "reload_empty")
            tags["reload"] = reload_timestamp;

        AnimWriter* writer = new AnimWriter(node->get_bundle(), tags);

        _data->_anims.push_back(node->get_bundle());
        path.detach_node();
    }
}


void GunFormater::register_formater() {
    
    TypeHandle type = Gun::get_class_type();
    FormaterManager::get_factory()->register_factory(type, make);
}


PandaNode* GunFormater::make(const FactoryParams& params) {
    
    return new GunFormater;
}
