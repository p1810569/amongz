// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "spawnFormater.h"


using namespace std;


void SpawnFormater::fillin(const AssetData& data) {

    TypeRegistry* registry = TypeRegistry::ptr();
    _type = registry->find_type(data.get_string("type"));
}


void SpawnFormater::register_formater() {
    
    TypeHandle type = Spawn::get_class_type();
    FormaterManager::get_factory()->register_factory(type, make);
}


TypeHandle SpawnFormater::get_spawn_type() const {
    
    TypeRegistry* registry = TypeRegistry::ptr();
    return registry->find_type(get_data().get_string("type"));
}



PandaNode* SpawnFormater::make(const FactoryParams& params) {
    
    return new SpawnFormater;
}
