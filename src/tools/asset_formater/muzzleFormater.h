// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MUZZLEFORMATER_H__
#define __MUZZLEFORMATER_H__

#include "formater.h"
#include "muzzle.h"


class MuzzleFormater: private Formater<Muzzle> {

public:
    ~MuzzleFormater() = default;

    virtual void fillin(const AssetData& data) override;

    static void register_formater();

private:
    static PandaNode* make(const FactoryParams& params);
    MuzzleFormater() = default;
};

#endif // __MUZZLEFORMATER_H__