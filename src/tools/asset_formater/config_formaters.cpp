// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <load_prc_file.h>
#include <dconfig.h>

#include "gunFormater.h"
#include "anchorFormater.h"
#include "barrelFormater.h"
#include "magFormater.h"
#include "gripFormater.h"
#include "chargingHandleFormater.h"
#include "suppressorFormater.h"
#include "stockFormater.h"
#include "sightFormater.h"
#include "redDotSightFormater.h"
#include "config_game.h"
#include "zombieFormater.h"
#include "hitBoxFormater.h"
#include "spawnFormater.h"
#include "mapFormater.h"
#include "config_formaters.h"

using namespace std;


ConfigureDef(config_formaters);

ConfigureFn(config_formaters) {
    init_formaters();
}


void init_formaters() {

    static bool initialized = false;
    if (initialized)
        return;

    initialized = true;
    
    load_prc_file_data("", "assert-abort true");

    init_game();

    FormaterParam::init_type();
    
    GunFormater::register_formater();
    AnchorFormater::register_formater();
    BarrelFormater::register_formater();
    MagFormater::register_formater();
    SuppressorFormater::register_formater();
    MuzzleFormater::register_formater();
    StockFormater::register_formater();
    GripFormater::register_formater();
    ChargingHandleFormater::register_formater();
    SightFormater::register_formater();
    RedDotSightFormater::register_formater();
    ZombieFormater::register_formater();
    HitBoxFormater::register_formater();
    SpawnFormater::register_formater();
    MapFormater::register_formater();
}


