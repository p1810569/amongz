// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <loader.h>
#include <bulletWorld.h>

#include <rescaleNormalAttrib.h>
#include <shadeModelAttrib.h>
#include <bulletPlaneShape.h>
#include <bulletPersistentManifold.h>
#include <bulletTickCallbackData.h>
#include <virtualFileSystem.h>
#include <aiWorld.h>
#include <cardMaker.h>


#include "playerControllerInput.h"
#include "playerController.h"
#include "collisionHandler.h"
#include "player.h"
#include "gun.h"
#include "barrel.h"
#include "mag.h"
#include "stock.h"
#include "grip.h"
#include "ammoData.h"
#include "animBundle.h"
#include "chargingHandle.h"
#include "suppressor.h"
#include "redDotSight.h"
#include "ammoPack.h"
#include "defines.h"
#include "callbacks.h"
#include "spawn.h"
#include "zombieAIController.h"
#include "render_utils.h"
#include "map.h"

using namespace std;

DEFINE_TYPEHANDLE(Map)



Map::Map(const string& name): 
    LocalNodePath(this),
    PandaNode(name),
    _physic_world(new BulletWorld()),
    _ai_world(new AIWorld(*this))
{

    load_collide_mask();
    load_zombies();
}


Map::~Map() {

    delete _ai_world;
}


void Map::init() {

    _physic_world->set_gravity(0,0,-GRAVITY);
    _physic_world->set_tick_callback(new CollisionHandler(_physic_world));
    
    AsyncTaskManager* mgr = AsyncTaskManager::get_global_ptr();

    AsyncTask* physic = do_add_task(named_method(update_physic), 40);
    AsyncTask* ai     = do_add_task(named_method(update_ai), 30);
    AsyncTask* spawn  = do_add_task(named_method(spawn_zombies), 0);

#if THREADED_PHYSIC
    AsyncTaskChain* physic_chain = mgr->make_task_chain("physic");
 	physic_chain->set_num_threads(1);
	physic_chain->set_frame_sync(true);
    physic->set_task_chain("physic");
#endif

#if THREADED_AI
    AsyncTaskChain* ai_chain = mgr->make_task_chain("ai");
 	ai_chain->set_num_threads(1);
	ai_chain->set_frame_sync(true);
    ai->set_task_chain("ai");
#endif

    add_controller(make_controller("player"));
}


void Map::add_controller(PlayerController* controller) {
    
    nassertv(!has_controller(controller))

    _controllers.push_back(controller);

    _physic_world->attach_character(controller);
    controller->set_pos(0,0,1);
    attach_new_node(controller);
}


void Map::remove_controller(PlayerController* player) {

    auto found = std::find(_controllers.begin(), _controllers.end(), player);

    if (found != _controllers.end())
        _controllers.erase(found);    
}


void Map::remove_controller(size_t id) {
    
    nassertv(id < _controllers.size())

    _controllers.erase(_controllers.begin() + id);
}


bool Map::has_controller(PlayerController* controller) const {
    
    auto it = std::find(_controllers.begin(), _controllers.end(), controller);
    return it != _controllers.end();    
}


PlayerController* Map::make_controller(const std::string& name) {

    PlayerController* controller = new PlayerControllerInput(name);

    // give the player a default gun
    PT(Gun) gun = make_asset<Gun>("m4a1");

    gun->set_fire_mode(Gun::FM_full_auto);

    gun->attach(make_asset<Barrel>("m4a1_barrel_short"));
    gun->attach(make_asset<Mag>("mag_stanag"));
    gun->attach(make_asset<Stock>("m4a1_stock"));
    gun->attach(make_asset<Grip>("vertical_grip"));
    gun->attach(make_asset<ChargingHandle>("ar15_raptor"));
    gun->attach(make_asset<Suppressor>("socom_monster"));
    gun->attach(make_asset<RedDotSight>("holo_sight_eotech"));
    
    attach_new_node(gun);

    // give the player some ammos
    controller->loot(new AmmoPack("5.56", 200));
    gun->set_num_rounds(gun->get_max_rounds());

    do_task_later([controller, gun](AsyncTask*) {
        controller->loot(gun);
        return AsyncTask::DS_done;

    }, "loot", 0.0);

    return controller;
}


size_t Map::get_num_controller() const {
    
    return _controllers.size();
}


PlayerController* Map::get_controller(size_t id) const {

    nassertr(id < _controllers.size(), nullptr)
    return _controllers[id];
}


BulletWorld* Map::get_physic_world() const {
    
    return _physic_world;
}


AIWorld* Map::get_ai_world() const {
    
    return _ai_world;
}


Map* Map::load(const std::string& name) {

    PandaNode* root = AssetManager::get_asset(Map::get_class_type(), name);
    return DCAST(Map, root);
}


void Map::load_collide_mask() {

    _physic_world->set_group_collision_flag(DEFAULT_COLLISION_MASK,
                                            DEFAULT_COLLISION_MASK, false);

    _physic_world->set_group_collision_flag(PLAYER_COLLISION_MASK,
                                            DEFAULT_COLLISION_MASK, true);

    _physic_world->set_group_collision_flag(HITBOX_COLLISION_MASK,
                                            HITBOX_COLLISION_MASK, false);

    _physic_world->set_group_collision_flag(HITBOX_COLLISION_MASK,
                                            BULLET_COLLISION_MASK, true);
    // bullets can collide with anything
    for (int i=0; i<32; ++i)
        _physic_world->set_group_collision_flag(BULLET_COLLISION_MASK, i, true);
}



AsyncTask::DoneStatus Map::update_physic(AsyncTask *task) {

    ClockObject* clock = ClockObject::get_global_clock();

    _physic_world->do_physics(clock->get_dt(), 10);

    return AsyncTask::DS_cont;
}


AsyncTask::DoneStatus Map::update_ai(AsyncTask *task) {

    _ai_world->update();
    
    return AsyncTask::DS_cont;    
}


AsyncTask::DoneStatus Map::spawn_zombies(AsyncTask *task) {

    task->set_delay(lerp(15.0f, 25.0f, (float)rand() / (float)RAND_MAX));

    if (_spawns.size() == 0)
        return AsyncTask::DS_again;
    
    int n = rand() % _spawns.size();

    for (int i=0; i<=n; ++i) {
        Spawn* spawn_point = _spawns[rand() % _spawns.size()];
        string name = _zombies[rand() % _zombies.size()];

        FactoryParams params;
        params.add_param(new ZombieAIController::ZombieParam(name));
        
        PandaNode* node = spawn_point->spawn(params);
        
        if (node != nullptr)
            attach_new_node(node);
    }

    return AsyncTask::DS_again;    
}


void Map::load_zombies() {

    Filename path(ZOMBIE_PATH);

    VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();
    PT(VirtualFileList) files = vfs->scan_directory(path);

    if (files == nullptr)
        return;

    for (size_t i=0; i<files->get_num_files(); ++i) {
        
        VirtualFile* file = files->get_file(i);
        Filename filename = file->get_filename();
        _zombies.push_back(filename.get_basename_wo_extension());
    }
}


void Map::register_with_read_factory() {
    
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}


void Map::fillin(DatagramIterator& scan, BamReader* manager) {
    
    PandaNode::fillin(scan, manager);

    // _num_bodies = scan.get_uint32();
    // manager->read_pointers(scan, _num_bodies);

    int num_spawn = scan.get_uint32();

    for (int i=0; i<num_spawn; ++i) {
        manager->read_pointer(scan);
        _spawns.push_back(nullptr);
    }
}


void Map::write_datagram(BamWriter* manager, Datagram& me) {
    
    PandaNode::write_datagram(manager, me);
    
    // int num_char = _physic_world->get_num_characters();
    // int num_rigi = _physic_world->get_num_rigid_bodies();
    // int num_soft = _physic_world->get_num_soft_bodies();
    // int num_ghos = _physic_world->get_num_ghosts();

    // me.add_uint32(num_char + num_rigi + num_soft + num_ghos);

    // for (int i=0; i<num_char; ++i)
    //     manager->write_pointer(me, _physic_world->get_character(i));

    // for (int i=0; i<num_rigi; ++i)
    //     manager->write_pointer(me, _physic_world->get_rigid_body(i));

    // for (int i=0; i<num_soft; ++i)
    //     manager->write_pointer(me, _physic_world->get_soft_body(i));

    // for (int i=0; i<num_ghos; ++i)
    //     manager->write_pointer(me, _physic_world->get_ghost(i));


    me.add_uint32(_spawns.size());

    for (int i=0; i<_spawns.size(); ++i)
        manager->write_pointer(me, _spawns[i]);
}


int Map::complete_pointers(TypedWritable** p_list, BamReader* reader) {
    
    int n = PandaNode::complete_pointers(p_list, reader);

    // for (int i=0; i<_num_bodies; ++i) {
    //     BulletBodyNode* body = DCAST(BulletBodyNode, p_list[n++]);
    //     _physic_world->attach(body);
    // }

    ZombieSpawns::iterator it;
    for (it = _spawns.begin(); it != _spawns.end(); ++it)
        (*it) = DCAST(Spawn, p_list[n++]);
    
    return n;
}


void Map::finalize(BamReader* reader) {
    
    PandaNode::finalize(reader);

    NodePathCollection objects = find_all_matches("**/+BulletBodyNode");

    for (int i=0; i<objects.get_num_paths(); ++i) {
        NodePath np = objects.get_path(i);
        _physic_world->attach(DCAST(BulletBodyNode, np.node()));
    }
}


TypedWritable* Map::make_from_bam(const FactoryParams& params) {

    Map* me = new Map;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);
    manager->register_finalize(me);

    return me;
}
