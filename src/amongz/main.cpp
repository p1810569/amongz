// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <load_prc_file.h>  // for load_prc_file
#include <stdlib.h>                 // for EXIT_SUCCESS
#include "config.h"                 // for COMMON_PRC
#include "framework.h"              // for start_pstats
#include "game.h"                   // for Game
#include "config_game.h"


int main(int argc, char* argv[]) {

#ifndef NDEBUG
    start_pstats();
#endif

#ifdef COMMON_PRC
    load_prc_file(COMMON_PRC);
#endif

#ifdef LINK_STATIC
    init_game();
#endif

    Game* game = Game::get_global_ptr();

    game->main_loop();
    
    return game->finish();
}