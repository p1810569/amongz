// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <eventHandler.h>
#include <dataNode.h>
#include <virtualFileSystem.h>
#include <shaderAttrib.h>
#include <inputDeviceManager.h>
#include <shadeModelAttrib.h>
#include <pointLightNode.h>
#include <callbacks.h>
#include <throw_event.h>
#include <dataGraphTraverser.h>
#include <bulletWorld.h>
#include <asyncTask.h>
#include <cIntervalManager.h>

#include "color.h"
#include "collisionHandler.h"
#include "viewportManager.h"
#include "config.h"
#include "testMap.h"
#include "game.h"

using namespace std;


Singleton<Game> Game::_global_ptr;



Game* Game::get_global_ptr() {

    if (!_global_ptr) {
        _global_ptr = new Game;
        _global_ptr->setup();
    }

    return _global_ptr;
}


Game::ExitStatus Game::finish() {
    
    ExitStatus status = Framework::finish();
    _global_ptr.release();

    return status;
}


GraphicsWindow* Game::get_window() const {
    
    return _window;
}



Game::Game():
    _event_handler(EventHandler::get_global_event_handler()),
    _data_root(new DataNode("root"))
{   
    nassertv(_global_ptr == nullptr)

    _audio_mgr = AudioManager::create_AudioManager();
}


AsyncTask::DoneStatus Game::process_garbage_collect(AsyncTask *task) {

    TransformState::garbage_collect();
    RenderState::garbage_collect();

    return AsyncTask::DS_cont;
}


Game::~Game() {


    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();

    engine->remove_all_windows();
    _event_handler->remove_all_hooks();

    delete _event_handler;

    if (_viewport_mgr != nullptr)
        delete _viewport_mgr;
}


void Game::close_window() {

    if (_window != nullptr) {

        GraphicsEngine* engine = GraphicsEngine::get_global_ptr();
        engine->remove_window(_window);
    }
}


DataNode* Game::get_data_root() const {

    return _data_root;
}


ViewportManager* Game::get_viewport_manager() const {
    
    return _viewport_mgr;
}


void Game::process_window_event(const Event* event) {

    if (event->get_num_parameters() == 1) {

        EventParameter param = event->get_parameter(0);
        const GraphicsOutput *window;
        DCAST_INTO_V(window, param.get_ptr());

        if (window != _window)
            return;
        
        if (!window->is_valid()) {
            close_window();
            set_exit_status(ES_success);
            AsyncTaskManager::get_global_ptr()->stop_threads();
        } else
            _viewport_mgr->update_layout();
    }
}


Map* Game::get_current_map() {

    return _current_map;
}


void Game::setup() {

    FrameBufferProperties fb_prop = FrameBufferProperties::get_default();
    fb_prop.set_rgb_color(true);
    fb_prop.set_stencil_bits(8);
    fb_prop.set_stereo(false);
    fb_prop.set_accum_bits(0);
    fb_prop.set_multisamples(0);
    
    WindowProperties win_prop = WindowProperties::get_default();
    win_prop.set_title("AmongZ");
    win_prop.set_size(1280,720);
    win_prop.set_fullscreen(false);
    // win_prop.set_undecorated(true);
    // win_prop.set_origin(0,0);
    // win_prop.set_maximized(true);
    // win_prop.set_foreground(true);
    // win_prop.set_z_order(WindowProperties::Z_top);

    _window = DCAST(GraphicsWindow, 
                    open_window(fb_prop, win_prop, "main-window"));


    if (_window == nullptr) {
        set_exit_status(ES_failure);
        return;
    }

    _window->set_clear_depth_active(false);
    _window->set_clear_stencil_active(false);
    _window->set_clear_color_active(true);
    _window->set_clear_color(Color::black());


    Loader* loader = Loader::get_global_ptr();

    _viewport_mgr = new ViewportManager(_window, ViewportManager::LP_column);
    _viewport_mgr->set_margin(8);


    register_hook(callback(process_window_event), "window-event");


    if (garbage_collect_states)
        do_add_task(named_method(process_garbage_collect), 46);

    do_add_task(named_method(process_input),   -30);
    do_add_task(named_method(process_data),    -20);
    do_add_task(named_method(process_event),   -10);
    do_add_task(named_method(process_interval), 0);
    do_add_task(named_method(process_audio),    30);
    do_add_task(named_method(process_render),   50);


    _current_map = Map::load("dead-end");

    if (_current_map != nullptr) {
        _current_map->init();
        _current_map->prepare_scene(_window->get_gsg());
    }
}


BulletWorld* get_physic_world() {

    Map* map = get_current_map();
    return map->get_physic_world();
}


AIWorld* get_ai_world() {

    Map* map = get_current_map();
    return map->get_ai_world();
}

Map* get_current_map() {
    
    Game* game = Game::get_global_ptr();
    return game->get_current_map();
}


AsyncTask::DoneStatus Game::process_audio(AsyncTask* task) {


    ClockObject* clock = ClockObject::get_global_clock();
    // double value = clock->get_real_time()*100;

    // PT(FilterProperties) filter = new FilterProperties();
    // filter->add_lowpass(value, 0.5);
    // filter->add_pitchshift(0.5,1.0,0.1);
    // self->_audio_mgr->configure_filters(filter);
    return AsyncTask::DS_cont;
}


AsyncTask::DoneStatus Game::process_event(AsyncTask *task) {

    throw_event("NewFrame");
    _event_handler->process_events();

    return AsyncTask::DS_cont;
}


AsyncTask::DoneStatus Game::process_interval(AsyncTask *task) {

    CIntervalManager* mgr = CIntervalManager::get_global_ptr();
    mgr->step();

    return AsyncTask::DS_cont;
}


AsyncTask::DoneStatus Game::process_render(AsyncTask *task) {

    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();

    if (engine != nullptr) {
        engine->render_frame();
        return AsyncTask::DS_cont;
    }
    return AsyncTask::DS_done;
}


AsyncTask::DoneStatus Game::process_data(AsyncTask *task) {

    DataGraphTraverser traverser;
    traverser.traverse(_data_root);

    return AsyncTask::DS_cont;
}


AsyncTask::DoneStatus Game::process_input(AsyncTask *task) {

    InputDeviceManager* mgr = InputDeviceManager::get_global_ptr();

    mgr->update(); 

    return AsyncTask::DS_cont;
}