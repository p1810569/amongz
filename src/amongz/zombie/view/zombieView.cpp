// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include <animControlCollection.h>  // for AnimControlCollection
#include <loader.h>                 // for Loader
#include <bulletCapsuleShape.h>
#include <bulletRigidBodyNode.h>
#include <eventAnimControl.h>
#include <bulletWorld.h>
#include <cIntervalManager.h>

#include "game.h"
#include "map.h"
#include "zombie.h"
#include "render_utils.h"
#include "zombieView.h"


using namespace std;

DEFINE_TYPEHANDLE(ZombieView)


ZombieView::ZombieView(Zombie* zombie):
    _zombie(zombie),
    _mgr(new CIntervalManager())
{    

}

ZombieView::~ZombieView() {
    
    delete _mgr;
}


void ZombieView::setup() {
    
    // enable GPU skinning

    NodePathCollection paths = _zombie->find_all_matches("**/-GeomNode");


    for (int i=0; i<paths.get_num_paths(); ++i) {
        NodePath path = paths.get_path(i);

        path.set_shader(Shader::load(SHADER_TYPE,
                SHADER_PATH VERT_SHADER("skinning"),
                SHADER_PATH FRAG_SHADER("basic")), 20);

        PandaNode* node = path.node();

        const RenderAttrib* attrib = 
            node->get_attrib(ShaderAttrib::get_class_slot());
        
        node->set_attrib(DCAST(ShaderAttrib, attrib)->set_flag(
                            ShaderAttrib::F_hardware_skinning, true));
    }



    int flags = PartGroup::HMF_ok_wrong_root_name | 
                PartGroup::HMF_ok_part_extra | 
                PartGroup::HMF_ok_anim_extra; 

    static vector<string> anims = {"zombie_walk"};

    NodePath rig_np = _zombie->find("**/+Character");

    if (!rig_np.is_empty()) {

        _character = DCAST(Character, rig_np.node());

        PartBundle* bundle = _character->get_bundle(0);
        
        for (const string& anim_name: anims) {
            AnimBundle* anim = _zombie->get_anim(anim_name);

            PT(EventAnimControl) control = 
                new EventAnimControl(anim_name, bundle, _mgr);

            PartSubset subset;

            if (bundle->do_bind_anim(control, anim, flags, subset))
                _controls.store_anim(control, anim_name);
        }
    }

    _controls.loop("zombie_walk", true);
}


void ZombieView::update() {
    
    _mgr->step();
}