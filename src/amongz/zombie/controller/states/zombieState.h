// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __ZOMBIESTATE_H__
#define __ZOMBIESTATE_H__


#include "state.h"
#include "zombie.h"

class ZombieController;

/**
 * @brief Classe abstraite d'un état d'un zombie
 * @remark Cette classe n'est pas ensée être instantié directement.
 * Chaque état doit hériter de cette classe.
 */
class ZombieState: public State {

protected:
    /**
     * @param name : nom de l'état
     * @param zombie : pointeur sur le zombie qui sera contrôlé par cet etat
    */
    ZombieState(const std::string& name, ZombieController* controller);
    virtual ~ZombieState() = default;

    ZombieController* _controller;

};
#endif // __ZOMBIESTATE_H__