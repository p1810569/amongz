// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <dconfig.h>
#include <virtualFileSystem.h>
#include <panda.h>

#include "defines.h"
#include "assetManager.h"
#include "config_weapon.h"
#include "config_gdk.h"
#include "playerView.h"
#include "config_bullet.h"
#include "playerControllerInput.h"
#include "config_game.h"
#include "hitBox.h"
#include "zombieView.h"
#include "zombieController.h"
#include "zombie.h"
#include "map.h"
#include "physic_utils.h"
#include "spawn.h"
#include "zombieAIController.h"


using namespace std;


ConfigureDef(config_game);

ConfigureFn(config_game) {
    init_game();
}


void init_game() {

    static bool initialized = false;
    if (initialized)
        return;

    initialized = true;
    
    init_libpanda();
    init_libbullet();
    init_gdk();
    init_weapon();

    PlayerView::init_type();
    PlayerControllerInput::init_type();
    HitBox::init_type();
    ZombieView::init_type();
    ZombieController::init_type();
    Zombie::init_type();
    ZombieAIController::init_type();
    Map::init_type();
    Spawn::init_type();

    Zombie::register_with_read_factory();
    HitBox::register_with_read_factory();
    Spawn::register_with_read_factory();
    Map::register_with_read_factory();
    ZombieAIController::register_factory();


    VirtualFileSystem* vfs = VirtualFileSystem::get_global_ptr();

    Filename data_path = ExecutionEnvironment::get_cwd() / DATA_PATH;
    Filename gdk_path = ExecutionEnvironment::get_cwd() / GDK_INSTALL_DIR;

    vfs->mount(data_path, "/", VirtualFileSystem::MF_read_only);
    vfs->mount(gdk_path, "/gdk", VirtualFileSystem::MF_read_only);


    AssetManager::set_default_location(MODEL_PATH);
    AssetManager::set_location(Zombie::get_class_type(), ZOMBIE_PATH);
    AssetManager::set_location(Map::get_class_type(), MAP_PATH);
}