// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <bulletMultiSphereShape.h>
#include <bulletCapsuleShape.h>
#include <bulletRigidBodyNode.h>
#include <eventHandler.h>
#include <eventQueue.h>

#include "game.h"
#include "map.h"
#include "utils.h"
#include "fsm.h"
#include "playerControllerInput.h"
#include "gun.h"
#include "melee.h"
#include "playerView.h"
#include "lvector2.h"
#include "config_weapon.h"
#include "ammoData.h"
#include "player.h"


using namespace std;

DEFINE_TYPEHANDLE(Player)


NameUniquifier Player::_names;


Player::Player(const string& name): 
    BulletCharacterControllerNode(make_shape(), 0.4f, add_name(name).c_str()),
    LocalNodePath(this),
    _ads_value(0.0f),
    _is_fire(false)
{   
    set_max_jump_height(PLAYER_MAX_JUMP_HEIGHT);
    set_jump_speed(PLAYER_JUMP_SPEED);

    // set_gravity(0.0);

    set_collide_mask(CollideMask::bit(PLAYER_COLLISION_MASK));

    _queue = new EventQueue();
    _handler = new EventHandler(_queue);
}


Player::~Player() {

    delete _handler;
    delete _queue;
}


void Player::set_name(const string& name) {
    
    BulletCharacterControllerNode::set_name(add_name(name));
}

float Player::get_ads() const {
    return _ads_value;
}

void Player::set_ads(float value) {
    _ads_value = value;
}

bool Player::get_fire() const {
    return _is_fire;
}

LVector3 Player::get_motion_delta() {
    return get_pos() - _prev_pos;
}

LVector3 Player::get_requested_motion() {
    return _req_motion;
}

EventHandler* Player::get_event_handler() const {
    return _handler;    
}

EventQueue* Player::get_event_queue() const {
    return _queue;
}


Weapon* Player::get_weapon(WeaponSlot slot) const {
    
    if (slot == WS_none)
        slot = _current_weapon_slot;

    switch (slot) {
        case WS_primary:
            return _primary_weapon;
        case WS_secondary:
            return _secondary_weapon;
        case WS_melee:
            return _melee_weapon;
        case WS_none:
            break;
    };
    return nullptr;
}


size_t Player::get_ammo(const string& name) const {
    
    Ammos::const_iterator found = _ammos.find(name);

    if (found != _ammos.end())
        return found->second;
    
    return 0;
}


size_t Player::get_ammo(AmmoData* ammo) const {
    return get_ammo(ammo->get_name());
}

Player::WeaponSlot Player::get_current_weapon() const {
    return _current_weapon_slot;
}

Gun* Player::get_primary_weapon() const {
    return _primary_weapon;
}

Gun* Player::get_secondary_weapon() const {
    return _secondary_weapon;
}

Melee* Player::get_melee_weapon() const {
    return _melee_weapon;
}

string Player::add_name(const string& name) {
    return _names.add_name(name);
}


BulletShape* Player::make_shape() {

    // return new BulletCapsuleShape(PLAYER_RADIUS, 
    //                 PLAYER_HEIGHT - PLAYER_RADIUS*2.0f);
    PTA_LVecBase3 points;
    PTA_float radius(2, PLAYER_RADIUS);

    points.push_back(LVector3(0,0,PLAYER_RADIUS));
    points.push_back(LVector3(0,0,PLAYER_HEIGHT - PLAYER_RADIUS));

    return new BulletMultiSphereShape(points, radius);
}


void Player::update() {

    _prev_pos = _curr_pos;
    _curr_pos = get_pos();
}



// void Player::set_primary_weapon(Gun* gun) {
//     _primary_weapon = gun;
// }

// void Player::set_secondary_weapon(Gun* gun) {
//     _secondary_weapon = gun;
// }

// void Player::set_melee_weapon(Melee* melee) {
//     _melee_weapon = melee;
// }

// void Player::set_ammo(const string& name, size_t n) {
//     _ammos[name] = n;
// }


// void Player::set_ammo(AmmoData* ammo, size_t n) {
//     set_ammo(ammo->get_name(), n);
// }

// void Player::set_current_weapon(WeaponSlot slot) {
//     _current_weapon_slot = slot;
// }


// void Player::set_ads(bool activate) {
//     _is_ads = activate;
// }

// void Player::set_fire(bool activate) {
//     _is_fire = activate;
// }