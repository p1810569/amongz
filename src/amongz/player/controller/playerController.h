// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PLAYERCONTROLLER_H__
#define __PLAYERCONTROLLER_H__

#include <pointerTo.h>
#include <aa_luse.h>
#include "utils.h"
#include "player.h"


class LootableItem;


class PlayerController: public Player {

    REGISTER_TYPE("PlayerController", Player)

public:
    enum Action {
        A_walk_forward,
        A_walk_backward,
        A_strafe_left,
        A_strafe_right,
        A_ads,
        A_reload,
        A_interact,
        A_jump,
        A_fire,
        A_throw_lethal,
        A_throw_tactical,
        A_crouch,
        A_prone,
        A_switch_weapon,
        A_primary_weapon,
        A_secondary_weapon,
        A_melee,
        A_next_crosshair,
        A_previous_crosshair,
        A_none
    };

    virtual ~PlayerController() = default;
    
    virtual void ads(bool activate);
    virtual void fire(bool activate);
    virtual void reload();
    virtual void jump();
    virtual void loot(LootableItem* item);
    virtual void switch_weapon();
    virtual void switch_crosshair(bool next);

protected:
    PlayerController(const std::string& name);
};


#endif // __PLAYERCONTROLLER_H__