// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PLAYERCONTROLLERINPUT_H__
#define __PLAYERCONTROLLERINPUT_H__


#include <asyncTask.h>
#include "playerController.h"
#include "fsm.h"
#include "nodePath.h"
#include "player.h"


class Player;
class PlayerInput;
class PlayerView;
class AmmoPack;



class PlayerControllerInput final : public PlayerController, private TaskPool {

    REGISTER_TYPE("PlayerControllerInput", PlayerController)

public:
    PlayerControllerInput(const std::string& name);
    ~PlayerControllerInput() = default;

    void do_action(Action action, bool activate);

private:

    FSM _fsm;
    CPT(PlayerInput) _input;

    AsyncTask::DoneStatus update(AsyncTask* task);

    bool has_input() const;
    void pick_input();
    float _view_cap;

    void move(LVector2 dir);
    void rotate_view(LVector2 dir);
    void fire(bool activate) override;
    void ads(bool activate) override;
    void reload() override;
    void jump() override;
    void loot(LootableItem* item) override;
    void switch_weapon() override;
    void switch_crosshair(bool next) override;

    void loot_weapon(Weapon* weapon);
    void loot_ammo(AmmoPack* ammo);
    
    void do_fire(const Event* event);
    void do_reload(const Event* event);

    LVector2 _move_vel;

    PT(PlayerView) _view;

    bool _auto_reload;
};


#endif // __PLAYERCONTROLLERINPUT_H__