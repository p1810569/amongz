// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <buttonThrower.h>
#include <inputDeviceManager.h>
#include <inputDeviceNode.h>

#include "game.h"
#include "utils.h"
#include "lvector2.h"
#include "playerInputGamepad.h"

using namespace std;
using DeviceClass = InputDevice::DeviceClass;
using Axis = InputDevice::Axis;


PlayerInputGamepad::PlayerInputGamepad(
        PlayerControllerInput* controller, InputDevice* device):
    PlayerInput(controller),
    _device(device),
    _view_sensitivity(2.0),
    _deadzone(0.2),
    _accel(2.0)
{

    // InputInterfaceManager* mgr = InputInterfaceManager::get_global_ptr();
    // _interface = mgr->acquire_interface(DeviceClass::gamepad);

    Game* game = Game::get_global_ptr();
    DataNode* root = game->get_data_root();

    _device_node = new InputDeviceNode(device, "gamepad");
    _thrower = new ButtonThrower("gamepad-thrower");

    _device_node->add_child(_thrower);
    root->add_child(_device_node);

    _axes.set_device(device);
}


PlayerInputGamepad::~PlayerInputGamepad() {

    _device_node->remove_all_children();

    if (_device->is_connected()) {
        InputDeviceManager* mgr = InputDeviceManager::get_global_ptr();
        mgr->add_device(_device);
    }
}


LVector2 PlayerInputGamepad::get_move() const {

    LVector2 value(0.0f);

    if (!_device->is_connected())
        return value;
    
    value.set_x(_axes.get_axis_value(Axis::left_x));
    value.set_y(_axes.get_axis_value(Axis::left_y));

    return filter_joystick(value);
}


LVector2 PlayerInputGamepad::get_view() const {
    
    LVector2 value(0.0f);

    if (!_device->is_connected())
        return value;

    value.set_x(_axes.get_axis_value(Axis::right_x));
    value.set_y(_axes.get_axis_value(Axis::right_y));
    
    return filter_joystick(value);
}


bool PlayerInputGamepad::is_valid() const {

    return _device->is_connected(); 
}


LVector2 PlayerInputGamepad::filter_joystick(LVector2 value) const {
    
    float d = value.length();
    float factor = saturate((_deadzone - d)/(_deadzone - 1.0));

    return value / d * pow(factor, _accel);
}


void PlayerInputGamepad::AxisManager::set_device(InputDevice* device) {
    
    if (_device != device) {

        _device = device;
        _index_cache.fill(-1); // invalidate cache
    }
}


double PlayerInputGamepad::AxisManager::get_axis_value(Axis axis) const {

    if (_device == nullptr)
        return 0.0;

    size_t axis_id = size_t(axis);
    int& index = _index_cache[axis_id];

    if (index == -1) {
        // update the cache entry
        for (int i=0; i<_device->get_num_axes(); ++i) {
            if (_device->get_axis(i).axis == axis) {
                index = i;
                break;
            }
        }
        if (index == -1)
            return 0.0;
    }
    return _device->get_axis_value(index);
}



PlayerInputGamepad* PlayerInputGamepad::make(PlayerControllerInput* controller) 
{
        
    InputDeviceManager* mgr = InputDeviceManager::get_global_ptr();
    
    InputDeviceSet devices = mgr->get_devices(DeviceClass::gamepad);

    if (devices.size() > 0) {
        InputDevice* device = devices[0];
        device->ref();
        // release the device so that it won't be reused by another player
        mgr->remove_device(device); 
        return new PlayerInputGamepad(controller, device);
    }

    return nullptr;
}
