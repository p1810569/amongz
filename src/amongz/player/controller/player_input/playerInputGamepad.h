// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PLAYERINPUTGAMEPAD_H__
#define __PLAYERINPUTGAMEPAD_H__

#include "playerInput.h"

class ButtonThrower;
class InputDevice;
class InputDeviceNode;
class PlayerControllerInput;


class PlayerInputGamepad final: public PlayerInput {

public:
    static PlayerInputGamepad* make(PlayerControllerInput* controller);
    ~PlayerInputGamepad();

    LVector2 get_move() const override;
    LVector2 get_view() const override;
    bool is_valid() const override;

    float _view_sensitivity;
    float _deadzone;
    float _accel;

private:
    PlayerInputGamepad(PlayerControllerInput* controller, InputDevice* device);

    PT(InputDevice) _device;
    PT(InputDeviceNode) _device_node;
    PT(ButtonThrower) _thrower;

    LVector2 filter_joystick(LVector2 value) const;

    class AxisManager {
    public:
        AxisManager() = default;
        ~AxisManager() = default;

        void set_device(InputDevice* device);
        double get_axis_value(InputDevice::Axis axis) const;
    private:
        PT(InputDevice) _device;

        typedef std::array<int, 19> CacheEntries;
        mutable CacheEntries _index_cache;
    };

    AxisManager _axes;
};

#endif // __PLAYERINPUTGAMEPAD_H__    fri