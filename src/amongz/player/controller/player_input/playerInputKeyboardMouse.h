// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PLAYERINPUTKEYBOARDMOUSE_H__
#define __PLAYERINPUTKEYBOARDMOUSE_H__

#include <mouseWatcher.h>
#include "playerInput.h"

class MouseAndKeyboard;
class MouseWatcher;
class GraphicsWindowInputDevice;
class InputMapping;

/**
 * @brief implementation de PlayerInput pour les entrés clavier / souris
 * @details en partant du principe qu'un ordinateur ne posède d'un clavier et 
 * qu'une souris, plusieurs instances ne peuvent exister en même temps; c'st 
 * pour cela que pour instancier cette classe, la méthode statique @fn make doit 
 * être utiliser
 **/
class PlayerInputKeyboardMouse final: public PlayerInput {

public:
    /**
     * @brief permet d'instancier la classe
     * @param controller pointeur sur 
     * @return pointeur vers l'instance crée, si une autre instance existe déja
     * alors un pointeur nul sera reourné
     */
    static PlayerInputKeyboardMouse* make(PlayerControllerInput* controller);

    ~PlayerInputKeyboardMouse();

    LVector2 get_move() const override;
    LVector2 get_view() const override;
    bool is_valid() const override;

    float _view_sensitivity;
    float _smoothing;
    float _accel;

private:
    PlayerInputKeyboardMouse(PlayerControllerInput* controller, int device);
    PlayerInputKeyboardMouse(const PlayerInputKeyboardMouse&) = delete;
    PlayerInputKeyboardMouse& operator =(const PlayerInputKeyboardMouse&) = delete;
    
    void window_events(const Event* event);

    void load_default_mapping();
    bool is_any_button_down(PlayerController::Action action) const;
    bool are_all_buttons_down(PlayerController::Action action) const;

    class MKEventHandler: public MouseWatcherRegion {  
    public:
        MKEventHandler(PlayerInputKeyboardMouse& host);
        ~MKEventHandler() = default;

    private:
        PlayerInputKeyboardMouse& _host;

        void press(const MouseWatcherParameter& param) override;
        void release(const MouseWatcherParameter& param) override;
        void dispatch(ButtonHandle button, bool activate) const;
    };


    PT(GraphicsWindow) _window;
    PT(MouseAndKeyboard) _device;
    PT(MouseWatcher) _watcher;

    MKEventHandler _handler;
    InputMapping _mapping;

    int _dev_id;
    bool _is_on_focus;

    mutable LVector2 _prev_mouse_pos;

    static bool _used;
};


#endif // __PLAYERINPUTKEYBOARDMOUSE_H__