// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __PLAYERINPUT_H__
#define __PLAYERINPUT_H__

#include <pointerTo.h>
#include <genericAsyncTask.h>
#include <buttonHandle.h>
#include "playerController.h"


class InputDevice;
class InputInterfaceManager;
class PlayerControllerInput;

/**
 * @interface PlayerInput
 * @brief classe abstraite représentant un périphérique controlant un joueur
 **/
class PlayerInput: public TypedReferenceCount {

public:

    PlayerInput(PlayerControllerInput* controller);
    virtual ~PlayerInput() = default;

    /**
     * @brief récupere le déplacement relatif d'un joueur
     **/
    virtual LVector2 get_move() const = 0;

    /**
     * @brief récupere le rotation e la vue relatif d'un joueur
     **/
    virtual LVector2 get_view() const = 0;

    /**
     * @brief vérifie si le périphérique est valide.
     * @details si cette metode retourne faux, alors les valeurs retourné par 
     * les méthodes virtuelles ne doivent pas être prises en compte
     **/
    virtual bool is_valid() const = 0;

protected:
    const WPT(PlayerControllerInput) _controller;
};


/**
 * @brief classe permettant des lier un bouton à un ou pliseurs action
 **/
class InputMapping {

public:
    InputMapping() = default;
    ~InputMapping() = default;
    
    typedef PlayerController::Action Action;

    typedef std::set<ButtonHandle> Buttons;
    typedef std::set<Action> Actions;

    typedef std::array<Buttons, PlayerController::A_none> ActionMap;
    typedef std::map<ButtonHandle, Actions> ButtonMap;

    /**
     * @brief map un bouton a une action
     */
    void map(ButtonHandle button, Action action);

    /**
     * @brief supprime le mapping d'un bouton a une action
     */
    void unmap(ButtonHandle button, Action action);

    /**
     * @brief reinitialise tous les mapping
     */
    void unmap_all();
    
    /**
     * @brief vérifie si un bouton est lié a une action
     */
    bool is_mapped(ButtonHandle button, Action action) const;
    
    /**
     * @brief renvoie tous les bouttons liés à une action
     */
    Buttons get_buttons(Action action) const;

    /**
     * @brief renvoie tous les actions liés à un boutons
     */
    Actions get_actions(ButtonHandle button) const;

private:
    ButtonMap _button_map;
    ActionMap _action_map;
};

#endif // __PLAYERINPUT_H__