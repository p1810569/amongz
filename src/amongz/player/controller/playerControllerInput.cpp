// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <asyncTaskManager.h>
#include <eventQueue.h>
#include <eventHandler.h>

#include "player.h"
#include "idleState.h"
#include "noWeaponState.h"
#include "takeWeaponState.h"
#include "inputInterfaceManager.h"
#include "playerInput.h"
#include "playerInputGamepad.h"
#include "playerInputKeyboardMouse.h"
#include "callbacks.h"
#include "fsm.h"
#include "gun.h"
#include "redDotSight.h"
#include "attachment.h"
#include "config_weapon.h"
#include "playerView.h"
#include "ammoPack.h"
#include "ammoData.h"
#include "melee.h"
#include "playerControllerInput.h"


using namespace std;


DEFINE_TYPEHANDLE(PlayerControllerInput)


PlayerControllerInput::PlayerControllerInput(const string& name):
    PlayerController(name),
    _move_vel(0.0),
    _view_cap(170.0),
    _view(new PlayerView(this)),
    _auto_reload(true)
{
    attach_new_node(_view);

    _handler->add_hook("fire", callback(do_fire));
    _handler->add_hook("reload", callback(do_reload));

    pick_input();

    do_add_task(named_method(update));

    _fsm.add_state(new IdleState(this));
    _fsm.add_state(new NoWeaponState(this));
    _fsm.add_state(new TakeWeaponState(this));

    _fsm.push_state("idle");
}




void PlayerControllerInput::do_action(Action action, bool activate) {


    State* state = _fsm.get_active_state();

    if (state != nullptr) {
        PlayerState* pstate = DCAST(PlayerState, state);
        pstate->do_action(action, activate);
    }
}


bool PlayerControllerInput::has_input() const {

    return _input != nullptr && _input->is_valid();    
}


AsyncTask::DoneStatus PlayerControllerInput::update(AsyncTask* task) {

    if (!has_input())
        pick_input();
    else {
        rotate_view(_input->get_view());
        move(_input->get_move());
    }

    Player::update();

    _fsm.update();
    _view->update();

    _handler->process_events();

    return AsyncTask::DS_cont;
}


void PlayerControllerInput::move(LVector2 dir) {
    
    ClockObject* clock = ClockObject::get_global_clock();

    _req_motion = LVector3(dir * clock->get_dt(), 0.0f);

    // simulate inertia
    _move_vel /= clock->get_dt() * 15.0 + 1.0;
    _move_vel += dir;

    LVector3 motion(_move_vel,0.0);

    set_linear_movement(motion, true);

}


void PlayerControllerInput::rotate_view(LVector2 dir) {

    ClockObject* clock = ClockObject::get_global_clock();

    dir.set_x(rad_2_deg(dir.get_x()));
    dir.set_y(rad_2_deg(dir.get_y()));

    set_angular_movement(-dir.get_x());

    dir *= clock->get_dt();

    float cap = _view_cap * 0.5f;
    float pitch = _view->get_p();
    float clamped = clamp(pitch + dir.get_y(), -cap, cap);
    _view->set_p(clamped);

    _view->set_view_move(-dir.get_x(), clamped - pitch);
}


void PlayerControllerInput::fire(bool activate) {

    Weapon* weapon = get_weapon();

    if (weapon == nullptr)
        return;

    if (weapon->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, weapon);

        if (activate)
            gun->pull_trigger();
        else
            gun->release_trigger();

        _is_fire = activate;
    }
}


void PlayerControllerInput::ads(bool activate) {

    _view->ads(activate);
}


void PlayerControllerInput::reload() {

    Weapon* weapon = get_weapon();

    if (weapon == nullptr)
        return;

    if (weapon->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, weapon);
        size_t num_rounds = gun->get_num_rounds();

        if (get_ammo(gun->get_ammo()) && num_rounds < gun->get_max_rounds())
            _view->reload();
    }
}


void PlayerControllerInput::jump() {

    if (can_jump())
        do_jump();
}


void PlayerControllerInput::loot(LootableItem* item) {

    if (item->is_of_type(Weapon::get_class_type()))
        loot_weapon(DCAST(Weapon, item));
    
    else if (item->is_of_type(AmmoPack::get_class_type()))
        loot_ammo(DCAST(AmmoPack, item));

    if (item->get_ref_count() == 0)
        delete item;  
}


void PlayerControllerInput::switch_weapon() {

    Gun* gun = DCAST(Gun, get_weapon());
    gun->release_trigger();

    if (_current_weapon_slot == WS_primary) {
        if (_secondary_weapon != nullptr)
            _current_weapon_slot = WS_secondary;
    } 
    else if (_current_weapon_slot == WS_secondary) {
        if (_primary_weapon != nullptr)
            _current_weapon_slot = WS_primary;
    }
}

void PlayerControllerInput::switch_crosshair(bool next) {
    
    Weapon* weapon = get_weapon();

    if (weapon && weapon->is_of_type(Gun::get_class_type())) {

        Gun* gun = DCAST(Gun, weapon);
        
        RedDotSight* sight = DCAST(RedDotSight, 
            gun->get_attachment(RedDotSight::get_class_type()));

        if (sight != nullptr) {
            
            size_t id = sight->get_crosshair();
            
            if (next) {
                if (id < sight->get_num_crosshair_texture() - 1) ++id;
            }
            else if (id > 0) --id;
            
            sight->set_crosshair(id);
        }
    }
}


void PlayerControllerInput::loot_weapon(Weapon* weapon) {
        
    if (weapon->is_of_type(Melee::get_class_type())) {

        _current_weapon_slot = WS_melee;
        _melee_weapon = DCAST(Melee, weapon);
    }
    
    else if (weapon->is_of_type(Gun::get_class_type())) {

        Gun* gun = DCAST(Gun, weapon);
        gun->set_event_queue(_queue);

        if (!_primary_weapon) {
            _primary_weapon = gun;
            _current_weapon_slot = WS_primary;
        }
        else if (!_secondary_weapon) {
            _secondary_weapon = gun;
            _current_weapon_slot = WS_secondary;
        }

        else if (_current_weapon_slot == WS_primary)
            _primary_weapon = gun;

        else if (_current_weapon_slot == WS_secondary)
            _secondary_weapon = gun;

        _view->take_gun(gun, true);

        Event* event = new Event("take-gun");
        event->add_parameter(gun);
        _queue->queue_event(event);
    }
}


void PlayerControllerInput::loot_ammo(AmmoPack* ammo) {
    
    AmmoData* data = ammo->get_data();
    _ammos[data->get_name()] += ammo->loot();
}


void PlayerControllerInput::do_fire(const Event* event) {

    nassertv(event->get_num_parameters() == 1)

    EventParameter param = event->get_parameter(0);
    Gun* gun = DCAST(Gun, param.get_ptr());

    if (get_weapon() == gun) {

        _view->fire();

        if (gun->get_num_rounds() == 0 && _auto_reload) {

            do_task_later([gun, this](AsyncTask*) {
                if (gun == get_weapon()) reload();
                return AsyncTask::DS_done;
            }, "reload", 0.2);
        }
    }
}


void PlayerControllerInput::do_reload(const Event* event) {

    Weapon* weapon = get_weapon();
    nassertv(weapon != nullptr);

    if (weapon->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, weapon);

        AmmoData* ammo = gun->get_ammo();

        Ammos::iterator it = _ammos.find(ammo->get_name());

        if (it == _ammos.end())
            return;

        size_t rounds = gun->get_num_rounds();
        size_t refill = min(gun->get_max_rounds() - rounds, it->second);

        gun->set_num_rounds(rounds + refill);
        it->second -= refill;
    }
}


void PlayerControllerInput::pick_input() {

    _input = PlayerInputGamepad::make(this);

    if (_input == nullptr)
        _input = PlayerInputKeyboardMouse::make(this);
}
