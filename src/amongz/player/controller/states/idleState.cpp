// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __IDLESTATE_H__
#define __IDLESTATE_H__


#include "idleState.h"
#include "playerInput.h"
#include "playerController.h"


using namespace std;


void IdleState::enter() {
    
}


void IdleState::update() {

}


void IdleState::exit() {
    
}


void IdleState::do_action(PlayerController::Action action, bool activate) {

    switch (action) {
        case PlayerController::A_ads:
            _controller->ads(activate); break;
        case PlayerController::A_fire:
            _controller->fire(activate); break;
        case PlayerController::A_reload:
            if (activate) _controller->reload(); break;
        case PlayerController::A_jump:
            if (activate) _controller->jump(); break;
        case PlayerController::A_next_crosshair:
            _controller->switch_crosshair(true); break;
        case PlayerController::A_previous_crosshair:
            _controller->switch_crosshair(false); break;


        default:
            break;
    }
}

void IdleState::move() {
    
}


IdleState::IdleState(PlayerController* controller): 
    PlayerState("idle", controller) 
{
    _flags = F_always_update;

}

#endif // __IDLESTATE_H__