// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __PLAYERSTATE_H__
#define __PLAYERSTATE_H__


#include "state.h"
#include "player.h"
#include "playerController.h"

/**
 * @brief Classe abstraite d'un état d'un joueur
 * @remark Cette classe n'est pas sensée être instantié directement.
 * Chaque état doit hériter de cette classe.
 */
class PlayerState: public State {

public:
    virtual void do_action(PlayerController::Action action, bool activate);

protected:
    /**
     * @param name : nom de l'état
     * @param player : pointeur sur le joueur qui sera contrôlé par cet etat
    */
    PlayerState(const std::string& name, PlayerController* controller);
    virtual ~PlayerState() = default;

    /**
     * @remark on n'utilise pas PT() pour éviter les références circulaires
    */
    PlayerController* const _controller;
};
#endif // __PLAYERSTATE_H__