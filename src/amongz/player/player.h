
// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <nameUniquifier.h>
#include <bulletCharacterControllerNode.h>
#include "callbacks.h"
#include "utils.h"


#define PLAYER_RADIUS 0.2f
#define PLAYER_HEIGHT 1.8f
#define PLAYER_MAX_JUMP_HEIGHT 2.0
#define PLAYER_JUMP_SPEED 3.0



class BulletCapsuleShape;
class Gun;
class Melee;
class Weapon;
class EventHandler;
class EventQueue;
class AmmoData;


class Player: public BulletCharacterControllerNode, public LocalNodePath {

    REGISTER_TYPE("Player", BulletCharacterControllerNode, LocalNodePath)

public:

    enum WeaponSlot {
        WS_none = 0,
        WS_primary,
        WS_secondary,
        WS_melee,
    };


    Player(const std::string& name);
    virtual ~Player();

    void set_name(const std::string& name);

    Weapon* get_weapon(WeaponSlot slot=WS_none) const;

    size_t get_ammo(const std::string& name) const;

    size_t get_ammo(AmmoData* ammo) const;

    WeaponSlot get_current_weapon() const;
    
    Gun* get_primary_weapon() const;

    Gun* get_secondary_weapon() const;

    Melee* get_melee_weapon() const;

    void update();
    
    float get_ads() const;
    void set_ads(float value);

    bool get_fire() const;

    LVector3 get_motion_delta();
    LVector3 get_requested_motion();

    EventHandler* get_event_handler() const;
    EventQueue* get_event_queue() const;

protected:

    EventQueue* _queue;
    EventHandler* _handler;

    float _health;
    float _stamina;
    float _ads_value;
    bool _is_fire;

    WeaponSlot _current_weapon_slot;
    PT(Gun) _primary_weapon;
    PT(Gun) _secondary_weapon;
    PT(Melee) _melee_weapon;

    typedef std::map<std::string, size_t> Ammos;
    Ammos _ammos;

    LVector3 _prev_pos;
    LVector3 _curr_pos;
    LVector3 _req_motion;

private:

    static NameUniquifier _names;
    static std::string add_name(const std::string& name);
    static BulletShape* make_shape();
};


#endif // __PLAYER_H__








    // void set_ammo(const std::string& name, size_t n);
    // void set_ammo(AmmoData* ammo, size_t n);
    // void set_current_weapon(WeaponSlot slot);
    // void set_primary_weapon(Gun* gun);
    // void set_secondary_weapon(Gun* gun);
    // void set_melee_weapon(Melee* melee);
    // void set_ads(bool activate);
    // void set_fire(bool activate);