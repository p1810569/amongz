// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __PLAYERVIEW_H__
#define __PLAYERVIEW_H__


#include <playerAnimInterface.h>
#include <displayRegion.h>
#include <character.h>  // for Character
#include <nodePath.h>   // for NodePath

#include "localRef.h"
#include "playerAnimInterface.h"
#include "playerViewBase.h"


class Camera;
class Player;
class RenderPipeline;
class Viewport;
class PlayerHUD;
class Weapon;
class Gun;

#define PLAYER_BACKGROUND_LAYER 1
#define PLAYER_VIEW_LAYER 1


class PlayerView: public PlayerViewBase, public LocalNodePath {

    REGISTER_TYPE("PlayerView", PlayerViewBase, LocalNodePath)

public:
    PlayerView(Player* player);
    ~PlayerView() = default;

    Viewport* get_viewport() const;

    void update();
    void take_gun(Gun* gun, bool first_time);

    void ads(bool enable);
    void fire();
    void reload();
    void set_scene(NodePath scene);

    void set_ads_height(float height);
    void set_view_move(float x, float y);

    using PlayerViewBase::set_effect;

private:
    void init_viewarms();
    void init_layers();

    PartBundleHandle* _part_handle;

    class ViewLayer final: public DisplayRegion {

        REGISTER_TYPE("PlayerView::ViewLayer", DisplayRegion)
    
    public:
        ViewLayer(GraphicsOutput* buffer, PlayerView* view);
        ~ViewLayer();

        Camera* get_camera() const;
        Lens* get_lens() const;
        void set_lens(Lens* lens);
        void set_lens(int index, Lens* lens);
        void set_fov(float fov);
        void set_initial_state(const RenderState* state);
        void set_camera_mask(DrawMask mask);
        void set_scene(NodePath scene);

    private:
        PT(Camera) _camera;
    };

    /** @brief Classe permetant la convertion des coordonnés de l'espace 
     * de vue à l'espace du monde avec un champ de vision différent,
     * voir ViewLayer
     */
    class FovAdjustEffect final: public RenderEffect {    
    public:
        FovAdjustEffect() = default;
        ~FovAdjustEffect() = default;

        void adjust_transform(CPT(TransformState)& net_transform,
                            CPT(TransformState)& node_transform,
                            const PandaNode* node) const override;
        
        bool has_adjust_transform() const override;
    private:
        mutable bool _ignore_flag = false;
    };


    PT(ViewLayer) _foreground_layer;
    PT(ViewLayer) _background_layer;
    PT(Character) _character;

    PlayerAnimInterface _anim_interface;

    NodePath _player_np;
    NodePath _weapon_np;
    NodePath _view;
    NodePath _root;

    float _foreground_fov;
    float _background_fov;

    float _foreground_fov_ads;
    float _background_fov_ads;

    PT(Weapon) _weapon;

    DrawMask _mask;
};

#endif // __PLAYERVIEW_H__



    // void r_replace_character(CharacterJoint* joint);

    // class GunAnchor: public CharacterJoint {
    // public:
    //     GunAnchor(Character* character, PartGroup* parent);
    //     ~GunAnchor();

    //     void detach_gun();
    //     bool attach_gun(Character* rig);

    //     bool update_internals(PartBundle *root, PartGroup *parent, 
    //         bool self_changed, bool parent_changed, Thread *current_thread) override;
    //     void r_replace_character(CharacterJoint* joint);

    //     virtual void get_blend_value(const PartBundle *root) override;
    //     Character* _character;

    //     friend class PlayerView;
    // };

    // PT(GunAnchor) _gun_anchor;
