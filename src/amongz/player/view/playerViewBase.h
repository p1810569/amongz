// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PLAYERVIEWBASE_H__
#define __PLAYERVIEWBASE_H__

#include "playerHUD.h"


class RenderPipeline;
class GraphicsBuffer;
class DIsplayRegion;
class Viewport;
class Player;


class PlayerViewBase: public PandaNode {

public:
    virtual ~PlayerViewBase();

    Player* get_player() const;

protected:
    PlayerViewBase(Player* player);

    void add_region(DisplayRegion* region);
    void remove_region(DisplayRegion* region);
    void set_event_queue(EventQueue* queue);


    PT(RenderPipeline) _pipeline;
    PT(GraphicsOutput) _buffer;
    PT(Viewport) _viewport;
    
    PlayerHUD _hud;

    void init();

    Player* _player;

private:
    void update_viewport(const Event* event);

    static RenderPipeline* make_pipeline(Viewport* viewport);
    static Viewport* make_viewport();

    typedef pvector<PT(DisplayRegion)> ViewRegions;
    ViewRegions _regions;
};

#endif // __PLAYERVIEWBASE_H__