// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PLAYERANIMINTERFACE_H__
#define __PLAYERANIMINTERFACE_H__


#include <animControlCollection.h>

#include "animPostProcess.h"
#include "animStack.h"
#include "animLayer.h"


class PartBundleHandle;
class Character;
class CIntervalManager;
class PlayerView;
class AdsControl;
class Gun;

class PlayerAnimInterface {

public:
    PlayerAnimInterface(PlayerView* view);
    ~PlayerAnimInterface();

    void update();

    void set_gun(Gun* gun, PartBundle* part);

    void pullout(bool first);
    void putaway(bool quick);
    void ads(bool enabled);
    void fire(bool ads);
    void reload(bool empty);

    void set_ads_height(float height);
    void set_weapon_sway(float x, float y);
    void set_sway_factor(float x, float y);
    void set_idle_sway_factor(float factor);
    
    float get_ads_value() const;

private:

    class AdsControl final : public AnimPostProcess {
    public:
        AdsControl();
        ~AdsControl() = default;

        bool set_control(AnimControl* control);
            
        CPT(TransformState) update() override;
        void set_ads_height(float height);

    private:
        float _height_factor = 1.0f;
        float _final_height = 0.0f;
    };

    class WeaponSway final : public AnimLayer {
    public:
        WeaponSway();
        ~WeaponSway() = default;

        CPT(TransformState) update() override;

        void set_factor(float x, float y);
        void set_stiffness(float stiffness);

        void set_sway(float x, float y);

    private:
        LVector2 _factor;
        LVector2 _value;
        LVector2 _sway;
        float _stiffness;
    };

    class IdleSway final : public AnimLayer {
    public:
        IdleSway();
        ~IdleSway() = default;

        CPT(TransformState) update() override;

        void set_factor(float factor);
        void set_speed(float speed);

    private:
        float _factor;
        float _speed;
    };

    AnimControlCollection _controls;
    CIntervalManager* _mgr;

    AnimStack _anim_stack;
    AnimStack _ads_stack;

    PT(AdsControl) _ads_control;
    PT(WeaponSway) _weapon_sway;
    PT(IdleSway) _idle_sway;
};

#endif // __PLAYERANIMINTERFACE_H__

























// class AnimControls {
    
//     public:
//         void play_all();
//         void play_all(double from, double to);
//         void loop_all(bool restart);
//         void loop_all(bool restart, double from, double to);
//         bool stop_all();
//         void pose_all(double frame);

//         AnimControlCollection* find_anim(const std::string& name) const;
//         AnimControl* find_anim(const std::string& anim_name,const std::string& bundle_name) const;

//         bool play(const std::string& name);
//         bool play(const std::string& name, double from, double to);
//         bool loop(const std::string& name, bool restart);
//         bool loop(const std::string& name, bool restart, double from, double to);
//         bool stop(const std::string& name);
//         bool pose(const std::string& name, double frame);

//         void store_anim(AnimControl* control, const std::string& bundle_name);

//         void clear_anims();
    
//     private:
//         typedef pmap<std::string, AnimControlCollection> Anims;
//         Anims _anims;
//     };