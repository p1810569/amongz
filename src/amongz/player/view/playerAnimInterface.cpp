// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <partBundleHandle.h>
#include <character.h>
#include <cIntervalManager.h>
#include <partSubset.h>
#include <animChannelMatrixDynamic.h>

#include "animLayer.h"
#include "animControl.h"
#include "eventAnimControl.h"
#include "eventAnim.h"
#include "ammoData.h"
#include "gun.h"
#include "sight.h"
#include "player.h"
#include "playerView.h"
#include "playerAnimInterface.h"


using namespace std;


GlobPattern all_but(vector<string> names) {

    stringstream ss;
    string::const_iterator it;

    ss << '*';
    for (string& name : names) {
        for (char c: name) ss << "[!" << c << ']';
    }
    ss << '*';

    return GlobPattern(ss.str());
}

GlobPattern all(const string& name) {

    stringstream ss;
    string::const_iterator it;

    for (char c: name) ss << "[!" << c << ']';
    ss << '*';

    return GlobPattern(ss.str());
}


bool PlayerAnimInterface::AdsControl::set_control(AnimControl* control)
{
    
    if (AnimPostProcess::set_control(control)) {

        int last_frame = control->get_num_frames() - 1;
        CPT(TransformState) transform = get_value(last_frame);

        _final_height = transform->get_pos().get_z();

        return true;
    }
    return false;
}


CPT(TransformState) PlayerAnimInterface::AdsControl::update() {
    
    CPT(TransformState) value = get_blend_value();

    LVecBase3 pos = value->get_pos();
    pos.set_z(pos.get_z() * _height_factor);

    return value->set_pos(pos);
}


void PlayerAnimInterface::AdsControl::set_ads_height(float height) {

    if (_final_height != 0.0)
        _height_factor = (_final_height - height) / _final_height;
    else
        _height_factor = 1.0f;
}


PlayerAnimInterface::WeaponSway::WeaponSway():
    AnimLayer("weapon_sway"),
    _sway(0.0f),
    _value(0.0f),
    _stiffness(10.0f),
    _factor(1.0f)
{       

}


CPT(TransformState) PlayerAnimInterface::WeaponSway::update() {
    
    ClockObject* clock = ClockObject::get_global_clock();
    
    LVector2 value(tanh(_sway.get_x() * _factor.get_x()),
                   tanh(_sway.get_y() * _factor.get_y()));

    _value /= clock->get_dt() * _stiffness + 1.0;
    _value += value;

    return TransformState::make_hpr(LVector3(_value, 0.0f));
}


void PlayerAnimInterface::WeaponSway::set_factor(float x, float y) {
    
    _factor.set_x(x);
    _factor.set_y(y);
}

void PlayerAnimInterface::WeaponSway::set_sway(float x, float y) {
    
    _sway.set_x(x);
    _sway.set_y(y);
}


void PlayerAnimInterface::WeaponSway::set_stiffness(float stiffness) {

    _stiffness = max(0.0f, stiffness);
}


PlayerAnimInterface::IdleSway::IdleSway():
    AnimLayer("idle_sway"),
    _factor(1.0f),
    _speed(1.3f)
{

}


CPT(TransformState) PlayerAnimInterface::IdleSway::update() {
    
    ClockObject* clock = ClockObject::get_global_clock();

    double time = clock->get_long_time();

    float ty = time * _speed;
    float tx = ty * 0.5 - 0.25;

    float x = (noise(tx)*0.2f + sin((tx + 0.5) * M_PI)) * _factor * 0.010;
    float y = (noise(ty)*0.2f + sin((ty + 0.5) * M_PI)) * _factor * 0.003;

    LVector3 pos(x, 0.0, y);
    LVector3 hpr(rad_2_deg(-x), rad_2_deg(y), 0.0);

    return TransformState::make_pos_hpr(pos*0.3, hpr);
}


void PlayerAnimInterface::IdleSway::set_factor(float factor) {
    _factor = factor;
}

void PlayerAnimInterface::IdleSway::set_speed(float speed) {
    _speed = speed;
}




PlayerAnimInterface::PlayerAnimInterface(PlayerView* view):
    _mgr(new CIntervalManager()),
    _ads_control(new AdsControl),
    _weapon_sway(new WeaponSway),
    _idle_sway(new IdleSway),
    _anim_stack("tag_anim"),
    _ads_stack("tag_ads")
{

    _anim_stack.local_object();
    _ads_stack.local_object();

    _anim_stack.add_layer(_weapon_sway);
    _anim_stack.add_layer(_idle_sway);
    _ads_stack.add_layer(_ads_control);

    Player* player = view->get_player();
    _mgr->set_event_queue(player->get_event_queue());
}


PlayerAnimInterface::~PlayerAnimInterface() {
    
    delete _mgr;
}


void PlayerAnimInterface::update() {

    _mgr->step();
    _anim_stack.update();
    _ads_stack.update();
}


void PlayerAnimInterface::set_gun(Gun* gun, PartBundle* part) {

    // first off, clear the old anims
    _controls.clear_anims();

    int flags = PartGroup::HMF_ok_wrong_root_name | 
                PartGroup::HMF_ok_part_extra | 
                PartGroup::HMF_ok_anim_extra; 

    size_t num_anims = gun->get_num_anim();

    PartSubset all_subset;
    PartSubset ads_subset;

    ads_subset.add_exclude_joint(GlobPattern("*"));
    ads_subset.add_include_joint(GlobPattern("tag_ads"));

    all_subset.add_exclude_joint(GlobPattern("tag_ads"));
    all_subset.add_exclude_joint(GlobPattern("tag_anim"));
    all_subset.add_include_joint(GlobPattern("tag_origin"));
    all_subset.add_include_joint(GlobPattern("tag_torso"));
    all_subset.add_include_joint(GlobPattern("tag_weapon"));
    all_subset.add_include_joint(GlobPattern("j_shoulder_ri"));
    all_subset.add_include_joint(GlobPattern("j_shoulder_le"));


    for (size_t i=0; i<num_anims; ++i) {

        AnimBundle* anim = gun->get_anim(i);
        string anim_name = anim->get_name();
        
        PartSubset& subset = all_subset;

        if (anim_name == "ads")
            subset = ads_subset;


        PT(EventAnimControl) control = 
            new EventAnimControl(anim_name, part, _mgr);

        if (part->do_bind_anim(control, anim, flags, subset))
            _controls.store_anim(control, anim_name);
    }

    _ads_stack.set_bundle(part);
    _anim_stack.set_bundle(part);

    AnimControl* control = _controls.find_anim("ads");
    if (control != nullptr)
        _ads_control->set_control(control);

    
    Sight* sight = DCAST(Sight, gun->get_attachment(Sight::get_class_type()));
    
    if (sight != nullptr)
        _ads_control->set_ads_height(sight->get_height());
}


void PlayerAnimInterface::pullout(bool first) {

    if (first) {
        if (!_controls.play("first_time_pullout"))
            _controls.play("pullout");
    } else
        _controls.play("pullout");
}


void PlayerAnimInterface::putaway(bool quick) {
    
    if (quick) {
        if (!_controls.play("quick_putaway"))
            _controls.play("putaway");
    } else
        _controls.play("putaway");
}


void PlayerAnimInterface::ads(bool enabled) {

    if (_controls.is_playing("reload") || _controls.is_playing("reload_empty"))
        return;

    AnimControl* control = _controls.find_anim("ads");

    if (control != nullptr) {
        
        int current = control->get_frame();
        int end = control->get_num_frames() - 1;

        if (enabled) {
            control->set_play_rate(1.0);
            control->play(current, end);
        } else {
            control->set_play_rate(-1.0);
            control->play(0, current);
        }
    }
}


void PlayerAnimInterface::fire(bool ads) {

    _controls.stop("reload");
    _controls.stop("reload_empty");

    if (ads) {
        if (!_controls.play("fire_ads"))
            _controls.play("fire");
    } else
        _controls.play("fire");
        
        // PT(CLerpAnimEffectInterval) interval = new CLerpAnimEffectInterval(
        //     "fire_blend", 0.1, CLerpInterval::BT_ease_out);
        
        // interval->add_control(control, "fire", 0.0, 1.0);
        // interval->start();
}


void PlayerAnimInterface::reload(bool empty) {

    if (_controls.is_playing("reload") || _controls.is_playing("reload_empty"))
        return;

    _controls.stop_all();

    // first, stop aiming down sight
    AnimControl* control = _controls.find_anim("ads");

    if (control != nullptr) { 
        control->set_play_rate(-1.0);
        control->play(0, control->get_frame());
    }

    if (empty) {
        if (!_controls.play("reload_empty"))
            _controls.play("reload");
    } else
        _controls.play("reload");

        // part->set_control_effect(control, 1.0);

        // PT(CLerpAnimEffectInterval) interval = new CLerpAnimEffectInterval(
        //     "fire_blend", 0.1, CLerpInterval::BT_ease_out);
        
        // interval->add_control(control, "fire", 0.0, 1.0);
        // interval->start();
}


void PlayerAnimInterface::set_ads_height(float height) {
    
    _ads_control->set_ads_height(height);
}


void PlayerAnimInterface::set_weapon_sway(float x, float y) {

    _weapon_sway->set_sway(x, y);   
}


void PlayerAnimInterface::set_sway_factor(float x, float y) {

    _weapon_sway->set_factor(x, y);    
}


void PlayerAnimInterface::set_idle_sway_factor(float factor) {

    _idle_sway->set_factor(factor);    
}


float PlayerAnimInterface::get_ads_value() const {

    AnimControl* control = _ads_control->get_control();    

    if (control != nullptr)
        return control->get_full_fframe() / control->get_num_frames();
    
    return 0.0f;
}



PlayerAnimInterface::AdsControl::AdsControl(): 
    AnimPostProcess("ads")
{
    
}



























// using EventInterval = EventAnimControl::EventInterval;


// class PartFilter: public PartSubset {

// public:
//     PartFilter(PartBundle* bundle): _bundle(bundle) {

//     }

//     void exclude_joint(const string& name) {

//         add_exclude_joint(name);
//     }

// private:

//     bool r_exclude_joint(const string& name, PartGroup* joint) 
//     {
//         int num_children = joint->get_num_children();

//         if (joint->get_name() == name) {
//             add_exclude_joint(name);

//             for (int i=0; i<num_children; ++i) {
//                 PartGroup* child = joint->get_child(i);
//                 add_include_joint(child->get_name());
//             }
//             return true;
//         }

//         for (int i=0; i<num_children; ++i) {
//             PartGroup* child = joint->get_child(i);
//             if (r_exclude_joint(name, child))
//                 return true;
//         }
//         return false;
//     }

//     PartBundle* _bundle;
// };







// void PlayerAnimInterface::AnimControls::play_all() {

//     for (Anims::iterator it=_anims.begin(); it != _anims.end(); ++it)
//         it->second.play_all();
// }


// void PlayerAnimInterface::AnimControls::play_all(double from, double to) {
    
//     for (Anims::iterator it=_anims.begin(); it != _anims.end(); ++it)
//         it->second.play_all(from, to);
// }


// void PlayerAnimInterface::AnimControls::loop_all(bool restart) {
    
//     for (Anims::iterator it=_anims.begin(); it != _anims.end(); ++it)
//         it->second.loop_all(restart);
// }


// void PlayerAnimInterface::
// AnimControls::loop_all(bool restart, double from, double to) {
    
//     for (Anims::iterator it=_anims.begin(); it != _anims.end(); ++it)
//         it->second.loop_all(restart, from ,to);
// }


// void PlayerAnimInterface::AnimControls::clear_anims() {
    
//     for (Anims::iterator it=_anims.begin(); it != _anims.end(); ++it)
//         it->second.clear_anims();
// }


// bool PlayerAnimInterface::AnimControls::stop_all() {

//     bool any = false;

//     for (Anims::iterator it=_anims.begin(); it != _anims.end(); ++it)
//         any |= it->second.stop_all();
    
//     return any;
// }


// void PlayerAnimInterface::AnimControls::pose_all(double frame) {
    
//     for (Anims::iterator it=_anims.begin(); it != _anims.end(); ++it)
//         it->second.pose_all(frame);
// }


// AnimControlCollection* PlayerAnimInterface::
// AnimControls::find_anim(const string& name) const {
    
//     Anims::const_iterator it = _anims.find(name);

//     if (it != _anims.end())
//         return const_cast<AnimControlCollection*>(&it->second);
    
//     return nullptr;
// }

// AnimControl* PlayerAnimInterface::
// AnimControls::find_anim(const string& anim_name, 
//                         const string& bundle_name) const
// {    
//     AnimControlCollection* controls = find_anim(anim_name);

//     if (controls != nullptr)
//         return controls->find_anim(bundle_name);
    
//     return nullptr;
    
// }


// bool PlayerAnimInterface::AnimControls::play(const string& name) {
    
//     AnimControlCollection* controls = find_anim(name);

//     if (controls != nullptr) {
//         controls->play_all();
//         return true;
//     }
//     return false;
// }


// bool PlayerAnimInterface::
// AnimControls::play(const string& name, double from, double to) {

//     AnimControlCollection* controls = find_anim(name);

//     if (controls != nullptr) {
//         controls->play_all(from, to);
//         return true;
//     }
//     return false;
// }


// bool PlayerAnimInterface::AnimControls::loop(const string& name, bool restart) {
    
//     AnimControlCollection* controls = find_anim(name);

//     if (controls != nullptr) {
//         controls->loop_all(restart);
//         return true;
//     }
//     return false;
// }


// bool PlayerAnimInterface::
// AnimControls::loop(const string& name, bool restart, double from, double to) {
    
//     AnimControlCollection* controls = find_anim(name);

//     if (controls != nullptr) {
//         controls->loop_all(restart, from, to);
//         return true;
//     }
//     return false;
// }


// bool PlayerAnimInterface::AnimControls::stop(const string& name) {
    
//     AnimControlCollection* controls = find_anim(name);

//     if (controls != nullptr) {
//         controls->stop_all();
//         return true;
//     }
//     return false;
// }


// bool PlayerAnimInterface::AnimControls::pose(const string& name, double frame) {
    
//     AnimControlCollection* controls = find_anim(name);

//     if (controls != nullptr) {
//         controls->pose_all(frame);
//         return true;
//     }
//     return false;
// }


// void PlayerAnimInterface::
// AnimControls::store_anim(AnimControl* control, const string& bundle_name) {
    
//     AnimBundle* anim = control->get_anim();
//     nassertv(anim != nullptr)
//     _anims[anim->get_name()].store_anim(control, bundle_name);
// }