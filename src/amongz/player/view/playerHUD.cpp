// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include <camera.h>
#include <orthographicLens.h>
#include <render_utils.h>
#include <displayRegion.h>
#include <cardMaker.h>
#include <textNode.h>
#include <fontPool.h>
#include <texturePool.h>
#include <samplerState.h>

#include "defines.h"
#include "gun.h"
#include "player.h"
#include "color.h"
#include "playerHUD.h"

using namespace std;


DEFINE_TYPEHANDLE(PlayerHUD)



PlayerHUD::PlayerHUD(Player* player, DisplayRegion* region): 
    _player(player),
    _region(region),
    _crosshair_value(0.0f),
    _root("hud"),
    _viewport_offset(1, LVector2i(0))
{
    create_2d_scene(_root, _camera_np);

    if (region != nullptr)
        set_region(region);

    init();
}


PlayerHUD::~PlayerHUD() {
    
    if (_region != nullptr && _region->get_camera() == _camera_np)
        _region->set_camera(NodePath());
}


void PlayerHUD::set_region(DisplayRegion* region) {

    _region = region;
    _region->set_camera(_camera_np);
}


void PlayerHUD::set_view_texture(Texture* texture) {

    if (_view_quad.is_empty()) {
        create_fullscreen_quad(_root, _view_quad);
        _view_quad.set_shader_input("u_viewport_offset", _viewport_offset);
    }

    _view_texture = texture;
    _view_quad.set_shader_input("u_render", texture);
}


void PlayerHUD::update_region() {

    int l, r, b, t;
    _region->get_pixels(l, r, b, t);

    if (!_view_quad.is_empty()) {

        _viewport_offset[0].set_x(l);
        _viewport_offset[0].set_y(b);
    }

    int width = r - l;
    int height = t - b;

    Camera* camera = DCAST(Camera, _camera_np.node());
    Lens* lens = camera->get_lens();
    lens->set_aspect_ratio((float)width / (float)height);

    LVector2 file_size = lens->get_film_size();

    LVector3 scale(file_size.get_x(), 0.0, file_size.get_y());
    nout << scale << endl;
    _view_quad.set_scale(scale);
}


void PlayerHUD::update() {

    LVector3 delta = _player->get_motion_delta();
    LVector3 requested = _player->get_requested_motion();

    float motion = min(requested.length(), delta.length());

    CPT(TransformState) transform = _crosshair->get_transform();

    LVector3 pos(transform->get_pos());

    float value = -0.025f;

    if (motion > 0.0f)
        value = motion;
    
    _crosshair_value += value * 0.4;

    _crosshair_value = clamp(_crosshair_value, 
                            _crosshair_scale, 
                            _crosshair_scale * 1.8f);

    float ads = 1.0f - _player->get_ads();

    _crosshair_np.set_color(Color(1.0f,1.0f,1.0f, ads));
    _crosshair_np.set_z(_crosshair_value * ads);
}


void PlayerHUD::init() {

    _root.set_depth_write(false);
    _root.set_depth_test(false);
    _root.set_material_off(true);
    _root.set_two_sided(true);

    // ammo counter

    EventHandler* handler = _player->get_event_handler();
    handler->add_hook("take-gun", callback(take_gun));
    handler->add_hook("reload", callback(reload));
    handler->add_hook("fire", callback(fire));

    TextFont* font = FontPool::load_font(FONT_PATH "roboto.bam");
    nassertv(font != nullptr)

    _ammo_counter = new TextNode("ammo-counter");
    _ammo_counter->set_align(TextNode::A_right);
    _ammo_counter->set_font(font);
    _ammo_counter->set_text("0/0");
    _ammo_counter->set_text_scale(0.05);
    _ammo_counter->set_shadow(0.0f, 0.002f);
    _ammo_counter->set_shadow_color(Color::black());

    NodePath counter_np = _root.attach_new_node(_ammo_counter, 10);
    counter_np.set_pos(0.9, 0.0, -0.5);

    // crosshair

    CardMaker cm("crosshair");
    cm.set_frame(-0.01f,0.01f,-0.01f,0.01f);

    _crosshair = cm.generate();
    _crosshair_np = NodePath(_crosshair);

    Texture* tex = TexturePool::load_texture(TEXTURE_PATH "crosshair_2.png", 4);
    tex->set_wrap_u(SamplerState::WM_border_color);
    tex->set_wrap_v(SamplerState::WM_border_color);
    tex->set_border_color(Color::transparent());

    for (int i=0; i<4; ++i) {

        NodePath path = _root.attach_new_node("rotation");
        NodePath crosshair_np = path.attach_new_node(_crosshair);

        crosshair_np.set_bin("transparent", CullBin::BT_unsorted);
        crosshair_np.set_transparency(TransparencyAttrib::M_alpha);
        crosshair_np.set_texture(tex);
        path.set_r(i * 90.0f);
    }
}


void PlayerHUD::fire(const Event* event) {
    
    nassertv(event->get_num_parameters() > 0)
    EventParameter param = event->get_parameter(0);

    Gun* gun = DCAST(Gun, param.get_ptr());
    _crosshair_value += 0.1;

    set_ammo(gun->get_num_rounds());
}


void PlayerHUD::reload(const Event* event) {

    Weapon* weapon = _player->get_weapon();

    if (weapon->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, weapon);
        
        _total_rounds = _player->get_ammo(gun->get_ammo());
        set_ammo(gun->get_num_rounds());
    }
}


void PlayerHUD::take_gun(const Event* event) {

    nassertv(event->get_num_parameters() > 0)
    EventParameter param = event->get_parameter(0);

    Gun* gun = DCAST(Gun, param.get_ptr());

    _total_rounds = _player->get_ammo(gun->get_ammo());
    _crosshair_scale = gun->get_recoil() * 0.3;

    set_ammo(gun->get_num_rounds());
}


void PlayerHUD::set_ammo(size_t ammo) {
    
    _ammo_counter->set_text(to_string(ammo) + '/' + to_string(_total_rounds));
}



    // CardMaker cm("fullscreen-quad");

    // cm.set_frame_fullscreen_quad();

    // _view_quad = _root_np.attach_new_node(cm.generate());

    // _view_quad.set_depth_write(false);
    // _view_quad.set_depth_test(false);
    // _view_quad.set_material_off(true);
    // _view_quad.set_two_sided(true);


    // Shader* shader = Shader::load(  SHADER_TYPE, 
    //                                 VERT_SHADER("quad"),
    //                                 FRAG_SHADER("texture"));

    // _view_quad.set_shader(shader);




    // _root_np = NodePath("root");
    // _camera = new Camera("hud-cam");

    // OrthographicLens* lens = new OrthographicLens;
    // static const PN_stdfloat left = -1.0f;
    // static const PN_stdfloat right = 1.0f;
    // static const PN_stdfloat bottom = -1.0f;
    // static const PN_stdfloat top = 1.0f;
    // lens->set_film_size(right - left, top - bottom);
    // lens->set_film_offset((right + left) * 0.5, (top + bottom) * 0.5);
    // lens->set_near_far(-1000, 1000);
    // _camera->set_lens(lens);

    // _camera_np = _root_np.attach_new_node(_camera);

    // // _scene = new Scene2D(_root_np);
    // // _scene->set_camera_node(_camera);