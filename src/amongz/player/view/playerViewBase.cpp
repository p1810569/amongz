// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include "pbrPipeline.h"
#include "renderPipeline.h"
#include "viewport.h"
#include "game.h"
#include "playerHUD.h"
#include "player.h"
#include "viewportManager.h"

#include "playerViewBase.h"


using namespace std;



PlayerViewBase::PlayerViewBase(Player* player):
    PandaNode("player-view"),
    _player(player),
    _viewport(make_viewport()),
    _hud(player, _viewport)
{
    _hud.local_object();

    _viewport->set_event_queue(_player->get_event_queue());
    _viewport->set_event_name("dimension-changed");

    EventHandler* handler = _player->get_event_handler();
    handler->add_hook("dimension-changed", callback(update_viewport));


#if USE_RENDER_PIPELINE
    _pipeline = make_pipeline(_viewport);
    _buffer = _pipeline->get_buffer();
#else
    _pipeline = nullptr;
    _buffer = _viewport->get_window();

    _buffer->set_clear_color_active(true);
    _buffer->set_clear_depth_active(true);
    _buffer->set_clear_stencil_active(true);

    _buffer->set_clear_color(LColor(0.2,0.2,0.2,1.0));
#endif
}


void PlayerViewBase::add_region(DisplayRegion* region) {

    _regions.push_back(region);    
}


void PlayerViewBase::remove_region(DisplayRegion* region) {

    ViewRegions::iterator it = find(_regions.begin(), _regions.end(), region);

    if (it != _regions.end())
        _regions.erase(it);
}


void PlayerViewBase::set_event_queue(EventQueue* queue) {

    _viewport->set_event_queue(queue);    
}


void PlayerViewBase::init() {
    
    if (_pipeline != nullptr)
        _hud.set_view_texture(_pipeline->init());
}


Player* PlayerViewBase::get_player() const {
    
    return _player;
}


PlayerViewBase::~PlayerViewBase() {
    
    if (_viewport != nullptr)
        _viewport->stash();
}


void PlayerViewBase::update_viewport(const Event* event) {

    GraphicsOutput* window = _viewport->get_window();
    int width = 0, height = 0;

    if (window->has_size()) {
        height = _viewport->get_pixel_height();
        width = _viewport->get_pixel_width();
    }

    if (height == 0)
        return;
    
    float aspect_ratio = (float)width / (float)height;

    _hud.update_region();
    
#if USE_RENDER_PIPELINE
    _pipeline->set_dimensions(width, height);
#endif

    LVector4 dimensions = _viewport->get_dimensions();

    for (DisplayRegion* region: _regions) {
        NodePath camera_np = region->get_camera();

#if !USE_RENDER_PIPELINE
        region->set_dimensions(dimensions);
#endif
        if (camera_np.is_empty())
            continue;

        Camera* camera = DCAST(Camera, camera_np.node());

        int lens_index = region->get_lens_index();
        Lens* lens = camera->get_lens(lens_index);

        lens->set_aspect_ratio(aspect_ratio);
    }
}


Viewport* PlayerViewBase::make_viewport() {

    Game* game = Game::get_global_ptr();
    ViewportManager* mgr = game->get_viewport_manager();
    
    return mgr->acquire_viewport();
}


RenderPipeline* PlayerViewBase::make_pipeline(Viewport* viewport) {
    
    GraphicsOutput* buffer = viewport->get_window();
    LVecBase2i size = viewport->get_pixel_size();

    auto pipeline = new RenderPipeline("render-pipeline", buffer, size);

    make_pbr_pipeline(pipeline);

    return pipeline;
}