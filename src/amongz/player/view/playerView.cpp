// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include <animControlCollection.h>  // for AnimControlCollection
#include <loader.h>                 // for Loader
#include <graphicsOutput.h>         // for GraphicsOutput
#include <stencilAttrib.h>          // for StencilAttrib
#include <renderAttrib.h>           // for RenderAttrib, RenderAttrib::M_always
#include <renderState.h>            // for RenderState
#include <camera.h>
#include <auto_bind.h>
#include <cLerpAnimEffectInterval.h>
#include <characterJointEffect.h>
#include <pStatTimer.h>
#include <shaderAttrib.h>

#include "game.h"
#include "map.h"
#include "player.h"
#include "viewportManager.h"
#include "renderPipeline.h"
#include "viewport.h"
#include "playerHUD.h"
#include "gun.h"
#include "pbrPipeline.h"
#include "sight.h"
#include "render_utils.h"
#include "playerView.h"

using namespace std;


DEFINE_TYPEHANDLE(PlayerView)
DEFINE_TYPEHANDLE(PlayerView::ViewLayer)


PlayerView::PlayerView(Player* player):
    PlayerViewBase(player),
    LocalNodePath(this),
    _anim_interface(this),
    _player_np(player),
    _mask(DrawMask::bit(PLAYER_VIEW_LAYER)),
    _foreground_fov(75.0),
    _background_fov(60.0),
    _foreground_fov_ads(_foreground_fov),
    _background_fov_ads(_background_fov),
    _view(attach_new_node("view")),
    _root(_view.attach_new_node("root"))
{

    set_pos(0,0, PLAYER_HEIGHT - PLAYER_RADIUS);
    
    _view.set_effect(new FovAdjustEffect);
    
    _foreground_layer = new ViewLayer(_buffer, this);
    _background_layer = new ViewLayer(_buffer, this);


    add_region(_background_layer);
    add_region(_foreground_layer);

    hide(DrawMask::all_on());
    show(_mask);


    init_viewarms();
    init_layers();
    init();
}



void PlayerView::init_viewarms() {

    Loader* loader = Loader::get_global_ptr();

    Filename path(MODEL_PATH);
    path.set_basename_wo_extension("us_military_viewarms");
    path.set_extension("bam");

    PT(PandaNode) model = loader->load_sync(path);

    PandaNode* node = model->get_child(0);

    _character = DCAST(Character, node);
    NodePath char_np = _root.attach_new_node(_character);

    // enable GPU skinning

    NodePath viewarms_np = char_np.find("**/-GeomNode");

    viewarms_np.set_shader(Shader::load(SHADER_TYPE,
            SHADER_PATH VERT_SHADER("skinning"),
            SHADER_PATH FRAG_SHADER("basic")), 20);

    PandaNode* viewarms = viewarms_np.node();

    const RenderAttrib* attrib = 
        viewarms->get_attrib(ShaderAttrib::get_class_slot());
    
    viewarms->set_attrib(DCAST(ShaderAttrib, attrib)->set_flag(
                        ShaderAttrib::F_hardware_skinning, true));
}


void PlayerView::init_layers() {

    // using two differents lenses for each layer allows more control on the
    // the field of view of each layer separately

    PerspectiveLens* bg_lens = new PerspectiveLens();

    bg_lens->set_fov(_background_fov);
    bg_lens->set_film_size(1.0,1.0);
    bg_lens->set_near(0.01);
    bg_lens->set_far(500.0);

    PerspectiveLens* fg_lens = new PerspectiveLens(*bg_lens);

    fg_lens->set_fov(_foreground_fov);


    _foreground_layer->set_lens(fg_lens);
    _background_layer->set_lens(bg_lens);


    // foreground layer should be rendered before background layer writes to 
    // the depth buffer, aside frome preventing view model to clip through,
    // it optimizes depth occlusion since model covers a good part of the screen
    _foreground_layer->set_sort(-2);
    _background_layer->set_sort(-1);

    _background_layer->disable_clears();
    _foreground_layer->disable_clears();


    CPT(RenderAttrib) fg_attr = 
        StencilAttrib::make(true,
            RenderAttrib::M_always,
            StencilAttrib::SO_zero,
            StencilAttrib::SO_replace,
            StencilAttrib::SO_replace,
            1, 0, 1);

    CPT(RenderAttrib) bg_attr =
        StencilAttrib::make(true,
            RenderAttrib::M_equal,
            StencilAttrib::SO_keep,
            StencilAttrib::SO_keep,
            StencilAttrib::SO_keep,
            0, 1, 0);
    

    CPT(RenderAttrib) sattr = 
        ShaderAttrib::make(Shader::load( SHADER_TYPE,
                    SHADER_PATH VERT_SHADER("basic"),
                    SHADER_PATH FRAG_SHADER("basic")), 10);


    CPT(RenderState) fg_state = RenderState::make(fg_attr);
    CPT(RenderState) bg_state = RenderState::make(bg_attr);
    CPT(RenderState) state = RenderState::make(sattr);


    // foreground layer sets the stencil mask so that view models dont go
    // through scene's geometry, for instance when you stand close to a wall 
    _foreground_layer->set_initial_state(fg_state->compose(state));
    _background_layer->set_initial_state(bg_state->compose(state));

    // background layer shouldn't render the foreground models
    _foreground_layer->set_camera_mask(_mask);
    _background_layer->set_camera_mask(_mask ^ Camera::get_all_camera_mask());

    // foreground layer must only render the view models, so starting traversal
    // from this node prevents from uselessly traversing the whole scene
    _foreground_layer->set_scene(*this);

    CharacterJoint* tag_cam = _character->find_joint("tag_camera");
    nassertv(tag_cam != nullptr)

    tag_cam->add_net_transform(_foreground_layer->get_camera());
    tag_cam->add_net_transform(_background_layer->get_camera());

    _foreground_layer->set_scissor_enabled(false);
    _background_layer->set_scissor_enabled(false);



    // 3rd person camera test


    // Map* map = get_current_map();
    // Camera* cam = new Camera("test cam");
    // PerspectiveLens* lens = new PerspectiveLens();
    // lens->set_near(0.01);
    // cam->set_lens(lens);
    // NodePath cam_np = map->attach_new_node(cam);
    // cam_np.set_pos(3,3,3);
    // cam_np.look_at(0,0,1.5);
    // _background_layer->set_camera(cam_np);
    // _background_layer->disable_clears();
}


void PlayerView::update() {

    if (_weapon == nullptr)
        return;

    float t = _anim_interface.get_ads_value();
    _player->set_ads(t);
    
    float fg_fov = lerp(_foreground_fov, _foreground_fov_ads, t);
    float bg_fov = lerp(_background_fov, _background_fov_ads, t);

    _foreground_layer->set_fov(fg_fov);
    _background_layer->set_fov(bg_fov);

    _anim_interface.set_sway_factor(lerp(0.5, 0.2, t),
                                    lerp(0.3, 0.2, t));

    _anim_interface.set_idle_sway_factor(lerp(1.0, 0.5, t));

    _anim_interface.update();
    _hud.update();
}


void PlayerView::take_gun(Gun* gun, bool first_time) {

    Game* game = Game::get_global_ptr();

    // replace current weapon
    if (!_weapon_np.is_empty()) {
        Map* map = game->get_current_map();
        _weapon_np.reparent_to(*map);
    }

    _weapon = gun;
    _weapon_np = NodePath(_weapon);

    _weapon_np.reparent_to(_root);

    NodePath path = _weapon_np.find("**/-Character");

    if (path.is_empty())
        return;

    Character* rig = DCAST(Character, path.node());
    _part_handle = rig->get_bundle_handle(0);

    _character->merge_bundles(_character->get_bundle_handle(0), _part_handle);

    _anim_interface.set_gun(gun, _character->get_bundle(0));

    _anim_interface.pullout(first_time);

    Sight* sight = DCAST(Sight, gun->get_attachment(Sight::get_class_type()));

    if (sight != nullptr) {
        _foreground_fov_ads = sight->get_foreground_fov();
        _background_fov_ads = sight->get_background_fov();
    } else {
        _foreground_fov_ads = _foreground_fov;
        _background_fov_ads = _background_fov;
    }
}


void PlayerView::ads(bool enable) {

    _anim_interface.ads(enable);
}


void PlayerView::fire() {

    _anim_interface.fire(_player->get_ads() > 0.5f);
}


void PlayerView::reload() {

    if (_weapon == nullptr)
        return;

    if (_weapon->is_of_type(Gun::get_class_type())) {
        Gun* gun = DCAST(Gun, _weapon);

        size_t num_rounds = gun->get_num_rounds();
        _anim_interface.reload(num_rounds == 0);
    }
}

void PlayerView::set_scene(NodePath scene) {

    _background_layer->set_scene(scene);
}


void PlayerView::set_ads_height(float height) {
    
    _anim_interface.set_ads_height(height);
}

void PlayerView::set_view_move(float x, float y) {
    
    _anim_interface.set_weapon_sway(x, y);
}

void PlayerView::ViewLayer::set_fov(float fov) {

    Lens* lens = get_lens();

    if (lens != nullptr)
        lens->set_fov(fov);
}


PlayerView::ViewLayer::ViewLayer(GraphicsOutput* buffer, PlayerView* view): 
    DisplayRegion(buffer, LVector4(0,1,0,1)), 
    _camera(new Camera("view-layer"))
{
    set_camera(view->attach_new_node(_camera));
}


PlayerView::ViewLayer::~ViewLayer() {
    
    NodePath cam(_camera);
    cam.remove_node();
}

Camera* PlayerView::ViewLayer::get_camera() const {
    return _camera;    
}

Lens* PlayerView::ViewLayer::get_lens() const {
    return _camera->get_lens(get_lens_index());
}

void PlayerView::ViewLayer::set_lens(Lens* lens) {
    _camera->set_lens(lens);
}

void PlayerView::ViewLayer::set_lens(int index, Lens* lens) {
    _camera->set_lens(index, lens);
}

void PlayerView::ViewLayer::set_initial_state(const RenderState* state) {
    _camera->set_initial_state(state);
}

void PlayerView::ViewLayer::set_camera_mask(DrawMask mask) {
    _camera->set_camera_mask(mask);
}

void PlayerView::ViewLayer::set_scene(NodePath scene) {
    _camera->set_scene(scene);
}



bool PlayerView::FovAdjustEffect::has_adjust_transform() const {
    return true;
}


void PlayerView::FovAdjustEffect::adjust_transform(
                            CPT(TransformState)& net_transform,
                            CPT(TransformState)& node_transform,
                            const PandaNode *node) const
{
    const PlayerView* view = DCAST(PlayerView, node->get_parent(0));

    Lens* fg_lens = view->_foreground_layer->get_lens();
    Lens* bg_lens = view->_background_layer->get_lens();

    LMatrix4 fg_proj = fg_lens->get_projection_mat();
    LMatrix4 bg_proj = bg_lens->get_projection_mat_inv();

    node_transform = node_transform->compose(
        TransformState::make_mat(fg_proj * bg_proj));
}

















// void PlayerView::r_replace_character(CharacterJoint* node) {
    
//     auto effect = node->get_effect(CharacterJointEffect::get_class_type());

//     if (effect != nullptr) {
//         const CharacterJointEffect* je = DCAST(CharacterJointEffect, effect);
//         if (je->get_character() == old) {
//             node->set_effect(CharacterJointEffect::make(this));
//             // node->clear_effect(CharacterJointEffect::get_class_type());
//         }
//     }
//     PandaNode::Children children = node->get_children();

//     for (size_t i=0; i<children.size(); ++i)
//         r_replace_joint_effect(children.get_child(i), old);
// }


// void PlayerView::ViewLayer::ref() {
    
//     DisplayRegion::ref();
//     Camera::ref();
// }


// bool PlayerView::ViewLayer::unref() {
    
//     bool res = false;

//     if (DisplayRegion::get_ref_count() > 0)
//         res |= DisplayRegion::unref();

//     if (Camera::get_ref_count() > 0)
//         res |= Camera::unref();

//     return res;
// }










// PlayerView::GunAnchor::GunAnchor(Character *character, PartGroup* parent): 
//     CharacterJoint(character, character->get_bundle(0), 
//                     parent, "gun_anchor", LMatrix4::ident_mat())
// {

// }

// PlayerView::GunAnchor::~GunAnchor() {
    
// }

// void PlayerView::GunAnchor::detach_gun() {

//     _children.clear();
// }

// bool PlayerView::GunAnchor::attach_gun(Character* rig) {

//     CharacterJointBundle* bundle = rig->get_bundle(0);
//     CharacterJoint* base_joint = rig->find_joint("base");

//     if (bundle != nullptr) {
//         nout << "attaching anchor" << endl;
//         _children.push_back(base_joint);
//         // Character* chara = get_character();
//         // CharacterJoint* joint = chara->find_joint("tag_weapon");
//         // PartBundle* root = chara->get_bundle(0);
//         // Thread* thread = Thread::get_current_thread();

//         // joint->update_internals(root, chara->find_joint("tag_torso"), false, false, thread);
//         // update_internals(root, joint, true, false, thread);
//         // base->update_internals(root, this, true, true, thread);
//         return true;
//     }
//     return false;
// }

// void PlayerView::GunAnchor::r_replace_character(CharacterJoint* joint) {
    

//     NodePathCollection nodes = joint->get_local_transforms();
//     nodes.add_paths_from(joint->get_net_transforms());

//     for (size_t i=0; i<nodes.size(); ++i) {
//         PandaNode* node = nodes.get_path(i).node();
//         node->set_effect(CharacterJointEffect::make(get_character()));
//     }

//     int num_children = joint->get_num_children();

//     for (int i=0; i<num_children; ++i) {
//         PartGroup* child = joint->get_child(i);
//         if (child->is_character_joint())
//             r_replace_character(DCAST(CharacterJoint, child));
//     }

// }


// void PlayerView::GunAnchor::get_blend_value(const PartBundle *root) {

// }


// bool PlayerView::GunAnchor::update_internals(PartBundle *root, PartGroup *parent, 
//             bool self_changed, bool parent_changed, Thread *current_thread) {
            
//     return CharacterJoint::update_internals(root, parent, self_changed, parent_changed, current_thread);
//     bool net_changed = false;

//     if (parent_changed || self_changed) {
//         CharacterJoint *parent_joint = DCAST(CharacterJoint, parent);

//         _net_transform = parent_joint->_net_transform;
//         net_changed = true;
//     }

//     if (net_changed) {
//         CPT(TransformState) t = TransformState::make_mat(_net_transform);

//         NodePathCollection nodes = get_net_transforms();
//         for (size_t i=0; i<nodes.size(); ++i) {

//             PandaNode* node = nodes.get_path(i).node();
//             node->set_transform(t, current_thread);
//         }
        

//         // Recompute the transform used by any vertices animated by this joint.
//         _skinning_matrix = _initial_net_transform_inverse * _net_transform;

//     }

//     if (self_changed) {
//                CPT(TransformState) t = TransformState::make_mat(_value);

//         NodePathCollection nodes = get_local_transforms();
//         for (size_t i=0; i<nodes.size(); ++i) {
            
//             PandaNode* node = nodes.get_path(i).node();
//             node->set_transform(t, current_thread);
//         }
//     }


//     return self_changed || net_changed;
// }
