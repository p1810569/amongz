// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __COLLISIONDETECTION_H__
#define __COLLISIONDETECTION_H__

#include <register_type.h>
#include <bulletSoftBodyNode.h>
#include <bulletRigidBodyNode.h>

#include "collisionHandler.h"
#include "utils.h"


class BulletContactCallbackData;
class PandaNode;

template<class T>
class CollisionDetection: public T {

public:
    using Type = CollisionDetection<T>;

    template <class... Args,
        std::enable_if_t<std::is_constructible<T, Args&&...>::value, int> = 0>
    CollisionDetection(Args&&... args) : T(std::forward<Args>(args)...) {}
    virtual ~CollisionDetection() = default;

    virtual void do_callback(BulletPersistentManifold* manifold, 
                            PandaNode* other) {}

    static void post_init(TypeHandle type) {
        CollisionHandler::register_handler(type, handler);
    }

private:
    static void handler(BulletPersistentManifold* manifold, 
                        PandaNode* object, PandaNode* other)
    {
        static_cast<Type*>(object)->do_callback(manifold, other);
    }
};

#endif // __COLLISIONDETECTION_H__