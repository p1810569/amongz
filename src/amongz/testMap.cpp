// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include <bulletPlaneShape.h>
#include <bulletWorld.h>
#include <pointLightNode.h>
#include <aiWorld.h>
#include <texturePool.h>
#include <bulletCapsuleShape.h>


#include "defines.h"
#include "player.h"
#include "fsm.h"
#include "playerControllerInput.h"
#include "gun.h"
#include "config_weapon.h"
#include "game.h"
#include "viewportManager.h"
#include "barrel.h"
#include "mag.h"
#include "stock.h"
#include "grip.h"
#include "chargingHandle.h"
#include "suppressor.h"
#include "ammoPack.h"
#include "assetManager.h"
#include "cardMaker.h"
#include "redDotSight.h"
#include "ammoData.h"
#include "zombieAIController.h"
#include "zombie_utils.h"
#include "hitBox.h"
#include "testMap.h"


using namespace std;



TestMap::TestMap(const string& name): Map(name) {
    
    BulletPlaneShape* plane_shape = new BulletPlaneShape(LVector3::up(), 0.0);
    BulletRigidBodyNode* ground_node = new BulletRigidBodyNode("ground");
    
    _physic_world->attach(ground_node);
    NodePath ground_np = attach_new_node(ground_node);

    ground_node->add_shape(plane_shape);

    ground_np.set_collide_mask(CollideMask::bit(DEFAULT_COLLISION_MASK));


    BulletDebugNode* debug = new BulletDebugNode("Debug");
    debug->show_wireframe(true);
    debug->show_bounding_boxes(true);
    debug->show_normals(false);
    NodePath debug_np = attach_new_node(debug);

    debug_np.show();

    _physic_world->set_debug_node(debug);


    PointLightNode* light = new PointLightNode("main light");
    light->set_radius(3);
    light->set_color(0,1,0);
    light->set_energy(1.5f);

    NodePath light_np = attach_new_node(light);
    light_np.set_pos(-2,1,1);


    PointLightNode* light2 = new PointLightNode("main light2");
    light2->set_radius(5);
    light2->set_color(0,1,0);
    light2->set_energy(1.5f);

    NodePath light_np2 = attach_new_node(light2);
    light_np2.set_pos(5,2,1);

    CardMaker cm("ground");
    cm.set_uv_range(LTexCoord(0,0), LTexCoord(100,100));
    cm.set_frame_fullscreen_quad();

    NodePath ground = attach_new_node(cm.generate());
    ground.set_scale(500.0);
    ground.set_pos(0.0);
    ground.set_hpr(0,-90,0);
    // ground.set_two_sided(true);

    Texture* grid = TexturePool::load_texture("/texture/test_grid.png");

    if (grid != nullptr)
        ground.set_texture(grid);
}


void TestMap::init() {

    Game* game = Game::get_global_ptr();
    ViewportManager* mgr = game->get_viewport_manager();

    mgr->acquire_viewports(1);


    PlayerController* controller1 = new PlayerControllerInput("player1");
    add_controller(controller1);

    // PlayerController* controller2 = new PlayerControllerInput("player2");
    // add_controller(controller2);


    // PlayerController* controller3 = new PlayerControllerInput("player3");
    // add_controller(controller3);

    // clock->set_mode(ClockObject::M_non_real_time);
    // clock->set_dt(0.016);
    
    for (int i=0; i<5; ++i) {

        PT(ZombieController) zombie = make_zombie<ZombieAIController>("zombie1");
        zombie->reparent_to(*this);

        zombie->set_x(((PN_stdfloat)rand() / (PN_stdfloat)RAND_MAX) * 10.0f);
        zombie->set_y(((PN_stdfloat)rand() / (PN_stdfloat)RAND_MAX) * 10.0f);
    }
}
