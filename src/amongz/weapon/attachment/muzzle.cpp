// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <loader.h>
#include <texturePool.h>
#include <shaderPool.h>
#include <bulletRigidBodyNode.h>
#include <bulletSphereShape.h>

#include "game.h"
#include "map.h"
#include "gun.h"
#include "ammoData.h"
#include "bulletWorld.h"
#include "assetManager.h"
#include "muzzle.h"


using namespace std;

DEFINE_TYPEHANDLE(Muzzle)


void Muzzle::register_with_read_factory() {

    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}


PandaNode* Muzzle::make_copy() const {
    return new Muzzle(*this);
}


float Muzzle::get_recoil_factor() const {
    return _data->_recoil_factor;
}

float Muzzle::get_length() const {
    return _data->_length;
}


void Muzzle::fire(Gun* gun) {

    Map* map = get_current_map();
    BulletWorld* world = get_physic_world();


    Bullet* bullet = new Bullet(world, gun->get_ammo());
    NodePath bullet_np = map->attach_new_node(bullet);

    bullet->set_into_collide_mask(CollideMask::bit(BULLET_COLLISION_MASK));

    NodePath np(this);

    CPT(TransformState) transform = np.get_transform(*map);
    LMatrix4 mat = transform->get_mat();
    
    LVector4 dir = mat.get_row(1);
    mat.set_row(3, mat.get_row(3) + dir*_data->_length);

    bullet->set_linear_velocity(dir.get_xyz() *  gun->get_muzzle_velocity());
    bullet_np.set_mat(mat);
    bullet_np.reparent_to(*map);
}


void Muzzle::write_datagram(BamWriter* manager, Datagram& me) {
    
    Attachment::write_datagram(manager, me);

    me.add_float32(_data->_recoil_factor);
    me.add_float32(_data->_length);
}


void Muzzle::fillin(DatagramIterator& scan, BamReader *manager) {

    Attachment::fillin(scan, manager);

    _data->_recoil_factor = scan.get_float32();
    _data->_length = scan.get_float32();
}


TypedWritable* Muzzle::make_from_bam(const FactoryParams& params) {

    Muzzle* me = new Muzzle;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}


Muzzle::Muzzle(const string& name):
    Attachment(name),
    _data(new Data)
{
    
}


Muzzle::Muzzle(const Muzzle& copy): 
    Attachment(copy),
    _data(copy._data) {

}