// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __STOCK_H__
#define __STOCK_H__


#include "attachment.h"
#include "gdk/utils.h"


class Stock: public Attachment {

    REGISTER_TYPE("Stock", Attachment)

public:
    Stock(const Stock& copy);
    virtual ~Stock() = default;

    float get_recoil_factor() const;

    class Data: public ReferenceCount {
    public:
        Data() = default;
        ~Data() = default;

        float _recoil_factor;
    };

    const PT(Data) _data;

    static void register_with_read_factory();

    virtual PandaNode* make_copy() const override;

protected:
    Stock(const std::string& name="");

    void write_datagram(BamWriter* manager, Datagram& me);
    void fillin(DatagramIterator& scan, BamReader *manager);

    using Namable::set_name;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);
};

#endif // __STOCK_H__