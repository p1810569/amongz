

// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SUPRESSOR_H__
#define __SUPRESSOR_H__


#include "muzzle.h"
#include "utils.h"



class Suppressor: public Muzzle {

    REGISTER_TYPE("Suppressor", Muzzle)

public:
    Suppressor(const Suppressor& copy);
    virtual ~Suppressor() = default;

    float get_velocity_factor() const;
    float get_noise_reduction() const;

    static void register_with_read_factory();

    virtual PandaNode* make_copy() const override;

protected:
    Suppressor(const std::string& name="");

    class Data: public ReferenceCount {
    public:
        Data() = default;
        ~Data() = default;
        float _velocity_factor;
        float _noise_reduction;
    };

    const PT(Data) _data;

    void write_datagram(BamWriter* manager, Datagram& me);
    void fillin(DatagramIterator& scan, BamReader *manager);

    using Namable::set_name;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);
};

#endif // __SUPRESSOR_H__