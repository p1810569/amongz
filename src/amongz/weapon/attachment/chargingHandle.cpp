// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.



#include <loader.h>
#include <texturePool.h>
#include <shaderPool.h>
#include "attachmentAnchor.h"

#include "assetManager.h"
#include "chargingHandle.h"

using namespace std;

DEFINE_TYPEHANDLE(ChargingHandle)


ChargingHandle::ChargingHandle(const string& name): Attachment(name) {
    
}


ChargingHandle::ChargingHandle(const ChargingHandle& copy): 
    Attachment(copy)
{
    
}

void ChargingHandle::register_with_read_factory() {
    
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

PandaNode* ChargingHandle::make_copy() const {
    return new ChargingHandle(*this);
}


TypedWritable* ChargingHandle::make_from_bam(const FactoryParams& params) {
    
    ChargingHandle* me = new ChargingHandle;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}
