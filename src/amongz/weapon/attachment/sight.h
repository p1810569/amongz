

// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SIGHT_H__
#define __SIGHT_H__


#include <loader.h>
#include <texturePool.h>
#include <shaderPool.h>

#include "attachment.h"
#include "utils.h"


class Sight: public Attachment {

    REGISTER_TYPE("Sight", Attachment)

public:
    Sight(const Sight& copy);
    virtual ~Sight() = default;

    float get_foreground_fov() const;
    float get_background_fov() const;
    float get_height() const;

    static void register_with_read_factory();

    virtual PandaNode* make_copy() const override;

protected:
    Sight(const std::string& name="");

    class Data: public ReferenceCount {
    public:
        Data() = default;
        ~Data() = default;

        float _fg_fov;
        float _bg_fov;
        float _height;
    };

    const PT(Data) _data;

    virtual void write_datagram(BamWriter* manager, Datagram& me) override;
    virtual void fillin(DatagramIterator& scan, BamReader *manager) override;

    using Namable::set_name;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);
};

#endif // __SIGHT_H__