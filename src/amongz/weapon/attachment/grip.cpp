// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <loader.h>
#include <texturePool.h>
#include <shaderPool.h>

#include "assetManager.h"
#include "grip.h"


using namespace std;

DEFINE_TYPEHANDLE(Grip)



void Grip::register_with_read_factory() {

    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

PandaNode* Grip::make_copy() const {
    return new Grip(*this);
}

float Grip::get_recoil_factor() const {
    return _data->_recoil_factor;
}


void Grip::write_datagram(BamWriter* manager, Datagram& me) {
    
    Attachment::write_datagram(manager, me);
    me.add_float32(_data->_recoil_factor);
}


void Grip::fillin(DatagramIterator& scan, BamReader *manager) {

    Attachment::fillin(scan, manager);
    _data->_recoil_factor = scan.get_float32();
}


TypedWritable* Grip::make_from_bam(const FactoryParams& params) {

    Grip* me = new Grip;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}


Grip::Grip(const string& name):
    Attachment(name),
    _data(new Data)
{
    
}


Grip::Grip(const Grip& copy): 
    Attachment(copy),
    _data(copy._data) {

}