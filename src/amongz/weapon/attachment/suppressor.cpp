// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "suppressor.h"


using namespace std;

DEFINE_TYPEHANDLE(Suppressor)




void Suppressor::register_with_read_factory() {
    
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

PandaNode* Suppressor::make_copy() const {
    return new Suppressor(*this);
}


void Suppressor::write_datagram(BamWriter* manager, Datagram& me) {
    
    Muzzle::write_datagram(manager, me);

    me.add_float32(_data->_velocity_factor);
    me.add_float32(_data->_noise_reduction);
}


void Suppressor::fillin(DatagramIterator& scan, BamReader *manager) {

    Muzzle::fillin(scan, manager);

    _data->_velocity_factor = scan.get_float32();
    _data->_noise_reduction = scan.get_float32();
}


TypedWritable* Suppressor::make_from_bam(const FactoryParams& params) {

    Suppressor* me = new Suppressor;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}


Suppressor::Suppressor(const string& name): 
    Muzzle(name),
    _data(new Data)
{
    
}


float Suppressor::get_noise_reduction() const {
    return _data->_noise_reduction;
}

float Suppressor::get_velocity_factor() const {
    return _data->_velocity_factor;
}


Suppressor::Suppressor(const Suppressor& copy): 
    Muzzle(copy),
    _data(copy._data)
{
    
}
