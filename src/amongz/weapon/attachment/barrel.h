

// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __BARREL_H__
#define __BARREL_H__

#include "attachment.h"
#include "utils.h"


class Barrel: public Attachment {

    REGISTER_TYPE("Barrel", Attachment)

public:
    Barrel(const Barrel& copy);
    virtual ~Barrel() = default;

    bool attach(AttachmentBundle* bundle, AttachmentAnchor* anchor) override;

    float get_velocity_factor() const;
    std::string get_gun() const;

    static void register_with_read_factory();

    virtual PandaNode* make_copy() const override;

protected:
    Barrel(const std::string& name="");
    
    class Data: public ReferenceCount {
    public:
        Data() = default;
        ~Data() = default;

        float _velocity_factor;
        std::string _gun;
    };

    const PT(Data) _data;

    void write_datagram(BamWriter* manager, Datagram& me);
    void fillin(DatagramIterator& scan, BamReader *manager);

    using Namable::set_name;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);
};

#endif // __BARREL_H__