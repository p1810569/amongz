// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CHARGINGHANDLE_H__
#define __CHARGINGHANDLE_H__

#include "attachment.h"



class ChargingHandle: public Attachment {

    REGISTER_TYPE("ChargingHandle", Attachment)

public:
    ChargingHandle(const ChargingHandle& copy);
    virtual ~ChargingHandle() = default;

    static void register_with_read_factory();

    virtual PandaNode* make_copy() const override;

protected:
    ChargingHandle(const std::string& name="");

    using Namable::set_name;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);
};

#endif // __CHARGINGHANDLE_H__