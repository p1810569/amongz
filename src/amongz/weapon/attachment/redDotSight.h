// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __REDDOTSIGHT_H__
#define __REDDOTSIGHT_H__

#include <textureCollection.h>

#include "color.h"
#include "sight.h"


class RedDotSight: public Sight {

    REGISTER_TYPE("RedDotSight", Sight)

public:
    RedDotSight(const RedDotSight& copy);
    virtual ~RedDotSight() = default;

    static void register_with_read_factory();

    static size_t get_num_crosshair_texture();
    static Texture* get_crosshair_texture(size_t id);
    static Texture* get_crosshair_texture(const std::string& name);

    Color get_default_color() const;
    Texture* get_default_texture() const;

    void set_crosshair(size_t id);
    size_t get_crosshair() const;
    
    Texture* get_texture() const;

    void set_color(Color color);
    Color get_color() const;

    virtual PandaNode* make_copy() const override;

protected:
    RedDotSight(const std::string& name="");

    class Data: public ReferenceCount {
    public:
        Data() = default;
        ~Data() = default;

        Color _default_color;
        Texture* _default_texture;
    };

    const PT(Data) _data;

    virtual void write_datagram(BamWriter* manager, Datagram& me) override;
    virtual void fillin(DatagramIterator& scan, BamReader *manager) override;
    virtual int complete_pointers(TypedWritable **plist, BamReader *manager) override;
    virtual void finalize(BamReader* reader) override;

    virtual void r_copy_children(const PandaNode* from, InstanceMap& inst_map,
                               Thread* current_thread) override;

    using Namable::set_name;

    NodePath _lens_np;

    static void load_textures();

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);

    static TextureCollection _crosshairs;
    static bool _texture_loaded;

    Color _crosshair_color;
    Texture* _crosshair_texture;

    size_t _crosshair_id;
};

#endif // __REDDOTSIGHT_H__