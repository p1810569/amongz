// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennaoura, Nicolas Leray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#include <bulletSphereShape.h>    // for BulletSphereShape
#include <bulletWorld.h>          // for BulletWorld
#include <nodePath.h>             // for NodePath
#include "defines.h"              // for PLAYER_COLLISION_MASK
#include "game.h"                 // for Game
#include "bullet.h"
#include "utils.h"
#include "map.h"
#include "ammoData.h"

using namespace std;


DEFINE_TYPEHANDLE(Bullet)

CallbackTask* Bullet::_clear_bullets;
Bullet::Bullets Bullet::_bullets;
double Bullet::_lifespan = BULLET_LIFESPAN;


Bullet::Bullet(BulletWorld* world, AmmoData* ammo): 
    RigidBodyCallback("bullet"),
    _ammo(ammo),
    _world(world)
{

    ClockObject* clock = ClockObject::get_global_clock();
    _timeout = clock->get_long_time() + _lifespan;

    if (_clear_bullets == nullptr)
        setup_clear_task();


    notify_collisions(true);
    set_mass(ammo->get_mass());

    world->attach_rigid_body(this);

    float radius = ammo->get_radius() * 10.0;

    add_shape(new BulletSphereShape(radius));

    set_ccd_motion_threshold(1e-7);
    set_ccd_swept_sphere_radius(radius);

    _bullets.push_back(this);
    _it = prev(_bullets.end());
}


Bullet::Type Bullet::get_bullet_type() const {
    
    return BT_standard;
}


void Bullet::do_callback(BulletPersistentManifold* manifold, 
                            PandaNode* other) {

    if (_it == _bullets.end())
        return;

    remove(_it);
}


float Bullet::get_damage() const {
    
    return _ammo->get_mass() * _ammo->get_damage_factor();
}


Bullet::Bullets::iterator Bullet::remove(Bullets::iterator it) {

    nassertr(it != _bullets.end(), _bullets.end())
    Bullet* bullet = *it;

    if (bullet->get_num_parents())
        bullet->get_parent(0)->remove_child(bullet);

    if (PT(BulletWorld) world = bullet->_world.lock())
        world->remove_rigid_body(bullet);

    bullet->_it = _bullets.end();

    return _bullets.erase(it);
}


void Bullet::setup_clear_task() {

    nassertv(_clear_bullets == nullptr)

    AsyncTaskManager* mgr = AsyncTaskManager::get_global_ptr();

    _clear_bullets = new CallbackTask(clear_bullets, "clear_bullets");
    _clear_bullets->set_sort(41);

#if THREADED_PHYSIC
    _clear_bullets->set_task_chain("physic");
#endif

    mgr->add(_clear_bullets);
}


AsyncTask::DoneStatus Bullet::clear_bullets(AsyncTask* task) {
    
    ClockObject* clock = ClockObject::get_global_clock();
    double current_time = clock->get_long_time();
    
    Bullets::iterator it = _bullets.begin();

    while (it != _bullets.end()) {
        Bullet* bullet = *it;
        
        if (current_time >= bullet->_timeout) {
            it = bullet->remove(it);
        } else
            ++it;
    }
    return AsyncTask::DS_cont;
}
