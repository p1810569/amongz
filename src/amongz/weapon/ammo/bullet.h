// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __BULLET_H__
#define __BULLET_H__

#include "physic_utils.h"
#include "callbacks.h"

class BulletWorld;
class AmmoData;


class Bullet: public RigidBodyCallback  {

    REGISTER_TYPE_POST_INIT("Bullet", RigidBodyCallback)

public:
    enum Type {
        BT_standard         = 0x0,
        BT_armor_piercing   = 0x1<<0,
        BT_tracer           = 0x1<<1,
        BT_incendiary       = 0x1<<2,
        BT_explosive        = 0x1<<3,
        BT_hollow_point     = 0x1<<4
    };

    Bullet(BulletWorld* world, AmmoData* ammo);
    virtual ~Bullet() = default;

    virtual Type get_bullet_type() const;

    void do_callback(BulletPersistentManifold* manifold, 
                    PandaNode* other) override;

    virtual float get_damage() const;

private:
    PT(AmmoData) _ammo;
    WPT(BulletWorld) _world;
    double _timeout;
    

    static void setup_clear_task();
    static AsyncTask::DoneStatus clear_bullets(AsyncTask* task);
    static CallbackTask* _clear_bullets;

    typedef plist<PT(Bullet)> Bullets;
    static Bullets _bullets;
    static double _lifespan;

    static Bullets::iterator remove(Bullets::iterator it);
    Bullets::iterator _it;
};


inline Bullet::Type operator|(Bullet::Type a, Bullet::Type b) {

    int res = static_cast<int>(a) | static_cast<int>(b);
    return static_cast<Bullet::Type>(res);
}

#endif // __BULLET_H__