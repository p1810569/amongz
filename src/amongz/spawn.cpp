// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Among Z. If not, see <http://www.gnu.org/licenses/>.

#include "spawn.h"

using namespace std;

DEFINE_TYPEHANDLE(Spawn)
Singleton<Spawn::ObjectFactory> Spawn::_factory;


Spawn::Spawn(TypeHandle type, const std::string& name): PandaNode(name),
    _type(type)
{
    
}


Spawn::Spawn(const Spawn& copy): PandaNode(copy),
    _type(copy._type)
{

}


PandaNode* Spawn::spawn(FactoryParams params) {

    nassertr(_type != TypeHandle::none(), nullptr)

    float x = (float)rand() / (float)RAND_MAX - 0.5f;
    float y = (float)rand() / (float)RAND_MAX - 0.5f;

    PandaNode* node = _factory->make_instance(_type, params);

    if (node == nullptr)
        return nullptr;

    LMatrix4 mat = get_transform()->get_mat();
    LVector4 pos =  mat.xform(LVector4(x, y, 0.0, 1.0));

    node->set_transform(TransformState::make_pos(pos.get_xyz()));

    return node;
}


TypedWritable* Spawn::make_from_bam(const FactoryParams& params) {
    
    Spawn* me = new Spawn;
    DatagramIterator scan;
    BamReader* manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);

    return me;
}

void Spawn::set_area(float width, float height) {
    
    CPT(TransformState) transform = get_transform();
    set_transform(transform->set_scale2d(LVector2(width, height)));
}


LVector2 Spawn::get_area() const {
    
    return get_transform()->get_scale2d();
}


Spawn::ObjectFactory* Spawn::get_factory() {
    
    if (!_factory)
        _factory = new ObjectFactory;
    
    return _factory;
}


void Spawn::register_with_read_factory() {
    
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}


void Spawn::set_spawn_type(TypeHandle type) {

    _type = type;    
}

TypeHandle Spawn::get_spawn_type() const {
    
    return _type;
}


PandaNode* Spawn::make_copy() const {

    return new Spawn(*this);    
}


void Spawn::fillin(DatagramIterator& scan, BamReader *manager) {
    
    PandaNode::fillin(scan, manager);
    
    TypeRegistry* registry = TypeRegistry::ptr();
    _type = registry->find_type(scan.get_string());
}


Spawn::Spawn(): PandaNode("spawn"),
    _type(TypeHandle::none())
{
    
}


void Spawn::write_datagram(BamWriter* manager, Datagram& me) {
    
    PandaNode::write_datagram(manager, me);
    me.add_string(_type.get_name());
}