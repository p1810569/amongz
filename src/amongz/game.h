
// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __GAME_H__
#define __GAME_H__


#include <audioManager.h>
#include "player.h"
#include "defines.h"
#include "framework.h"
#include "callbacks.h"


class ViewportManager;
class EventHandler;
class DataNode;
class InputInterfaceManager;
class AudioManager;
class GraphicsWindow;
class InputDevice;
class Map;
class GamePlaceHolder;
class AsyncTask;
class AIWorld;
class BulletWorld;


class Game final: public Framework, public TaskPool {

public:
    ~Game() override;

    static Game* get_global_ptr();

    ExitStatus finish() override;
    
    GraphicsWindow* get_window() const;
    
    void close_window();
    
    ViewportManager*  get_viewport_manager() const;
    DataNode* get_data_root() const;

    Map* get_current_map();


private:
    Game();
    Game(const Game&) = delete;
    Game& operator =(const Game&) = delete;
    

    AsyncTask::DoneStatus process_input(AsyncTask *task);
    AsyncTask::DoneStatus process_data(AsyncTask *task);
    AsyncTask::DoneStatus process_event(AsyncTask *task);
    AsyncTask::DoneStatus process_interval(AsyncTask *task);
    AsyncTask::DoneStatus process_render(AsyncTask *task);
    AsyncTask::DoneStatus process_audio(AsyncTask *task);
    AsyncTask::DoneStatus process_garbage_collect(AsyncTask *task);

    void process_window_event(const Event* event);
    
    
    PT(DataNode) _data_root;
    PT(GraphicsWindow) _window;
    PT(AudioManager) _audio_mgr;
    PT(Map) _current_map;

    ViewportManager* _viewport_mgr;
    EventHandler* _event_handler;

    void setup();

    static Singleton<Game> _global_ptr;

    friend class GamePlaceHolder;
};

BulletWorld* get_physic_world();
AIWorld* get_ai_world();
Map* get_current_map();


#endif // __GAME_H__