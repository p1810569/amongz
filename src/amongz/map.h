
// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MAP_H__
#define __MAP_H__

#include <typedWritableReferenceCount.h>
#include <asyncTask.h>
#include <nodePath.h>
#include "callbacks.h"


class BulletWorld;
class Player;
class AIWorld;
class PlayerController;
class BulletBodyNode;
class Spawn;


class Map: public PandaNode, public LocalNodePath, public TaskPool {

    REGISTER_TYPE("Map", PandaNode, LocalNodePath)

public:
    Map(const std::string& name="");
    Map(const Map& map) = delete;
    virtual ~Map();

    virtual void init();

    virtual void add_controller(PlayerController* player);
    
    virtual void remove_controller(PlayerController* player);
    virtual void remove_controller(size_t id);

    virtual bool has_controller(PlayerController* controller) const;

    virtual PlayerController* make_controller(const std::string& name);

    void add_bullet_for_removal(BulletBodyNode* node);

    size_t get_num_controller() const;
    PlayerController* get_controller(size_t id) const;

    BulletWorld* get_physic_world() const;
    AIWorld* get_ai_world() const;

    using NodePath::get_name;
    using NodePath::prepare_scene;

    static Map* load(const std::string& name);

    static void register_with_read_factory();

protected:

    virtual void fillin(DatagramIterator& scan, BamReader* manager);
    virtual void write_datagram(BamWriter* manager, Datagram& me);
    virtual int complete_pointers(TypedWritable** p_list, BamReader* reader) override;
    virtual void finalize(BamReader* reader) override;

    AIWorld* _ai_world;
    PT(BulletWorld) _physic_world;

    typedef pvector<PT(Spawn)> ZombieSpawns;
    typedef pvector<std::string> ZombieNames;

    ZombieSpawns _spawns;
    ZombieNames _zombies;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);
    
    void load_zombies();
    void load_collide_mask();

    typedef pvector<PT(PlayerController)> PlayerControllers;
    typedef plist<PT(BulletBodyNode)> NodesToRemove;

    PlayerControllers _controllers;
    size_t _num_bodies; // only used for reading bam file

    AsyncTask::DoneStatus update_physic(AsyncTask *task);
    AsyncTask::DoneStatus update_ai(AsyncTask *task);

    AsyncTask::DoneStatus spawn_zombies(AsyncTask *task);

};

#endif // __MAP_H__