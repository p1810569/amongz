// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "pipelineNode.h"

using namespace std;



OutputPortBase::OutputPortBase(const string& name, 
    TypeHandle type, PipelineNode* node): Port(name, type, node) {
    
    node->add_output(this);
}


bool OutputPortBase::connect(PipelineNode* child, const string& name) {

    nassertr(child != nullptr, false);

    InputPortBase* port = child->get_input(name);
    nassertr(port != nullptr, false)

    return connect(port);
}


bool OutputPortBase::connect(InputPortBase* port) {

    nassertr(port != nullptr && port->get_data_type() == get_data_type(), false)
    port->_endpoint = this;

    return true;
}


void OutputPortBase::disconnect(InputPortBase* port) {
    
    nassertv(port != nullptr)
    port->_endpoint = nullptr;
}


void* OutputPortBase::get_data() const {
    
    return _ptr;
}


void OutputPortBase::set_data(void* data) {

    _ptr = data;    
}