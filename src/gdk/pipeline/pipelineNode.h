// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PIPELINENODE_H__
#define __PIPELINENODE_H__



#include <unordered_map>
#include <texture.h>
#include "IOPort.h"
#include "utils.h"


class PipelineNode: public Namable, public TypedWritableReferenceCount {

    REGISTER_TYPE("PipelineNode", Namable, TypedWritableReferenceCount)

public:

    PipelineNode(const std::string& name);
    ~PipelineNode();

    bool connect(PipelineNode* other, const std::string& output, 
                const std::string& input);

    typedef std::unordered_map<std::string, PT(InputPortBase)> InputPorts;
    typedef std::unordered_map<std::string, PT(OutputPortBase)> OutputPorts;

    int get_num_input() const;
    int get_num_output() const;

    InputPortBase* get_input(const std::string& name);
    OutputPortBase* get_output(const std::string& name);

    const InputPorts get_inputs() const;
    const OutputPorts get_outputs() const;


protected:
    
    virtual void do_traverse();

    InputPortBase* add_input(const std::string& name, 
                            TypeHandle type=get_type_handle(Texture));

    OutputPortBase* add_output(const std::string& name, 
                            TypeHandle type=get_type_handle(Texture));
 

    void add_input(InputPortBase* input);
    void add_output(OutputPortBase* output);

    template<class T>
    void set_output_data(const std::string& name, T* data) {

        OutputPortBase* port = get_output(name);
        nassertv(port != nullptr)

        port->set_data(data);
    }

    template<class T>
    T* get_input_data(const std::string& name) {

        InputPortBase* port = get_input(name);
        nassertr(get_type_handle(T) == port->get_data_type() && port != nullptr, nullptr)
        
        void* data = port->get_data();
        return port->get_data();
    }

    void collect_inputs();

private:
    bool _visited;

    InputPorts _inputs;
    OutputPorts _outputs;

    friend class InputPortBase;
    friend class OutputPortBase;
    friend class RenderStage;
    friend class RenderPipeline;
};

#endif // __PIPELINENODE_H__