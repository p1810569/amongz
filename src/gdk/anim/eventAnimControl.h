// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Among Z. If not, see <http://www.gnu.org/licenses/>.

#ifndef __EVENTANIMCONTROL_H__
#define __EVENTANIMCONTROL_H__

#include <animControl.h>
#include <cMetaInterval.h>
#include <nameUniquifier.h>

#include "callbacks.h"

class EventAnim;


class EventAnimControl: public AnimControl {

    REGISTER_TYPE("EventAnimControl", AnimControl)

public:
    EventAnimControl(const std::string& name, PartBundle* part, 
                    CIntervalManager* manager=nullptr);
    
    ~EventAnimControl();

    double get_duration() const;

protected:
    virtual void animation_activated();

private:
    void initialize();
    AsyncTask::DoneStatus check_interval(AsyncTask* task);

    PT(CMetaInterval) _interval;
    CIntervalManager* _mgr;
    CallbackTask _check_task;
    bool _initialized;

    static NameUniquifier _names;
};

#endif // __EVENTANIMCONTROL_H__