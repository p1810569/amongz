// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Among Z. If not, see <http://www.gnu.org/licenses/>.

#include <animBundle.h>
#include "eventAnim.h"


using namespace std;

DEFINE_TYPEHANDLE(EventAnim)



void EventAnim::write_datagram(BamWriter* manager, Datagram& me) {

    AnimGroup::write_datagram(manager, me);

    size_t num_tags = _tags.size();

    me.add_uint16(num_tags);

    for (size_t i=0; i<num_tags; ++i) {
        me.add_float64(_tags[i]._timestamp);
        me.add_string(_tags[i]._name);
    }
}


void EventAnim::fillin(DatagramIterator& scan, BamReader* manager) {
    
    AnimGroup::fillin(scan, manager);

    size_t num_tags = scan.get_uint16();

    for (size_t i=0; i<num_tags; ++i) {
        EventTag tag;
        tag._timestamp = scan.get_float64();
        tag._name = scan.get_string();
        _tags.push_back(tag);
    }

    sort(_tags.begin(), _tags.end());
}


TypedWritable* EventAnim::make_from_bam(const FactoryParams& params) {
    
    EventAnim* me = new EventAnim;
    DatagramIterator scan;
    BamReader *manager;

    parse_params(params, scan, manager);
    me->fillin(scan, manager);
    return me;
}


AnimGroup* EventAnim::make_copy(AnimGroup* parent) const {
    
    return new EventAnim(parent, *this);
}


size_t EventAnim::get_num_tags() const {
    
    return _tags.size();
}


EventAnim::EventTag EventAnim::get_tag(size_t id) const {
    
    nassertr(id < _tags.size(), EventTag())
    return _tags[id];
}


EventAnim::EventTag EventAnim::get_tag(const string& name) const {
    
    for (const EventTag& tag: _tags) {
        if (tag._name == name)
            return tag;
    }
    return EventTag();
}


void EventAnim::register_with_read_factory() {
    
    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}


EventAnim::EventAnim(AnimGroup* parent, const EventAnim& copy): 
    AnimGroup(parent, copy),
    _tags(copy._tags)
{
    
}


EventAnim::EventAnim(AnimGroup* parent, const string& name):
    AnimGroup(parent, name)
{
    
}
