// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Among Z. If not, see <http://www.gnu.org/licenses/>.

#include <cInterval.h>
#include <partBundle.h>
#include <cIntervalManager.h>
#include <asyncTaskManager.h>

#include "eventAnimControl.h"
#include "eventAnim.h"


using namespace std;

DEFINE_TYPEHANDLE(EventAnimControl)

NameUniquifier EventAnimControl::_names;



void EventAnimControl::animation_activated() {

    AnimControl::animation_activated();

    if (!_initialized) initialize();


    if (_interval != nullptr) {

        if (is_playing()) {
            AsyncTaskManager* mgr = AsyncTaskManager::get_global_ptr();

            _interval->start(get_full_fframe(), -1.0, get_play_rate());
            if (!mgr->has_task(&_check_task))
                mgr->add(&_check_task);
        } 
        else {
            _interval->clear_to_initial();
            _check_task.remove();
        }
    }   
}


void EventAnimControl::initialize() {

    nassertv(!_initialized)
    _initialized = true;

    AnimBundle* bundle = get_anim();
    double duration = get_duration();

    for (size_t i=0; i<bundle->get_num_children(); ++i) {

        AnimGroup* group = bundle->get_child(i);

        if (!group->is_of_type(EventAnim::get_class_type()))
            continue;
        
        EventAnim* anim = DCAST(EventAnim, group);

        size_t num_tags = anim->get_num_tags();

        double prev = 0.0;

        if (_interval == nullptr) {
            _interval = new CMetaInterval(_names.add_name(get_name()));

            if (_mgr != nullptr)
                _interval->set_manager(_mgr);
        }

        for (size_t i=0; i<num_tags; ++i) {
            EventAnim::EventTag tag = anim->get_tag(i);

            double timestamp = min(tag._timestamp, duration);
            double time = timestamp - prev;
            prev = timestamp;

            CInterval* interval = new CInterval(tag._name, time, true);
            interval->set_done_event(tag._name);

            if (_mgr != nullptr)
                interval->set_manager(_mgr);

            _interval->add_c_interval(interval);
        }
    }
}


AsyncTask::DoneStatus EventAnimControl::check_interval(AsyncTask* task) {

    nassertr(_interval != nullptr, AsyncTask::DS_done)

    if (!is_playing()) {
        _interval->clear_to_initial();
        return AsyncTask::DS_done;
    }
    return AsyncTask::DS_cont;    
}


EventAnimControl::EventAnimControl(const string& name, PartBundle* part, 
                                    CIntervalManager* manager):
    AnimControl(name, part, 1.0f, 0),
    _check_task(named_method(check_interval)),
    _initialized(false),
    _mgr(manager)
{
    _check_task.local_object();
}


EventAnimControl::~EventAnimControl() {
    
    _check_task.remove();
}


double EventAnimControl::get_duration() const {
    
    return (double)get_num_frames() / get_frame_rate();
}