#include <dconfig.h>
#include "config_anim.h"
#include "eventAnim.h"
#include "eventAnimControl.h"


ConfigureDef(config_anim);

ConfigureFn(config_anim) {
    init_anim();
}


void init_anim() {
    static bool initialized = false;
    if (initialized)
        return;

    initialized = true;

    EventAnim::init_type();
    EventAnimControl::init_type();

    EventAnim::register_with_read_factory();
}
