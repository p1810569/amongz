// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with Among Z. If not, see <http://www.gnu.org/licenses/>.

#ifndef __EVENTANIM_H__
#define __EVENTANIM_H__

#include <animBundle.h>
#include "utils.h"


class EventAnim: public AnimGroup {

    REGISTER_TYPE("EventAnim", AnimGroup)

protected:
    EventAnim() = default;
    EventAnim(AnimGroup* parent, const EventAnim& copy);

public:
    EventAnim(AnimGroup* parent, const std::string& name);
    virtual ~EventAnim() = default;

    struct EventTag {
        std::string _name;
        double _timestamp;

        bool operator <(const EventTag& other) const {
            return _timestamp < other._timestamp;
        }
    };

    size_t get_num_tags() const;
    EventTag get_tag(size_t id) const;
    EventTag get_tag(const std::string& name) const;

    static void register_with_read_factory();

protected:
    virtual void write_datagram(BamWriter* manager, Datagram& me) override;
    virtual void fillin(DatagramIterator& scan, BamReader* manager) override;
    virtual AnimGroup* make_copy(AnimGroup* parent) const override;

    typedef pvector<EventTag> Tags;
    Tags _tags;

private:
    static TypedWritable* make_from_bam(const FactoryParams& params);
};

#endif // __EVENTANIM_H__