#include <dconfig.h>
#include "config_gdk.h"
#include "config_light.h"
#include "config_anim.h"


ConfigureDef(config_gdk);

ConfigureFn(config_gdk) {
    init_light();
}


void init_gdk() {
    static bool initialized = false;
    if (initialized)
        return;

    initialized = true;

    init_light();
    init_anim();
}
