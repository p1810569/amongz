// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <graphicsEngine.h>
#include <displayRegionCullCallbackData.h>
#include <displayRegionDrawCallbackData.h>
#include <displayRegion.h>

#include "renderPipeline.h"
#include "renderStage.h"

using namespace std;


NameUniquifier RenderStage::_names(".", "stage");

DEFINE_TYPEHANDLE(RenderStage)
DEFINE_TYPEHANDLE(RenderStage::RenderCallback)


RenderStage::RenderCallback::RenderCallback(RenderStage* stage): 
    _stage(stage)
{
    
}



void RenderStage::RenderCallback::do_callback(CallbackData *data) {


    if (data->is_of_type(DisplayRegionCullCallbackData::get_class_type())){

        auto cbdata = DCAST(DisplayRegionCullCallbackData, data);

        if (PT(RenderStage) stage = _stage.lock())
            stage->cull(cbdata);
        else {
            SceneSetup* setup = cbdata->get_scene_setup();
            setup->get_display_region()->clear_cull_callback();
        }
    }

    else if (data->is_of_type(DisplayRegionDrawCallbackData::get_class_type())){

        auto cbdata = DCAST(DisplayRegionDrawCallbackData, data);

        if (PT(RenderStage) stage = _stage.lock())
            stage->draw(cbdata);
        else {
            SceneSetup* setup = cbdata->get_scene_setup();
            setup->get_display_region()->clear_cull_callback();
        }
    }

    else if (data->is_of_type(RenderCallbackData::get_class_type())) {
        
        auto cbdata = DCAST(RenderCallbackData, data);
        
        if (PT(RenderStage) stage = _stage.lock())
            stage->render(cbdata);
        else
            cbdata->get_window()->clear_render_callback();
    }
}


void RenderStage::do_traverse() {
    
    setup();
    _pipeline->add_stage(this);
}


void RenderStage::setup() {
    
}


RenderStage::RenderStage(const string& name, RenderPipeline* pipeline): 
    PipelineNode(_names.add_name(name)),
    _callback(new RenderCallback(this)),
    _pipeline(pipeline)
{

}


RenderStage::RenderStage(const RenderStage& copy): 
    PipelineNode(_names.add_name(copy.get_name())),
    _pipeline(copy._pipeline),
    _callback(new RenderCallback(this))
{
    
}


void RenderStage::hook_region(DisplayRegion* region, CBtype type) {

    nassertv(region != nullptr)

    if (type & CB_draw)
        region->set_draw_callback(_callback);
    
    if (type & CB_cull)
        region->set_cull_callback(_callback);
}


void RenderStage::cull(DisplayRegionCullCallbackData* data) {
    
    data->upcall();
}


void RenderStage::draw(DisplayRegionDrawCallbackData* data) {
    
    data->upcall();
}


void RenderStage::render(RenderCallbackData* data) {

    data->upcall();
}


RenderPipeline* RenderStage::get_pipeline() const {

    return _pipeline;    
}


GraphicsOutput* RenderStage::add_buffer(FrameBufferProperties fb_prop,
    const string& name, int size_x, int size_y, int sort, GraphicsOutput* host)
{

    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();
    
    auto* buffer =  engine->make_output( host->get_pipe(), name, sort, fb_prop, 
                                WindowProperties::size(size_x, size_y), 
                                GraphicsPipe::BF_refuse_window | 
                                GraphicsPipe::BF_refuse_parasite |
                                GraphicsPipe::BF_fb_props_optional |
                                GraphicsPipe::BF_resizeable,
                                host->get_gsg(), host);

    _buffers.push_back(buffer);

    return buffer;
}


void RenderStage::add_buffer(GraphicsOutput* buffer) {

    _buffers.push_back(buffer);    
}


GraphicsOutput* RenderStage::get_buffer() const {
    
    if (_buffers.size() > 0)
        return _buffers[0];
    
    return nullptr;
}


GraphicsOutput* RenderStage::get_buffer(size_t n) const {
    
    nassertr(n < _buffers.size(), nullptr)
    return _buffers[n];
}


GraphicsOutput* RenderStage::get_buffer(const string& name) const {

    for (GraphicsOutput* buffer: _buffers) {
        if (buffer->get_name() == name)
            return buffer;
    }
    return nullptr;
}


size_t RenderStage::get_num_buffers() const {
    
    return _buffers.size();
}


void RenderStage::remove_buffer(GraphicsOutput* buffer) {
    
    Buffers::iterator found = find(_buffers.begin(), _buffers.end(), buffer);
    nassertv(found != _buffers.end())
    _buffers.erase(found);
}


void RenderStage::remove_buffer(size_t n) {

    nassertv(n < _buffers.size())
    _buffers.erase(next(_buffers.begin(), n));
}


int RenderStage::get_index() const {
    
    return _pipeline->get_stage_index(this);
}


GraphicsOutput* RenderStage::add_callback(int sort) {

    
    GraphicsOutput* host = _pipeline->get_host();
    
    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();
    
    FrameBufferProperties fb_props;
    fb_props.clear();

    WindowProperties win_props;
    win_props.clear_size();

    auto buffer = engine->make_output(  host->get_pipe(), get_name(), 
                                    sort, fb_props, win_props, 
                                    GraphicsPipe::BF_require_callback_window,
                                    host->get_gsg(), host);

    add_buffer(buffer);

    if (buffer->is_of_type(CallbackGraphicsWindow::get_class_type())) {
        CallbackGraphicsWindow* window = DCAST(CallbackGraphicsWindow, buffer);
        window->set_render_callback(_callback);
    }

    return buffer;
}
