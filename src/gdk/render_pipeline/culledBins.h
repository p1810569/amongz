// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CULLEDBINS_H__
#define __CULLEDBINS_H__


#include <typedWritableReferenceCount.h>
#include <typeHandle.h>
#include <cullBin.h>
#include <sceneSetup.h>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <map>


class CulledBins: public ReferenceCount {

public:
    CulledBins() = default;
    ~CulledBins() = default;
  

    class Group: public ReferenceCount {
    public:
        Group() = default;
        ~Group() = default;

        typedef std::vector<PT(CullBin)> CullBins;
        typedef std::map<PT(SceneSetup), CullBins> Setups;
        typedef std::set<std::string> BinNames;

        void clear();
        virtual void draw() const;
        void add_bin(SceneSetup* setup, CullBin* bin);
        void add_bin_name(const std::string& name);
        void remove_bin_name(const std::string& name);
        bool has_bin_name(const std::string& name) const;

        Setups get_setups();
    
    protected:
        Setups _setups;
        BinNames _bins;
    };

    size_t add_bin_to_groups(SceneSetup* setup, CullBin* bin);

    Group* add_group(const std::string& name);
    void remove_group(const std::string& name);

    Group* get_group(const std::string& name) const;

    void clear();

private:
    typedef std::map<std::string, PT(Group)> Groups;
    Groups _groups;
};

#endif // __CULLEDBINS_H__