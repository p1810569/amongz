// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __RENDERSTAGE_H__
#define __RENDERSTAGE_H__


#include <callbackObject.h>
#include <callbackGraphicsWindow.h>
#include <nameUniquifier.h>
#include <string>

#include "pipelineNode.h"

class DisplayRegion;
class RenderPipeline;
class DisplayRegionDrawCallbackData;
class DisplayRegionCullCallbackData;


class RenderStage: public PipelineNode {

    REGISTER_TYPE("RenderStage", PipelineNode)

public:
    using RenderCallbackData = CallbackGraphicsWindow::RenderCallbackData;
    
    RenderStage(const std::string& name, RenderPipeline* pipeline);
    RenderStage(const RenderStage& copy);
    virtual ~RenderStage() = default;

    enum CBtype {
        CB_cull = 0x1<<0,
        CB_draw = 0x1<<1
    };

    void hook_region(DisplayRegion* region, CBtype type=CB_cull);
    virtual void cull(DisplayRegionCullCallbackData* data);
    virtual void draw(DisplayRegionDrawCallbackData* data);
    virtual void render(RenderCallbackData* data);

    RenderPipeline* get_pipeline() const;

    size_t get_num_buffers() const;

    GraphicsOutput* get_buffer() const;
    GraphicsOutput* get_buffer(size_t n) const;
    GraphicsOutput* get_buffer(const std::string& name) const;

    void remove_buffer(GraphicsOutput* buffer);
    void remove_buffer(size_t n);

    int get_index() const;

protected:
    void add_buffer(GraphicsOutput* buffer);

    GraphicsOutput* add_callback(int sort=0);

    GraphicsOutput* add_buffer(FrameBufferProperties fb_prop,
        const std::string& name, int size_x, int size_y, int sort=0, 
        GraphicsOutput* host=nullptr);
    

    class RenderCallback: public CallbackObject {
        
        REGISTER_TYPE("RenderStage::RenderCallback", CallbackObject)

    public:
        ALLOC_DELETED_CHAIN(RenderCallback);

        RenderCallback(RenderStage* stage);
        ~RenderCallback() = default;

        virtual void do_callback(CallbackData *data);

    private:
        WPT(RenderStage) _stage;
    };

    PT(RenderCallback) _callback;
    RenderPipeline* _pipeline;

    typedef std::vector<PT(GraphicsOutput)> Buffers;
    Buffers _buffers;
    
    void do_traverse() final;
    virtual void setup();

private:
    friend class RenderPipeline;

    static NameUniquifier _names;
    using Namable::set_name;
};

#endif // __RENDERSTAGE_H__