// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "renderPipeline.h"

#include <displayRegionCullCallbackData.h>
#include <cullResult.h>
#include <sceneSetup.h>
#include <graphicsStateGuardian.h>
#include <displayRegion.h>
#include <cullBinManager.h>
#include <thread.h>
#include <graphicsPipeSelection.h>
#include <dataGraphTraverser.h>


using namespace std;

DEFINE_TYPEHANDLE(RenderPipeline)


RenderPipeline::RenderPipeline( const string& name, GraphicsOutput* host, 
                                LVecBase2i dimension): 
    PipelineNode(name), 
    _host(host), 
    i_final_render("final render", this),
    _dimensions(dimension)
{

    if (_dimensions == LVecBase2i(0)) {
        if (host != nullptr && host->has_size())
            _dimensions = host->get_size();
    }
}


void RenderPipeline::add_stage(RenderStage* stage) {

    _stages.push_back(stage);
}


void RenderPipeline::add_debug_stage(const string& name, Texture* texture) {
    
    nassertv(get_debug_stage(name) != nullptr)

    auto found = _debug_stages.find(name);

    if (found == _debug_stages.end())
        _debug_stages[name] = texture;
}


Texture* RenderPipeline::get_debug_stage(const string& name) {
    
    auto found = _debug_stages.find(name);

    if (found == _debug_stages.end())
        return found->second;

    return nullptr;
}


GraphicsOutput* RenderPipeline::get_buffer() const {

    return _buffer;
}


void RenderPipeline::set_buffer(GraphicsOutput* buffer) {
    
    _buffer = buffer;
}


void RenderPipeline::set_host(GraphicsOutput* host) {

    _host = host;
}


GraphicsOutput* RenderPipeline::get_host() const {
    
    return _host;
}


Texture* RenderPipeline::get_output_texture() const {
    
    return i_final_render.get_data();
}


LVecBase2i RenderPipeline::get_dimensions() const {

    return _dimensions;
}


void RenderPipeline::set_dimensions(LVecBase2i size) {
    
    if (size != _dimensions) {
        for (RenderStage* stage: _stages) {
            for (GraphicsOutput* buffer: stage->_buffers)
                buffer->set_size_and_recalc(size.get_x(), size.get_y());
        }
    }
    _dimensions = size;
}


void RenderPipeline::set_dimensions(int width, int height) {

    set_dimensions(LVecBase2i(width, height));    
}


void RenderPipeline::remove_stage(const string& name) {
    
    Stages::iterator it;

    for (it=_stages.begin(); it != _stages.end(); ++it)
        if ((*it)->get_name() == name) break;

    if (it != _stages.end()) {
        _stages.erase(it);
        init();
    }
}


void RenderPipeline::remove_stage(size_t n) {
    
    nassertv(n < _stages.size())
    _stages.erase(_stages.begin() + n);
    init();
}


void RenderPipeline::remove_stage(RenderStage* stage) {
    
    Stages::iterator found = find(_stages.begin(), _stages.end(), stage);
    nassertv(found != _stages.end())
    _stages.erase(found);
    init();
}


RenderStage* RenderPipeline::get_stage(const string& name) const {
    
    for (auto stage: _stages) {
        if (stage->get_name() == name)
            return stage;
    }
    return nullptr;
}


RenderStage* RenderPipeline::get_stage(size_t n) const {
    
    nassertr(n < _stages.size(), nullptr)
    return _stages[n];
}


size_t RenderPipeline::get_num_stage() const {

    return _stages.size();
}


int RenderPipeline::get_stage_index(const RenderStage* stage) const {
    
    Stages::const_iterator it = find(_stages.begin(), _stages.end(), stage);

    if (it != _stages.end())
        return it - _stages.begin();
    
    return -1;
}


Texture* RenderPipeline::init() {

    collect_inputs();
    sort_stages_buffers();

    return i_final_render.get_data();
}


void RenderPipeline::sort_stages_buffers() {

    nassertv(_host != nullptr)

    // we start from the host buffer sort
    int current_sort = _host->get_child_sort();

    Stages::reverse_iterator it;

    for (it=_stages.rbegin(); it != _stages.rend(); ++it) {

        RenderStage* stage = *it;
        stage->_visited = false;

        // first fist find the highest sort of the stage's buffers
        int sort_offset = 0;
        for (GraphicsOutput* buffer: stage->_buffers)
            sort_offset = max(sort_offset, buffer->get_sort());

        int base_sort = current_sort -= sort_offset + 1;

        for (GraphicsOutput* buffer: stage->_buffers) {
            int sort = base_sort + buffer->get_sort();
            current_sort = min(current_sort, sort);
            buffer->set_sort(sort);
        }
    }
}

// RenderStage* RenderPipeline::add_stage(const string& name) {
    
//     auto result = _stage_constructs.find(name);
//     nassertr(result != _stage_constructs.end(), nullptr)

//     return result->second(this);
// }


// RenderPipeline::StageConstructs RenderPipeline::_stage_constructs;
