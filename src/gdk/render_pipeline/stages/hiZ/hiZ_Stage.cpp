// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "hiZ_Stage.h"
#include <graphicsEngine.h>
#include <dataNodeTransmit.h>
#include "renderPipeline.h"
#include "render_utils.h"
#include "utils.h"


using namespace std;
using RenderCallbackData = CallbackGraphicsWindow::RenderCallbackData;


void HiZ_Stage::render(RenderCallbackData* data) {
    
    if (data->get_callback_type() != CallbackGraphicsWindow::RCT_begin_frame) {
        data->upcall();
        return;
    }

    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();
    GraphicsStateGuardian* gsg = _buffer->get_gsg();

    int size_x = get_ceil_size(_hi_z_tex->get_x_size(), SST_16);
    int size_y = get_ceil_size(_hi_z_tex->get_y_size(), SST_16);
    
    
    if (!_camera.is_null()) {
        Lens* lens = _camera->get_lens();
        _near_far[0].set_x(lens->get_near());
        _near_far[0].set_y(lens->get_far());
    }

    CPT(ShaderAttrib) attr = DCAST(ShaderAttrib, 
        _linearize.get_attrib(ShaderAttrib::get_class_type()));

    const ShaderAttrib* downscale_attr = get_shader_attrib(_downscale);
    const ShaderAttrib* linearize_attr = get_shader_attrib(_linearize);


    dispatch_compute(gsg,linearize_attr, size_x, size_y);

    for (int i=0; i<_num_lod; ++i) {

        size_x = get_ceil_size(size_x, SST_1);
        size_y = get_ceil_size(size_y, SST_1);

        _downscale.set_shader_input("u_input_depth", _hi_z_tex, true, false, -1, i);
        _downscale.set_shader_input("u_output_depth", _hi_z_tex, false, true, -1, i+1);

        dispatch_compute(gsg, downscale_attr, size_x, size_y);
    }

    data->set_render_flag(false);
}


void HiZ_Stage::set_num_lod(int n) {

    nassertv(n >= 0)
    _num_lod = n;
}


int HiZ_Stage::get_num_lod() const {

    return _num_lod;
}


void HiZ_Stage::setup() {


    LVecBase2i size = _pipeline->get_dimensions();

    _hi_z_tex->setup_2d_texture(size.get_x(), size.get_y(), 
                                Texture::T_float,
                                Texture::F_r32);

    _hi_z_tex->set_clear_color(LColor(1,0,0,1)); // reset to max depth
    _hi_z_tex->set_magfilter(SamplerState::FT_nearest_mipmap_nearest);
    _hi_z_tex->set_minfilter(SamplerState::FT_nearest_mipmap_nearest);
    _hi_z_tex->set_wrap_u(SamplerState::WM_border_color);
    _hi_z_tex->set_wrap_v(SamplerState::WM_border_color);

    _num_lod = min(_num_lod, _hi_z_tex->get_expected_num_mipmap_levels());

    o_hi_depth.set_data(_hi_z_tex.p());

    if (i_camera.has_data())
        _camera = i_camera.get_data();

    if (i_depth.has_data())
        _linearize.set_shader_input("u_input_depth", i_depth.get_data());

    _linearize.set_shader_input("u_output_depth", _hi_z_tex);
    _linearize.set_shader_input("u_near_far", _near_far);
}


HiZ_Stage::HiZ_Stage(RenderPipeline* pipeline): RenderStage("hiZ", pipeline),
    _near_far(1, LVecBase2f(0.0)),
    _linearize("linearize"), 
    _downscale("downscale"),
    i_depth("depth", this),
    i_camera("camera", this),
    o_hi_depth("hierarchical depth", this)
{    

    _buffer = add_callback();

    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();
    _hi_z_tex = new Texture("hi-z");

    _downscale_shader = Shader::load_compute(SHADER_TYPE, 
                SDK_SHADER_PATH COMP_SHADER("downscale_depth"));

    _linearize_shader = Shader::load_compute(SHADER_TYPE, 
                SDK_SHADER_PATH COMP_SHADER("linearize_depth"));


    _linearize.set_shader(_linearize_shader);
    _downscale.set_shader(_downscale_shader);

}