// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __HIZ_STAGE_H__
#define __HIZ_STAGE_H__


#include "renderStage.h"


class HiZ_Stage: public RenderStage {

    using RenderCallbackData = CallbackGraphicsWindow::RenderCallbackData;

public:
    HiZ_Stage(RenderPipeline* pipeline);
    ~HiZ_Stage() = default;

    void render(RenderCallbackData* data) override;

    void set_num_lod(int n);
    int get_num_lod() const;


    InputPort<Texture> i_depth;
    InputPort<Camera> i_camera;

    OutputPort<Texture> o_hi_depth;

private:
    int _num_lod = 7;
    PT(GraphicsOutput) _buffer;
    PT(Texture) _hi_z_tex;
    PT(Camera) _camera;
    PT(Shader) _downscale_shader;
    PT(Shader) _linearize_shader;

    NodePath _linearize;
    NodePath _downscale;
    PTA_LVecBase2f _near_far;


protected:
    virtual void setup();
};
#endif // __HIZ_STAGE_H__