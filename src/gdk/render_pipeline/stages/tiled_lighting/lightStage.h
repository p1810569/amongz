
// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __LIGHTSTAGE_H__
#define __LIGHTSTAGE_H__


#include "renderStage.h"
#include "culledBins.h"
#include "lightManager.h"
#include "lightCallbackData.h"
#include "lightMeshRender.h"



class WireRenderCallback;
// class LightDrawCallback;

class LightStage: public RenderStage {

    friend class WireRenderCallback;

    using RenderCallbackData = CallbackGraphicsWindow::RenderCallbackData;

public:

    LightStage(RenderPipeline* pipeline);
    ~LightStage() = default;

    void draw(DisplayRegionDrawCallbackData* data) override;

    InputPort<Texture> i_hi_depth;
    InputPort<Texture> i_normal;
    InputPort<Texture> i_accumulator;
    InputPort<CulledBins> i_bins;
    InputPort<SceneSetup> i_main_scene_setup;

    // OutputPort<Texture> o_accumulator;
    OutputPort<Texture> o_light_debug;


private:

    PT(SceneSetup) _main_scene_setup;

    void setup_debug_buffer();
    void setup_textures();
    void setup_shaders();

    GraphicsOutput* _buffer;

    NodePath _accumulate;
    NodePath _combine;
    NodePath _debug;
    NodePath _blit;
    
    PTA_LVecBase2f _near_far;
    PTA_LVecBase2f _view_angle;
    PTA_LVecBase2i _view_size;
    PTA_LVecBase2f _view_scale;
    PTA_LVecBase2f _half_tile_size;

    PT(CulledBins::Group) _bins;

    PT(Texture) _debug_tex;
    PT(Texture) _debug_tex2;

    static bool _light_buffer_init;


    PT(Shader) _combine_lights;
    PT(Shader) _accumulate_light;
    PT(Shader) _debug_shader;

    LightMeshRender _light_render;

    friend class LightMeshRender;

protected:
    virtual void setup() override;
};
#endif // __LIGHTSTAGE_H__