// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "lightLods.h"
#include <graphicsStateGuardian.h>
#include <stateMunger.h>
#include <cullTraverser.h>
#include <cullHandler.h>
#include <omniBoundingVolume.h>
#include <glgsg.h>
#include "lightMeshRender.h"
#include "render_utils.h"

using namespace std;


void LightLods::add_light_for_draw(int lod, int id, LMatrix4 transform) {
    
    nassertv(lod < _lods.size())

    LodData& lod_data = _lods[lod];

    lod_data._ids.push_back(id);
    lod_data._trans.push_back(transform);
}


bool LightLods::safe_to_flatten() const {

    return false;    
}


bool LightLods::safe_to_combine() const {

    return false;    
}


void LightLods::clear_lights() {
    
    for (LodData& lod: _lods) {
        lod._ids.clear();
        lod._trans.clear();
    }
}


void LightLods::set_max_num_lights(size_t n) {
    
    _max_lights = n;

    for (LodData& lod: _lods) {
        lod._ids.reserve(n);
        lod._trans.reserve(n);
    }
}


size_t LightLods::get_max_num_lights() const {

    return _max_lights;
}


size_t LightLods::get_num_lights() const {

    size_t num_lights = 0;

    for (const LodData& lod: _lods)
        num_lights += lod._ids.size();

    return num_lights;
}


void LightLods::add_lod(Geom* geom) {

    _lods.resize(_lods.size() + 1);
    
    LodData& data = _lods.back();

    data._offset.resize(1);
    data._ids.reserve(_max_lights);
    data._trans.reserve(_max_lights);

    int index = get_num_lods();


    CPT(RenderState) state = RenderState::make(
        make_shader_input("u_instance_offset", data._offset));

    add_geom(geom);
    set_geom_state(index, state);
}


size_t LightLods::get_num_lods() const {

    return get_num_geoms();    
}


void LightLods::DrawCallback::do_callback(CallbackData *cbdata) {
    
    GeomDrawCallbackData* data = DCAST(GeomDrawCallbackData, cbdata);
    GraphicsStateGuardianBase* gsg = data->get_gsg();

    if (gsg->is_of_type(GLGraphicsStateGuardian::get_class_type())) {
        PT(GLGraphicsStateGuardian) glgsg = DCAST(GLGraphicsStateGuardian, gsg);
        // glgsg->issue_memory_barrier(GL_ALL_BARRIER_BITS);
    }

    data->upcall();
}


LightLods::LightLods(): GeomNode("lights lods"), _max_lights(0),
    _draw_cb(new DrawCallback()) 
{
    
}


void LightLods::add_for_draw(CullTraverser* trav, CullTraverserData& data) {
    
    
    Geoms geoms = get_geoms(trav->get_current_thread());
    int num_lods = geoms.get_num_geoms();


    for (int i = 0; i < num_lods; ++i) {

        LodData& lod = _lods[i];
        size_t num_instance = lod._ids.size();

        if (num_instance == 0) continue;

    
        CPT(RenderState) state = data._state->compose(geoms.get_geom_state(i));
        CPT(ShaderAttrib) sattr = get_render_attrib<ShaderAttrib>(state);

        state = state->set_attrib(sattr->set_instance_count(num_instance));

        CullableObject* object = new CullableObject(
                                            geoms.get_geom(i), 
                                            move(state),
                                            TransformState::make_identity());

        object->set_draw_callback(_draw_cb);

        trav->get_cull_handler()->record_object(object, trav);
    }
}


void LightLods::collect(int32_t* ids, LMatrix4* trans, size_t& offset) {


    for (LodData& lod: _lods) {
        size_t count = lod._ids.size();

        if (count == 0)
            continue;

        lod._offset[0] = offset;
        memcpy(ids + offset, lod._ids.p(), count*sizeof(int32_t));
        memcpy(trans + offset, lod._trans.p(), count*sizeof(LMatrix4));

        offset += count;
        lod._trans.clear();
        lod._ids.clear();
    }
}



// void LightLods::set_geom(const Geom* geom) {

//     _geom = geom; 
// }


// CPT(Geom) LightLods::get_geom() const {
    
//     return _geom.p();
// }




// void LightLods::draw_lights(GraphicsStateGuardian* gsg, CPT(RenderState) state,
//                         bool force, Thread* current_thread)
// {
    
    // GeomPipelineReader geom_reader(_geom, current_thread);
    // CPT(GeomVertexData) munged_data = geom_reader.get_vertex_data();
    
    // // PT(GeomMunger) munger = gsg->get_geom_munger(state, current_thread);

    // // // if (!munger->munge_geom(_geom, munged_data, force, current_thread))
    // // //     return;
    
    // // munger->munge_geom(_geom, munged_data, force, current_thread);

    // // PT(StateMunger) state_munger = DCAST(StateMunger, munger);
    // // if (state_munger->should_munge_state())
    // //     state = state_munger->munge_state(state);

    // gsg->set_state_and_transform(state, TransformState::make_identity());
    // gsg->push_group_marker("drawing lights");

    // GeomVertexDataPipelineReader data_reader(munged_data, current_thread);
    // data_reader.check_array_readers();
    // geom_reader.draw(gsg, &data_reader, force);

    // gsg->pop_group_marker();
// }
