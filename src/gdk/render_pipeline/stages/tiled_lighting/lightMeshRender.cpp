// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <glgsg.h>
#include <cullHandler.h>
#include <omniBoundingVolume.h>
#include <displayRegionDrawCallbackData.h>
#include <vector>

#include "lightMeshRender.h"
#include "lightStage.h"
#include "lightBufferManager.h"
#include "pointLightNode.h"
#include "spotLightNode.h"
#include "geodesic.h"
#include "cone.h"
#include "renderPipeline.h"
#include "render_utils.h"


using namespace std;


LightMeshRender::Renderers LightMeshRender::_renderers;
LightMeshRender* LightMeshRender::_current_renderer;


LightMeshRender::LightMeshRender(LightStage* light_stage):
    _light_stage(light_stage),
    _assignment("assignment"),
    _transforms("transforms"),
    _hw_conservative_raster(0),
    _geom_lod(4),
    _tile_size(SST_16),
    _num_light_per_tile(256),
    _instance_offset(1,0),
    _num_rendered_lights(0),
    _max_rendered_lights(2048)
{

    RenderPipeline* pipeline = light_stage->_pipeline;
    LVecBase2i size = pipeline->get_dimensions();
    GraphicsOutput* host = pipeline->get_host();

    _num_tile_x = get_ceil_size(size.get_x(), _tile_size);
    _num_tile_y = get_ceil_size(size.get_y(), _tile_size);

    FrameBufferProperties fb_prop = FrameBufferProperties::get_default();
    fb_prop.set_force_hardware(true);
    fb_prop.set_rgb_color(false);
    fb_prop.set_stereo(false);
    fb_prop.set_accum_bits(0);
    fb_prop.set_rgba_bits(0,0,0,0);
    fb_prop.set_depth_bits(24);
    fb_prop.set_multisamples(0);
    fb_prop.set_back_buffers(0);

    _buffer = make_buffer(fb_prop, "light assignment buffer", 
                        _num_tile_x, _num_tile_y, 0, host);

    light_stage->add_buffer(_buffer);

    static Camera* cam = nullptr;

    if (cam == nullptr) {
        cam = new Camera("light_mesh_render");
        cam->set_cull_bounds(new OmniBoundingVolume());
    }

    DisplayRegion* dr = _buffer->make_mono_display_region();
    dr->set_camera(_assignment.attach_new_node(cam));
    dr->disable_clears();

    // GraphicsEngine* engine = GraphicsEngine::get_global_ptr();
    // GameFramework* game = GameFramework::get_global_ptr();

    // _buffer = engine->make_output(game->get_default_pipe(), "light rendering", 0, fb_prop,
    //                 WindowProperties::size(_num_tile_x, _num_tile_y),
    //                 GraphicsPipe::BF_require_window, host->get_gsg(), host);


    setup_buffers();
    setup_shaders();
    setup_geom_lods();

    _renderers[_light_stage->get_pipeline()->get_buffer()] = this;
}


LightMeshRender::~LightMeshRender() {

    _renderers.erase(_light_stage->get_pipeline()->get_buffer());
}


void LightMeshRender::setup_geom_lods() {

    GraphicsStateGuardian* gsg = _buffer->get_gsg();
    Thread* current_thread = Thread::get_current_thread();
    bool force = !gsg->get_effective_incomplete_render();

    LightLods& point_lods = _lights_pools[LightData::T_point];
    LightLods& spot_lods = _lights_pools[LightData::T_spot];

    point_lods.set_max_num_lights(_max_rendered_lights);
    spot_lods.set_max_num_lights(_max_rendered_lights);

    for (LightLods& lods: _lights_pools)
        _assignment.attach_new_node(&lods);
    
    for (int i=0; i<_geom_lod; ++i) {

        point_lods.add_lod(new Geodesic(i));
        spot_lods.add_lod(new Cone(i));
    }
    // _assignment.ls();
}



void LightMeshRender::setup_buffers() {

    _light_transforms   = new Texture("light-transforms");
    _light_ids          = new Texture("light-ids");
    _light_counter      = new Texture("light-counter");
    _tile_buffer        = new Texture("tile-buffer");

    _tiles_ids          = new Texture("tiles-ids");
    _tiles_min_depth    = new Texture("tiles-min-depth");
    _tiles_max_depth    = new Texture("tiels-max-depth");


    _light_transforms->setup_buffer_texture(_max_rendered_lights*4,
                                        Texture::T_float,
                                        Texture::F_rgba32,
                                        GeomEnums::UH_dynamic);

    _light_ids->setup_buffer_texture(   _max_rendered_lights,
                                        Texture::T_unsigned_int,
                                        Texture::F_r32,
                                        GeomEnums::UH_dynamic);

    _light_counter->setup_2d_texture(   _num_tile_x, _num_tile_y, 
                                        Texture::T_unsigned_int, 
                                        Texture::F_r32i);
    
    _tile_buffer->setup_2d_texture_array(
                                        _num_tile_x, _num_tile_y, 
                                        _num_light_per_tile, 
                                        Texture::T_float,
                                        Texture::F_rgba32);

    _tiles_ids->setup_2d_texture_array(
                                        _num_tile_x, _num_tile_y, 
                                        _num_light_per_tile, 
                                        Texture::T_unsigned_int,
                                        Texture::F_r32i);

    _tiles_min_depth->setup_2d_texture_array(
                                        _num_tile_x, _num_tile_y, 
                                        _num_light_per_tile, 
                                        Texture::T_float,
                                        Texture::F_r32);

    _tiles_max_depth->setup_2d_texture_array(
                                        _num_tile_x, _num_tile_y, 
                                        _num_light_per_tile, 
                                        Texture::T_float,
                                        Texture::F_r32);

}


void LightMeshRender::setup_shaders() {


    LightBufferManager* mgr = DCAST(LightBufferManager,
                                    LightManager::get_global_ptr());


    GraphicsStateGuardian* gsg = _buffer->get_gsg();

    if (gsg->has_extension("GL_NV_conservative_raster"))
        _hw_conservative_raster = GL_CONSERVATIVE_RASTERIZATION_NV;
    else if (gsg->has_extension("GL_INTEL_conservative_rasterization"))
        _hw_conservative_raster = GL_CONSERVATIVE_RASTERIZATION_INTEL;


    if (_hw_conservative_raster)
        _light_assignment = Shader::load(SHADER_TYPE, 
                SDK_SHADER_PATH VERT_SHADER("light"),
                SDK_SHADER_PATH FRAG_SHADER("light_assignment"));
    else
        _light_assignment = Shader::load(SHADER_TYPE, 
                SDK_SHADER_PATH VERT_SHADER("light_conservative"),
                SDK_SHADER_PATH FRAG_SHADER("light_assignment_conservative"),
                SDK_SHADER_PATH GEOM_SHADER("conservative"));


    _update_transforms = Shader::load_compute(SHADER_TYPE,
                SDK_SHADER_PATH COMP_SHADER("update_transforms"));

    _assignment.set_shader(_light_assignment);
    _assignment.set_shader_input("lightBuffer", mgr->get_buffer());
    _assignment.set_shader_input("u_near_far", _light_stage->_near_far);
    _assignment.set_shader_input("u_view_scale", _light_stage->_view_scale);
    _assignment.set_shader_input("u_half_pixel_size", _light_stage->_half_tile_size);
    _assignment.set_shader_input("u_lights_tiles", _tile_buffer);
    _assignment.set_shader_input("u_lights_ids", _tiles_ids);
    _assignment.set_shader_input("u_lights_min", _tiles_min_depth);
    _assignment.set_shader_input("u_lights_max", _tiles_max_depth);
    _assignment.set_shader_input("u_light_counter", _light_counter);
    _assignment.set_shader_input("u_output", _light_stage->_debug_tex2);
    _assignment.set_shader_input("u_ids", _light_ids);

    _assignment.set_bin("lights", CullBin::BT_unsorted);
    _assignment.set_depth_write(false);
    _assignment.set_depth_test(false);
    _assignment.set_two_sided(true);


    _transforms.set_shader(_update_transforms);
    _transforms.set_shader_input("u_transforms", _light_transforms);
    _transforms.set_shader_input("lightBuffer", mgr->get_buffer());
    _transforms.set_shader_input("u_ids", _light_ids);
}



void LightMeshRender::draw_lights(DisplayRegionDrawCallbackData* data) {
    
    // update the transform buffer
    PTA_uchar ids_buf = _light_ids->modify_ram_image();
    PTA_uchar trans_buf = _light_transforms->modify_ram_image();

    int32_t* ids = (int32_t*)ids_buf.p();
    LMatrix4* trans = (LMatrix4*)trans_buf.p();
    
    size_t offset = 0;

    for (LightLods& lods: _lights_pools)
        lods.collect(ids, trans, offset);


    GraphicsStateGuardian* gsg = _buffer->get_gsg();
    dispatch_compute(gsg, get_shader_attrib(_transforms), _num_rendered_lights);


    // now draw the lights mesh

    SceneSetup* scene_setup = _light_stage->_main_scene_setup;
    nassertv(scene_setup != nullptr)


    CullResult* cull_result = data->get_cull_result();
    Thread *current_thread = Thread::get_current_thread();
    DisplayRegion* dr = scene_setup->get_display_region();

    if (cull_result == nullptr || scene_setup == nullptr) {
        // Nothing to see here.
    } else if (dr->is_stereo()) {

    } else if (!gsg->set_scene(scene_setup)) {
        // The scene or lens is inappropriate somehow.
        display_cat.error()
        << gsg->get_type() << " cannot render scene with specified lens.\n";

    } else {
        // Tell the GSG to forget its state.
        gsg->clear_state_and_transform();

        if (gsg->begin_scene()) {
            nout << "drawing lights\n" << endl;
            gsg->push_group_marker("drawing lights");
            if (_hw_conservative_raster) {
    
                glEnable(_hw_conservative_raster);
                cull_result->draw(current_thread);
                glDisable(_hw_conservative_raster);
            
            } else 
                cull_result->draw(current_thread);
            nout << "finish drawing lights\n" << endl;
            gsg->pop_group_marker();
            gsg->end_scene();
        }
    }
    
    _num_rendered_lights = 0;
}



bool LightMeshRender::add_light_for_draw(int id, LightData::Type type, int lod,
                                        CPT(TransformState) transform) {

    LightLods& lods = _lights_pools[type];
    size_t num_lods = lods.get_num_lods();

    if (_num_rendered_lights < _max_rendered_lights &&
            num_lods > 0 && lod < num_lods) 
    {
        lods.add_light_for_draw(lod, id, transform->get_mat());
        ++_num_rendered_lights;
        return true;
    }
    return false;
}


TileSize LightMeshRender::get_tile_size() const {
    
    return _tile_size;
}


size_t LightMeshRender::get_num_tile_x() const {
    
    return _num_tile_x;    
}


size_t LightMeshRender::get_num_tile_y() const {

    return _num_tile_y;    
}


const Geom* LightMeshRender::get_lod_geom(LightData::Type type, size_t lod) const {
    
    const LightLods& lods = _lights_pools[type];
    nassertr(lod < lods.get_num_lods(), nullptr)

    return lods.get_geom(lod);
}


size_t LightMeshRender::get_num_lods(LightData::Type type) const {
    
    return _lights_pools[type].get_num_lods();
}


GraphicsOutput* LightMeshRender::get_buffer() const {
    
    return _buffer;
}


GraphicsOutput* LightMeshRender::get_debug_buffer() const {
    
    return _debug_buffer;
}


Texture* LightMeshRender::get_counter_texture() const {
    
    return _light_counter;
}


Texture* LightMeshRender::get_tiles_buffer() const {
    
    return _tile_buffer;
}

Texture* LightMeshRender::get_tiles_ids_buffer() const {
    
    return _tiles_ids;
}


Texture* LightMeshRender::get_tiles_min_depth_buffer() const {
    
    return _tiles_min_depth;
}


Texture* LightMeshRender::get_tiles_max_depth_buffer() const {
    
    return _tiles_max_depth;
}


LightMeshRender* LightMeshRender::get_current_renderer(CullTraverser* trav) {
    
    SceneSetup* setup = trav->get_scene();
    GraphicsOutput* buffer = setup->get_display_region()->get_window();
    LightMeshRender*& current = LightMeshRender::_current_renderer;


    if (current == nullptr || current->_light_stage->_pipeline->get_buffer() != buffer)
    {
        // update the cached renderer
        auto found = LightMeshRender::_renderers.find(buffer);
        current = found->second;
    }
    

    return current;
}


LightStage* LightMeshRender::get_stage() const {

    return _light_stage;    
}


void LightMeshRender::set_lod_geom(LightData::Type type, size_t lod, Geom* geom) {

    LightLods& lods = _lights_pools[type];

    nassertv(lod < lods.get_num_lods())

    lods.set_geom(lod, geom);
}

    // GLGraphicsStateGuardian* glgsg;

    // if (gsg->is_of_type(GLGraphicsStateGuardian::get_class_type()))
    //     glgsg = DCAST(GLGraphicsStateGuardian, gsg);

    // if (glgsg != nullptr)
    //     glgsg->issue_memory_barrier(GL_ALL_BARRIER_BITS | GL_ALL_BARRIER_BITS_EXT); 


    // int32_t* ids_buf = (int32_t*)ids.p();
    // UnalignedLMatrix4* trans_buf = (UnalignedLMatrix4*)transforms.p();

    // for (LightLods& lods: _lights_pools) {
    //     for (LightLod& lod: lods) {

    //         lod.collect(ids_buf, trans_buf);
    //         size_t num_lights = lod.get_num_lights();
    //         ids_buf += num_lights;
    //         trans_buf += num_lights;
    //     }
    // }
    // gsg->push_group_marker("updating light transforms");
    
    // gsg->set_state_and_transform(_transforms.get_state(), TransformState::make_identity());
    // gsg->dispatch_compute(_num_rendered_lights, 1, 1);
    // gsg->pop_group_marker();



 // for (LightLods& lods: _lights_pools) {
    //     int i = -1;
    //     for (LightLod& lod: lods) {
    //         i++;
    //         int num_instance = lod.get_num_lights();

    //         if (num_instance == 0)
    //             continue;


    //         // _assignment.set_instance_count(1);
    //         const RenderState* state = _assignment.get_state();

    //         gsg->clear_state_and_transform();
    //         nout << "drawing light lod " << i << " with " << num_instance << " instances and offset :" <<  _instance_offset[0] << "\n";
    //         lod.draw_lights(gsg, state, force, current_thread);

    //         _instance_offset[0] += num_instance;
    //         lod.clear_lights();

    //         // if (glgsg != nullptr)
    //         //     glgsg->issue_memory_barrier(GL_ALL_BARRIER_BITS);
    //     }
    // }




        // LVecBase3 pos = internal_transform->get_pos();
        // float cam_fact = trav->get_scene()->get_camera_node()->get_lod_scale();
        // float dist = abs(pos.get_z())*0.4;

        // float fact = 1.0f/(dist + 1.0f) * cam_fact;

        // fact = clamp(fact*point_light->get_radius(), 0.0f, 1.0f);
        // int index = fact * (stage->_num_lods - 1);

    
    // CullableObject* object_wire = new CullableObject(geom,
    //                                         state_wire, 
    //                                         internal_transform);


    // bool force = !trav->get_effective_incomplete_render();
    // GraphicsStateGuardian* gsg = trav->get_scene()->get_display_region()->get_window()->get_gsg();

    // if (object_wire->munge_geom(gsg, gsg->get_geom_munger(state_wire, current_thread), trav, force))
    //     stage->_wire_bin->add_object(object_wire, current_thread);
    // else
    //     delete object_wire;



    // CullableObject* object = new CullableObject(geom, state, internal_transform);

    // object->set_draw_callback(stage->_light_draw_cb);

    // trav->get_cull_handler()->record_object(object, trav);