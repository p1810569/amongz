// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <cullTraverserData.h>
#include <cullHandler.h>
#include "lightBufferManager.h"
#include "shadowBufferManager.h"
#include "config_tiled_lighting.h"
#include "pointLightNode.h"
#include "spotLightNode.h"
#include "lightMeshRender.h"
#include "lightStage.h"
#include "render_utils.h"
#include "utils.h"


using namespace std;


struct ALIGN_16BYTE LightDataBatch {

    static const size_t size;
    static const size_t block;

    LightData datas[4];
    int32_t ids[4];
};

const size_t LightDataBatch::size = (sizeof(LightDataBatch) + 15u) & ~15u;
const size_t LightDataBatch::block = LightDataBatch::size >> 4;


DEFINE_TYPEHANDLE(LightBufferManager)


LightBufferManager::LightBufferManager(): _collect_frame(-1),
    _wireframe("light-wireframe") {

    CDWriter cdata(_cycler);
    cdata->_lights.resize(light_buffer_size);

    CullBinManager* mgr = CullBinManager::get_global_ptr();
    mgr->add_bin("lights", CullBin::BT_unsorted, 0);
    
    setup_buffer();
}


void LightBufferManager::setup_buffer() {

    CDWriter cdata(_cycler);

    size_t size = cdata->_lights.get_size();

    size_t buffer_size = LightData::block * size + (size << 2);
    size_t update_size = LightDataBatch::block * 128;


    _light_buffer = new ShaderBuffer("light buffer", buffer_size, 
                                    GeomEnums::UH_dynamic);

    _update_buffer = new Texture("update buffer");

    _update_buffer->setup_buffer_texture(update_size, 
                                    Texture::T_unsigned_int, 
                                    Texture::F_rgba32,
                                    GeomEnums::UH_dynamic);
    
    _update_buffer->set_compression(Texture::CM_off);


    _update_lights = Shader::load_compute(SHADER_TYPE, 
                        SDK_SHADER_PATH COMP_SHADER("update_lights"));

    cdata->_update.set_shader(_update_lights);
    cdata->_update.set_shader_input("u_update_buffer", _update_buffer);
    cdata->_update.set_shader_input("lightBuffer", _light_buffer);

    _wireframe.set_bin("lights", 10);
    _wireframe.set_depth_write(false);
    _wireframe.set_depth_test(false);
    _wireframe.set_two_sided(false);
    _wireframe.set_attrib(RenderModeAttrib::make(RenderModeAttrib::M_wireframe), 10);
    _wireframe.set_color(LColor(1,1,1,1));
}


void LightBufferManager::add_light(LightBase* light) {

    CDWriter cdata(_cycler);

    nassertv(cdata->_lights.get_num_items() < cdata->_lights.get_size())
    nassertv(light->get_id() == -1)

    light->ref();
    
    int slot = cdata->_lights.acquire_slot();
    nassertv(slot >= 0)

    cdata->_lights[slot] = light;
    light->set_needs_update(true);
    light->set_id(slot);
}


void LightBufferManager::remove_light(LightBase* light) {
    
    int slot = light->get_id();

    CDWriter cdata(_cycler);

    nassertv(slot >=0 && slot < cdata->_lights.get_size())
    nassertv(cdata->_lights.is_slot_used(slot))

    cdata->_lights.release_slot(slot);
    light->set_id(-1);
    light->unref();
}


void LightBufferManager::add_light_for_draw(LightBase* light, CullTraverser* trav, CullTraverserData& data) {
    
    Thread *current_thread = trav->get_current_thread();

    LightMeshRender* renderer = LightMeshRender::get_current_renderer(trav);

    if (renderer == nullptr)
        return;

    CPT(TransformState) transform;
    LightData::Type type = get_light_type(light);

    int num_lod = renderer->get_num_lods(type);
    
    if (num_lod == 0) 
        return;
    
    int lod = 0;

    switch (type) {

        case LightData::T_point: {
            PointLightNode* point_light = DCAST(PointLightNode, light);
            float radius = point_light->get_radius();
            
            CPT(TransformState) prev_transform = data._net_transform;

            data._net_transform = data._net_transform->set_scale(radius);
            transform = data.get_internal_transform(trav);

            data._net_transform = prev_transform;

            LVecBase3 pos = transform->get_pos();
            float cam_fact = trav->get_scene()->get_camera_node()->get_lod_scale();
            float dist = abs(pos.get_z())*0.4;

            float fact = 1.0f/(dist + 1.0f) * cam_fact;

            fact = saturate(fact*point_light->get_radius());
            lod = fact * (num_lod - 1);
            
            break;
        }
        default:
            nout << "unknown light type" << endl;
            return;
    }

    CPT(Geom) geom = renderer->get_lod_geom(type, lod);

    nassertv(geom != nullptr)

    CPT(RenderState) state = _wireframe.get_state();

    if (renderer->add_light_for_draw(light->get_id(), type, lod, transform)) {

        CullableObject* object = new CullableObject(geom,
                                                _wireframe.get_state(),
                                                transform);
    
        trav->get_cull_handler()->record_object(object, trav);
    }
}


void LightBufferManager::update_light_buffer(GraphicsStateGuardian* gsg) {

    CDWriter cdata(_cycler);

    ClockObject* clock = ClockObject::get_global_clock();
    int frame = clock->get_frame_count();

    if (frame > _collect_frame) {
        _collect_frame = frame;

        PTA_uchar ptr = _update_buffer->modify_ram_image();
        LightDataBatch* buffer = (LightDataBatch*)ptr.p();

        const RenderState* state = cdata->_update.get_state();

        gsg->push_group_marker("update light buffer");

        gsg->set_state_and_transform(state, TransformState::make_identity());

        int num_batch = 0;
        int offset = 0;

        Lights::PoolContainer::iterator it = cdata->_lights.begin();

        while (it != cdata->_lights.end()) {
            
            if (it->_used) {
                
                LightDataBatch& batch = buffer[num_batch];
                LightBase* light = it->_value;

                batch.ids[offset] = light->get_id();
                LightData& data = batch.datas[offset];
                
                collect_light_data(light, data);
                light->set_needs_update(false);
                
                offset = (offset + 1) & 3u;

                if (offset == 0) {
                    if (num_batch == 127) {
                        gsg->dispatch_compute(128, 1, 1);
                        num_batch = 0;
                    } else
                        ++num_batch;
                }
            }
            ++it;
        }

        // fill the unused batch ids with -1
        if (offset != 0) {
            if (num_batch == 0)
                num_batch = 1;
            
            for (int i=offset; i<4; ++i)
                buffer[num_batch - 1].ids[i] = -1;
        }
        // send the left batches
        if (num_batch > 0)
            gsg->dispatch_compute(num_batch, 1, 1);

        gsg->pop_group_marker();
    }
}


void LightBufferManager::collect_light_data(LightBase* light, LightData& data) const {
    
    data.color = light->get_color().get_xyz() * light->get_energy();

    switch (get_light_type(light)) {

        case LightData::T_point: {

            PointLightNode* pl = DCAST(PointLightNode, light);

            data.type = LightData::T_point;
            data.data.set_x(pl->get_radius());
            break;
        }
        case LightData::T_spot:

            break;

        default:

            break;
            // not implemented
    }
}


size_t LightBufferManager::get_buffer_size() const {

    CDReader cdata(_cycler);
    return cdata->_lights.get_size();
}


void LightBufferManager::set_buffer_size(size_t size) {

    CDWriter cdata(_cycler);
    cdata->_lights.resize(size);
}


LightData::Type LightBufferManager::get_light_type(const TypeHandle type) const{
    
    LightTypes::const_iterator it = _light_types.find(type);

    if (it == _light_types.end())
        return LightData::T_none;
    
    return it->second;
}


LightData::Type LightBufferManager::get_light_type(LightBase* light) const {
    
    return get_light_type(light->get_type());
}


void LightBufferManager::map_light_type(const TypeHandle handle, LightData::Type type) {

    _light_types[handle] = type;    
}


bool LightBufferManager::has_light_type(LightData::Type type) const {
    
    LightTypes::const_iterator it = _light_types.begin();

    for (; it != _light_types.end(); ++it) {
        if (it->second == type)
            return true;
    }
    return false;
}


size_t LightBufferManager::get_num_lights() const {

    CDReader cdata(_cycler);
    return cdata->_lights.get_num_items();
}


ShadowBufferManager* LightBufferManager::get_shadow_manager() const{

    return _shadow_manager;    
}



LightBufferManager::CData::CData():
    _update_buffer_size(512),
    _light_buffer_size(0),
    _update("update light buffer")
{
    
}


LightBufferManager::CData::CData(const CData &copy): 
    _lights(copy._lights),
    _update_buffer_size(copy._update_buffer_size),
    _light_buffer_size(copy._light_buffer_size),
    _update(copy._update)
{
    
}


CycleData* LightBufferManager::CData::make_copy() const {

    return new CData(*this);
}


void LightBufferManager::CData::write_datagram(BamWriter *manager, Datagram &dg) const {
    
}


void LightBufferManager::CData::fillin(DatagramIterator &scan, BamReader *manager) {
    
}


LightManager* LightBufferManager::create() {
    
    return new LightBufferManager();
}


ShaderBuffer* LightBufferManager::get_buffer() {
    
    return _light_buffer;
}
