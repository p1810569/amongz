

// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __SHADOWBUFFERMANAGER_H__
#define __SHADOWBUFFERMANAGER_H__


#include "utils.h"

class GraphicsOutput;
class Texture;


struct ALIGN_16BYTE ShadowMap {

    uint32_t _layer;
    float _opacity;
};

typedef ShortSizePoT ShadowSize;

class ShadowBufferManager {

public:

    ShadowBufferManager(GraphicsOutput* host = nullptr);
    ~ShadowBufferManager() = default;

    void update();

    void set_max_shadow_map(size_t n);
    void set_max_shadow_cubemap(size_t n);

    size_t get_max_shadow_map() const;
    size_t get_max_shadow_cubemap() const;

    void set_shadow_map_size(ShadowSize size);
    void set_shadow_cubemap_size(ShadowSize size);
    
    ShadowSize get_shadow_map_size() const;
    ShadowSize get_shadow_cubemap_size() const;

private:

    static GraphicsOutput* create_buffer(Texture* texture, GraphicsOutput* host);

    size_t _num_shadow_map;
    size_t _num_shadow_cubemap;

    ShadowSize _shadow_map_size;
    ShadowSize _shadow_cubemap_size;

    PT(GraphicsOutput) _shadow_map_buffer;
    PT(GraphicsOutput) _shadow_cubemap_buffer;

    PT(Texture) _shadow_map;
    PT(Texture) _shadow_cubemap;
};
#endif // __SHADOWBUFFERMANAGER_H__