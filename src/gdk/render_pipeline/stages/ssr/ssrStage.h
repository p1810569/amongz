// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SSRSTAGE_H__
#define __SSRSTAGE_H__


#include "renderStage.h"


class SSR_Stage: public RenderStage {

    using RenderCallbackData = CallbackGraphicsWindow::RenderCallbackData;

public:
    SSR_Stage(RenderPipeline* pipeline);
    ~SSR_Stage() = default;

    void render(RenderCallbackData* data) override;

    InputPort<Texture> i_hi_depth;
    InputPort<Texture> i_normal;
    InputPort<Texture> i_specular_and_roughness;

    OutputPort<Texture> o_specular_accumulator;

private:
    bool _use_half_res = true;
    LVecBase2i _size;

    PT(Texture) _output_tex;
    PT(Shader) _shader;

    NodePath _node;

protected:
    virtual void setup();
};
#endif // __SSRSTAGE_H__