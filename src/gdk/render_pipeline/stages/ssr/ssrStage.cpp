// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <graphicsEngine.h>
#include <dataNodeTransmit.h>
#include "renderPipeline.h"
#include "render_utils.h"
#include "ssrStage.h"


using namespace std;
using RenderCallbackData = CallbackGraphicsWindow::RenderCallbackData;


void SSR_Stage::render(RenderCallbackData* data) {
    
    if (data->get_callback_type() != CallbackGraphicsWindow::RCT_begin_frame) {
        data->upcall();
        return;
    }

    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();
    LVecBase2i size = _pipeline->get_dimensions();
    
    nassertv(_size.get_y() != 0 && _size.get_x() != 0)


    CPT(ShaderAttrib) sattr = DCAST(ShaderAttrib, 
        _node.get_attrib(ShaderAttrib::get_class_type()));

    engine->dispatch_compute(LVecBase3i(_size,1), sattr, get_buffer()->get_gsg());

    data->set_render_flag(false);
}


void SSR_Stage::setup() {


    if (i_specular_and_roughness.has_data())
        _node.set_shader_input("normal_vel", i_specular_and_roughness.get_data());

    if (i_specular_and_roughness.has_data())
        _node.set_shader_input("spec_rough", i_specular_and_roughness.get_data());
    
    if (i_hi_depth.has_data())
        _node.set_shader_input("hi_depth", i_hi_depth.get_data());

    o_specular_accumulator.set_data(_output_tex.p());
}


SSR_Stage::SSR_Stage(RenderPipeline* pipeline): RenderStage("ssr", pipeline),
    i_hi_depth("hierarchical depth", this), i_normal("normal", this),
    i_specular_and_roughness("specular and roughness", this),
    o_specular_accumulator("specular accumulator", this)
{

    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();
    _size = _pipeline->get_dimensions();

    if (_use_half_res)
        _size /= 2;

    _output_tex = new Texture("ssr-output");

    _output_tex->setup_2d_texture(_size.get_x(), _size.get_y(), 
                                Texture::T_half_float,
                                Texture::F_rgb16);

    _output_tex->set_magfilter(SamplerState::FT_linear);
    _output_tex->set_minfilter(SamplerState::FT_linear);
    _output_tex->set_wrap_u(SamplerState::WM_border_color);
    _output_tex->set_wrap_v(SamplerState::WM_border_color);


    _shader = Shader::load_compute(SHADER_TYPE, COMP_SHADER("ssr"));
    _node.set_shader(_shader);
    _node.set_shader_input("output", _output_tex);
}
