// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __OPAQUESTAGE_H__
#define __OPAQUESTAGE_H__


#include "renderStage.h"
#include "culledBins.h"
#include <string>


class OpaqueStage: public RenderStage {

    REGISTER_TYPE("OpaqueStage", RenderStage)

public:
    OpaqueStage(RenderPipeline* pipeline);
    OpaqueStage(const OpaqueStage& copy) = delete;
    virtual ~OpaqueStage() = default;

    virtual void draw(DisplayRegionDrawCallbackData* data) override;
    virtual void render(RenderCallbackData* data) override;

    OutputPort<Texture> o_accumulator_and_sun_shadow;
    OutputPort<Texture> o_diffuse_and_matID;
    OutputPort<Texture> o_depth_and_stencil;
    OutputPort<Texture> o_specular_and_roughness;
    OutputPort<Texture> o_normal_and_velocity;
    OutputPort<SceneSetup> o_main_scene_setup;
    OutputPort<CulledBins> o_culled_bins;

protected:
    virtual void setup() override;

private:

    bool is_first_region(DisplayRegion* region) const;
    bool is_main_region(DisplayRegion* region) const;

    void hook_display_regions();

    PT(Texture) _ao, _dm, _ds, _sr, _st, _nv;
    PT(GraphicsOutput) _gbuffer;
    PT(CulledBins) _culled_bins;
    PT(SceneSetup) _main_scene_setup;


    Texture* add_target(const std::string& name, 
        DrawableRegion::RenderTexturePlane bitplane,
        GraphicsOutput::RenderTextureMode mode=GraphicsOutput::RTM_bind_or_copy);
};

#endif // __OPAQUESTAGE_H__