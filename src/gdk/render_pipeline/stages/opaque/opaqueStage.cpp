// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <displayRegionDrawCallbackData.h>

#include "opaqueStage.h"
#include "renderPipeline.h"
#include "utils.h"

using namespace std;

DEFINE_TYPEHANDLE(OpaqueStage)


OpaqueStage::OpaqueStage(RenderPipeline* pipeline): 
    RenderStage("opaque", pipeline),
    _main_scene_setup(new SceneSetup),
    _culled_bins(new CulledBins),
    o_accumulator_and_sun_shadow("accumulator and sun shadow", this),
    o_diffuse_and_matID("diffuse and matID", this),
    o_depth_and_stencil("depth and stencil", this),
    o_specular_and_roughness("specular and roughness", this),
    o_normal_and_velocity("normal and velocity", this),
    o_culled_bins("culled bins", this),
    o_main_scene_setup("main scene setup", this)
{

    FrameBufferProperties fb_prop = FrameBufferProperties::get_default();

    fb_prop.set_force_hardware(true);
    fb_prop.set_rgb_color(true);
    fb_prop.set_stereo(false);
    fb_prop.set_accum_bits(0);
    fb_prop.set_rgba_bits(8,8,8,8);
    fb_prop.set_depth_bits(24);
    fb_prop.set_stencil_bits(8);
    fb_prop.set_multisamples(0);
    fb_prop.set_back_buffers(0);
    fb_prop.set_aux_hrgba(2);
    fb_prop.set_aux_rgba(2);


    GraphicsOutput* host = _pipeline->get_host();
    LVecBase2i size = _pipeline->get_dimensions();

    add_callback(-1); // for hooking new display region

    _gbuffer = add_buffer(fb_prop, "gbuffer", size.get_x(), size.get_y(), 0, host);

    _ds = add_target("depth_&_stencil", DrawableRegion::RTP_depth_stencil);
    _dm = add_target("diffuse_&_matID", DrawableRegion::RTP_color);
    _sr = add_target("spec_&_rough", DrawableRegion::RTP_aux_rgba_0);
    _ao = add_target("acc_&_ao", DrawableRegion::RTP_aux_hrgba_0);
    _nv = add_target("normal_&_vel", DrawableRegion::RTP_aux_hrgba_1);


    hook_display_regions();

    _pipeline->set_buffer(_gbuffer);
}


void OpaqueStage::draw(DisplayRegionDrawCallbackData* data) {


    Thread* current_thread = Thread::get_current_thread();

    SceneSetup* setup = data->get_scene_setup();
    DisplayRegion* dr = setup->get_display_region();
    GraphicsOutput* buffer = dr->get_window();

    // this display region is no longer in the gbuffer, unhook it
    if (buffer != _gbuffer) {
        dr->clear_draw_callback();
        data->upcall();
        return;
    }
    
    GraphicsStateGuardian* gsg = buffer->get_gsg();
    CullResult* result = dr->get_cull_result(current_thread);
    CullBinManager* bin_manager = CullBinManager::get_global_ptr();
    bool force = !gsg->get_effective_incomplete_render();


    int num_bins = bin_manager->get_num_bins();

    gsg->clear_state_and_transform();

    if (is_first_region(dr))
        _culled_bins->clear();


    if (!gsg->set_scene(setup)) {
        display_cat.error()
            << gsg->get_type() << " cannot render scene.\n";

    } else if (gsg->begin_scene()) {

        for (int i = 0; i < num_bins; ++i) {
            int bin_index = bin_manager->get_bin(i);
            CullBin* bin = result->get_bin(bin_index);
            
            if (_culled_bins->add_bin_to_groups(setup, bin) == 0) {

                gsg->push_group_marker(bin->get_name());
                bin->draw(force, current_thread);
                gsg->pop_group_marker();
            }
        }
        gsg->end_scene();
    }

    if (is_main_region(dr))
        *_main_scene_setup = *setup; // copy the scene setup
}



void OpaqueStage::render(RenderCallbackData* data) {
    
    if (data->get_callback_type() != CallbackGraphicsWindow::RCT_begin_frame) {
        data->upcall();
        return;
    }

    hook_display_regions();

    data->set_render_flag(false);
}


void OpaqueStage::setup() {

    o_accumulator_and_sun_shadow.set_data(_ao);
    o_diffuse_and_matID.set_data(_dm);
    o_depth_and_stencil.set_data(_ds);
    o_specular_and_roughness.set_data(_sr);
    o_normal_and_velocity.set_data(_nv);
    o_culled_bins.set_data(_culled_bins);
    o_main_scene_setup.set_data(_main_scene_setup);


    _gbuffer->set_clear_color_active(true);
    _gbuffer->set_clear_depth_active(true);
    _gbuffer->set_clear_stencil_active(true);
    // _gbuffer->set_clear_value(DrawableRegion::RTP_aux_hrgba_0, LColor(1,0,0,1));

    _gbuffer->set_clear_active(DrawableRegion::RTP_aux_rgba_0, true);
    _gbuffer->set_clear_active(DrawableRegion::RTP_aux_rgba_1, true);
    _gbuffer->set_clear_active(DrawableRegion::RTP_aux_hrgba_0, true);
    _gbuffer->set_clear_active(DrawableRegion::RTP_aux_hrgba_1, true);


    hook_display_regions();
}


bool OpaqueStage::is_first_region(DisplayRegion* region) const {

    int num_region = _gbuffer->get_num_active_display_regions();
    nassertr(num_region > 0, false)

    DisplayRegion* dr = _gbuffer->get_active_display_region(0);
    int min_sort = dr->get_sort();

    for (int i=1; i<num_region; ++i) {
        dr = _gbuffer->get_active_display_region(i);
        int sort = dr->get_sort();
        if (sort < min_sort)
            min_sort = sort;
    }
    return region->get_sort() == min_sort;
}


bool OpaqueStage::is_main_region(DisplayRegion* region) const {

    nassertr(_gbuffer->get_num_active_display_regions() > 0, false)
    return _gbuffer->get_active_display_region(0) == region;
}


void OpaqueStage::hook_display_regions() {

    int num_region = _gbuffer->get_num_active_display_regions();

    for (int i=0; i<num_region; ++i) {
        DisplayRegion* region = _gbuffer->get_active_display_region(i);

        if (region->get_draw_callback() != _callback)
            hook_region(region, RenderStage::CB_draw);
    }
}


Texture* OpaqueStage::add_target(const string& name, 
    DrawableRegion::RenderTexturePlane bitplane,
    GraphicsOutput::RenderTextureMode mode)
{

    nassertr(_gbuffer != nullptr, nullptr);

    Texture* tex = new Texture(name);
    tex->set_wrap_u(SamplerState::WM_clamp);
    tex->set_wrap_v(SamplerState::WM_clamp);
    tex->set_minfilter(SamplerState::FT_nearest);
    tex->set_magfilter(SamplerState::FT_nearest);
    tex->set_keep_ram_image(false);

    _gbuffer->add_render_texture(tex, mode, bitplane);

    return tex;
}
