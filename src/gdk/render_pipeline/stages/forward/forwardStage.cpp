// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <displayRegionDrawCallbackData.h>

#include "forwardStage.h"
#include "renderPipeline.h"


using namespace std;

DEFINE_TYPEHANDLE(ForwardStage)


ForwardStage::ForwardStage(RenderPipeline* pipeline): 
    RenderStage("forward", pipeline),
    i_accumulator("accumulator", this),
    i_culled_bins("culled bins", this),
    i_depth_and_stencil("depth and stencil", this),
    o_accumulator("accumulator", this),
    o_depth_and_stencil("depth and stencil", this)
{
    
    FrameBufferProperties fb_prop = FrameBufferProperties::get_default();

    fb_prop.set_force_hardware(true);
    fb_prop.set_rgb_color(false);
    fb_prop.set_stereo(false);
    fb_prop.set_accum_bits(0);
    fb_prop.set_depth_bits(24);
    fb_prop.set_stencil_bits(8);
    fb_prop.set_multisamples(0);
    fb_prop.set_back_buffers(0);
    fb_prop.set_aux_hrgba(2);

    GraphicsOutput* host = _pipeline->get_host();
    LVecBase2i size = _pipeline->get_dimensions();

    _buffer = add_buffer(fb_prop, "fbuffer", size.get_x(), size.get_y(), 0, host);
    _buffer->disable_clears();

    hook_region(_buffer->make_display_region(), CB_draw);
    
    add_bin_name("forward");
    add_bin_name("transparent");
}


ForwardStage::~ForwardStage() {
    
    if (i_culled_bins.has_data()) {
        CulledBins* bins = i_culled_bins.get_data();
        if (bins != nullptr)
            bins->remove_group("forward");
    }
}


void ForwardStage::draw(DisplayRegionDrawCallbackData* data) {
    
    SceneSetup* setup = data->get_scene_setup();
    Thread* current_thread = Thread::get_current_thread();

    if (setup != nullptr)
        data->upcall();

    // now, draw forward pass, e.g : transparent objects

    if (_forward_bins != nullptr)
        _forward_bins->draw();
}


void ForwardStage::setup() {

    if (i_depth_and_stencil.has_data()) {
        Texture* depth = i_depth_and_stencil.get_data();
        _buffer->add_render_texture(depth,
                                GraphicsOutput::RTM_bind_or_copy,
                                DrawableRegion::RTP_depth_stencil);

        o_depth_and_stencil.set_data(depth);
    }

    if (i_accumulator.has_data()) {
        Texture* accumulator = i_accumulator.get_data();
        _buffer->add_render_texture(accumulator,
                                GraphicsOutput::RTM_bind_or_copy,
                                DrawableRegion::RTP_aux_hrgba_0);

        o_accumulator.set_data(accumulator);
    }


    if (i_culled_bins.has_data()) {
        _forward_bins = i_culled_bins.get_data()->add_group("forward");

        for (const string& name: _bin_names)
            _forward_bins->add_bin_name(name);
    }
}


void ForwardStage::add_bin_name(const string& name) {
    
    _bin_names.insert(name);
}


void ForwardStage::remove_bin_name(const string& name) {
    
    _bin_names.erase(name);
}
