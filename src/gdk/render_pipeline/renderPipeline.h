
// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __RENDERPIPELINE_H__
#define __RENDERPIPELINE_H__


#include <graphicsOutput.h>
#include <drawableRegion.h>
#include <texture.h>
#include <cullBin.h>
#include <texture.h>
#include <typedReferenceCount.h>
#include <pmap.h>
#include <string>

#include "renderStage.h"



class RenderPipeline: public PipelineNode {

    REGISTER_TYPE("RenderPipeline", PipelineNode)

public:
    RenderPipeline(const std::string& name, GraphicsOutput* host=nullptr, 
                    LVecBase2i dimension=LVecBase2i(0));

    ~RenderPipeline() = default;

    InputPort<Texture> i_final_render;


    GraphicsOutput* get_buffer() const;
    void set_buffer(GraphicsOutput* buffer);

    RenderStage* get_stage(const std::string& name) const;
    RenderStage* get_stage(size_t n) const;

    void remove_stage(const std::string& name);
    void remove_stage(RenderStage* stage);
    void remove_stage(size_t n);

    size_t get_num_stage() const;

    int get_stage_index(const RenderStage* stage) const;

    LVecBase2i get_dimensions() const;
    void set_dimensions(LVecBase2i size);
    void set_dimensions(int width, int height);

    void add_debug_stage(const std::string& name, Texture* texture);
    Texture* get_debug_stage(const std::string& name);

    void set_host(GraphicsOutput* host);
    GraphicsOutput* get_host() const;

    Texture* get_output_texture() const;


    void add_stage(RenderStage* stage);
    RenderStage* add_stage(const std::string& name);

    Texture* init();

private:

    void sort_stages_buffers();

    typedef pmap<std::string, PT(Texture)> DebugStages;
    typedef pvector<PT(RenderStage)> Stages;

    DebugStages _debug_stages;
    Stages _stages;


    PT(DisplayRegion) _output_region;
    PT(GraphicsOutput) _input;
    PT(GraphicsOutput) _host;
    PT(GraphicsOutput) _buffer;

    PT(Texture) _output_texture;
    LVecBase2i _dimensions;
};


#endif // __RENDERPIPELINE_H__




    // template<class T>
    // class StageRegistery {
    // public:
    //     StageRegistery(const char* id) {
    //         RenderPipeline::register_stage<T>(id);
    //     }
    // };

    // template<class T>
    // static void register_stage(const std::string& id) {

    //     nassertv(_stage_constructs.find(id) == _stage_constructs.end())
    //     _stage_constructs[id] = &_instanciate_stage<T>;
    // }


    // static StageConstructs _stage_constructs;

    // typedef RenderStage* (*StageConstruct)(RenderPipeline*);
    // typedef pmap<std::string, StageConstruct> StageConstructs;

    // template<class T>
    // static RenderStage* _instanciate_stage() {
    //     return dynamic_cast<RenderStage*>(new T());
    // }

// #define REGISTER_STAGE(cls, name) \
//     static RenderPipeline::StageRegistery<cls> cls##_(name);

