// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "render_utils.h"
#include <cardMaker.h>
#include <camera.h>
#include <orthographicLens.h>
#include <graphicsStateGuardian.h>
#include <graphicsEngine.h>

using namespace std;


void create_2d_scene(NodePath root, NodePath& cam_np) {

    Camera* cam = new Camera("pipeline-output");

    OrthographicLens* lens = new OrthographicLens;
    static const PN_stdfloat left = -1.0f;
    static const PN_stdfloat right = 1.0f;
    static const PN_stdfloat bottom = -1.0f;
    static const PN_stdfloat top = 1.0f;
    lens->set_film_size(right - left, top - bottom);
    lens->set_film_offset((right + left) * 0.5, (top + bottom) * 0.5);
    lens->set_near_far(-1000, 1000);
    cam->set_lens(lens);

    cam_np = root.attach_new_node(cam);
}


void create_fullscreen_quad(NodePath root, NodePath& quad_np, 
                            const string& shader_file) {
    
    CardMaker cm("fullscreen-quad");
    cm.set_frame_fullscreen_quad();

    quad_np = root.attach_new_node(cm.generate());

    Shader* shader = Shader::load(  SHADER_TYPE,
                                    SDK_SHADER_PATH VERT_SHADER("quad"),
                                    shader_file);

    quad_np.set_shader(shader);
}


const ShaderAttrib* get_shader_attrib(NodePath node) {

    const RenderAttrib* attr = node.get_attrib(ShaderAttrib::get_class_type());
    return DCAST(ShaderAttrib, attr);
}


void dispatch_compute( GraphicsStateGuardian* gsg, 
                        const ShaderAttrib* attrib,
                        size_t x, size_t y, size_t z) {
    
    const Shader *shader = attrib->get_shader();
    nassertv(shader != nullptr);
    nassertv(gsg != nullptr);

    string shader_name = shader->get_filename(Shader::ST_compute).get_basename();

    CPT(RenderState) state = RenderState::make(attrib);

    gsg->set_state_and_transform(state, TransformState::make_identity());

    gsg->push_group_marker(string("Compute ") + shader_name);
    gsg->dispatch_compute(x, y, z);
    gsg->pop_group_marker();
}



GraphicsOutput* make_buffer(FrameBufferProperties fb_prop,
    const string& name, int size_x, int size_y, int sort, GraphicsOutput* host)
{

    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();

    return  engine->make_output( host->get_pipe(), name, sort, fb_prop, 
                                WindowProperties::size(size_x, size_y), 
                                GraphicsPipe::BF_refuse_window | 
                                GraphicsPipe::BF_refuse_parasite |
                                GraphicsPipe::BF_fb_props_optional |
                                GraphicsPipe::BF_resizeable,
                                host->get_gsg(), host);
}


void draw_fullscreen_quad(GraphicsStateGuardian* gsg, CPT(RenderState) state, bool force) {

    static const CPT(Geom) geom = make_fullscreen_quad()->get_geom(0);

    Thread* current_thread = Thread::get_current_thread();

    GeomPipelineReader geom_reader(geom, current_thread);
    CPT(GeomVertexData) munged_data = geom_reader.get_vertex_data();
    
    // PT(GeomMunger) munger = gsg->get_geom_munger(state, current_thread);

    // if (!munger->munge_geom(lod._geom, munged_data, force, current_thread))
    //     continue;
    
    // PT(StateMunger) state_munger = DCAST(StateMunger, munger);
    // if (state_munger->should_munge_state())
    //     state = state_munger->munge_state(state);

    gsg->set_state_and_transform(state, TransformState::make_identity());
    gsg->push_group_marker("drawing fullscreen quad");

    GeomVertexDataPipelineReader data_reader(munged_data, current_thread);
    data_reader.check_array_readers();
    geom_reader.draw(gsg, &data_reader, force, current_thread);

    gsg->pop_group_marker();   
}



GeomNode* make_fullscreen_quad() {

    static CardMaker cm("fullscreen quad");
    cm.set_frame_fullscreen_quad();
    PT(PandaNode) node = cm.generate();
    node->ref();
    
    return DCAST(GeomNode, node);
}