// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __RENDER_UTILS_H__
#define __RENDER_UTILS_H__



#include <shaderAttrib.h>
#include <nodePath.h>

#include "config.h"


class FrameBufferProperties;


#define SDK_SHADER_PATH "/gdk/shader/"


#if defined(NDEBUG) && defined(USE_SPIRV)
    #define SHADER(name, type) name ".spirv" 
    #define SHADER_TYPE Shader::SL_SPIR_V
#else
    #define SHADER(name, type) name "." type
    #define SHADER_TYPE Shader::SL_GLSL
#endif

#define VERT_SHADER(name) SHADER(name, "vert")
#define GEOM_SHADER(name) SHADER(name, "geom")
#define FRAG_SHADER(name) SHADER(name, "frag")
#define TESS_SHADER(name) SHADER(name, "tess")
#define COMP_SHADER(name) SHADER(name, "comp")





std::string get_unique_name(const std::string& name, 
                            std::vector<std::string> names);


void create_2d_scene(NodePath root, NodePath& cam_np);


void create_fullscreen_quad(NodePath root, NodePath& quad_np, 
        const std::string& shader=SDK_SHADER_PATH FRAG_SHADER("texture"));


const ShaderAttrib* get_shader_attrib(NodePath node);



void dispatch_compute(  GraphicsStateGuardian* gsg, 
                        const ShaderAttrib* attrib,
                        size_t x, size_t y=1, size_t z=1);


GraphicsOutput* make_buffer(FrameBufferProperties fb_prop,
                            const std::string& name, int size_x, 
                            int size_y, int sort, GraphicsOutput* host);


void draw_fullscreen_quad(GraphicsStateGuardian* gsg, 
                        CPT(RenderState) state, bool force);


GeomNode* make_fullscreen_quad();


template<class T>
CPT(T) get_render_attrib(CPT(RenderState) state) {

    return DCAST(T, state->get_attrib(T::get_class_slot()));
}


template<class T>
CPT(RenderAttrib) set_shader_input(CPT(RenderAttrib) attr, 
                                CPT_InternalName name, T& value)
{
    CPT(ShaderAttrib) sattr = DCAST(ShaderAttrib, attr);
    return sattr->set_shader_input(ShaderInput(name, value));
}


template<class T>
CPT(RenderAttrib) make_shader_input(CPT_InternalName name, T& value) {

    return set_shader_input(ShaderAttrib::make(), name, value);
}



#endif // __RENDER_UTILS_H__