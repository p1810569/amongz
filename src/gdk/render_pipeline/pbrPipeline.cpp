// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include "stages/opaque/opaqueStage.h"
#include "stages/forward/forwardStage.h"
#include "stages/hiZ/hiZ_Stage.h"
#include "stages/tiled_lighting/lightStage.h"

#include "renderPipeline.h"
#include "pbrPipeline.h"


using namespace std;


void make_pbr_pipeline(RenderPipeline* pipeline) {

    ForwardStage* forward   = new ForwardStage(pipeline);
    OpaqueStage* opaque     = new OpaqueStage(pipeline);
    // HiZ_Stage* hi_z         = new HiZ_Stage(pipeline);
    // LightStage* light       = new LightStage(pipeline);

    // opaque->o_depth_and_stencil.connect(hi_z->i_depth);
    // opaque->o_accumulator_and_sun_shadow.connect(light->i_accumulator);
    // opaque->o_main_scene_setup.connect(light->i_main_scene_setup);
    // opaque->o_culled_bins.connect(light->i_bins);
    // opaque->o_normal_and_velocity.connect(light->i_normal);

    // hi_z->o_hi_depth.connect(light->i_hi_depth);

    // light->o_light_debug.connect(pipeline->i_final_render);

    // light->o_accumulator.connect(pipeline->i_final_render);

    // light->o_accumulator.connect(forward->i_accumulator);
    opaque->o_accumulator_and_sun_shadow.connect(forward->i_accumulator);
    opaque->o_depth_and_stencil.connect(forward->i_depth_and_stencil);
    opaque->o_culled_bins.connect(forward->i_culled_bins);
    forward->o_accumulator.connect(pipeline->i_final_render);
}