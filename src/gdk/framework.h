
// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __FRAMEWORK_H__
#define __FRAMEWORK_H__


#include <asyncTaskManager.h>
#include <graphicsEngine.h>
#include <graphicsPipe.h>


void start_pstats();

class Framework {

public:

    enum ExitStatus {
        ES_success = EXIT_SUCCESS,
        ES_failure = EXIT_FAILURE,
        ES_none
    };

    virtual ~Framework();

    ExitStatus get_exit_status() const;
    void set_exit_status(ExitStatus status=ES_success);

    virtual void main_loop() const;
    virtual void do_frame(Thread* current_thread) const;

    GraphicsPipe* get_default_pipe();

    GraphicsOutput* open_window(FrameBufferProperties fb_prop,
                                WindowProperties win_prop, 
                                const std::string& name);

    virtual ExitStatus finish();

private:
    ExitStatus _exit_flag;

    PT(GraphicsPipe) _default_pipe;
    PT(AsyncTaskManager) _task_mgr;

protected:
    Framework();

    virtual void make_default_pipe();

};
#endif // __FRAMEWORK_H__