#include "lightBase.h"
#include "lightManager.h"


using namespace std;

DEFINE_TYPEHANDLE(LightBase)


float LightBase::get_energy() const {

    CDReader cdata(_cycler);
    return cdata->_energy;
}


void LightBase::set_energy(float energy) {

    nassertv(energy >= 0.0f)

    CDWriter cdata(_cycler);
    cdata->_energy = energy;
}


bool LightBase::needs_update() const {
    
    CDReader cdata(_cycler);
    return cdata->_needs_update;   
}


void LightBase::set_needs_update(bool value) {

    CDWriter cdata(_cycler);
    cdata->_needs_update = value;    
}


LightBase::LightBase(const string& name, Lens* lens):
    LightLensNode(name, lens)
{
}


LightBase::LightBase(const LightBase& copy): 
    LightLensNode(copy),
    _cycler(copy._cycler)
{
}


void LightBase::add_for_draw(CullTraverser* trav, CullTraverserData& data) {


    LightManager* mgr = LightManager::get_global_ptr();
    mgr->add_light_for_draw(this, trav, data);
}


bool LightBase::is_renderable() const {
    
    return true;
}


bool LightBase::safe_to_combine() const  {
    
    return false;
}


void LightBase::setup_shadow_map() {
    
}


void LightBase::fillin(DatagramIterator &scan, BamReader *manager) {
    LightLensNode::fillin(scan, manager);

    CDWriter cdata(_cycler);
    cdata->fillin(scan, manager); 
}


LightBase::CData::CData(): 
    _energy(1.0f), 
    _needs_update(false),
    _id(-1) 
{
    
}


LightBase::CData::CData(const CData& copy): 
    _energy(copy._energy),
    _needs_update(copy._needs_update),
    _id(-1)
{
    
}


CycleData* LightBase::CData::make_copy() const {
    
    return new CData(*this);
}


void LightBase::CData::write_datagram(BamWriter *manager, Datagram &dg) const {
    
    dg.add_float32(_energy);
}


void LightBase::CData::fillin(DatagramIterator &scan, BamReader *manager) {
    
    _energy = scan.get_float32();
}


void LightBase::bind(GraphicsStateGuardianBase *gsg, 
                    const NodePath &light, int light_id) 
{
}


void LightBase::set_color(const LColor &color) {

    Light::set_color(color);
    set_needs_update(true);
}


void LightBase::set_color(float r, float g, float b) {
    
    set_color(LColor(r,g,b,0));
}


void LightBase::set_color_temperature(PN_stdfloat temperature) {
    
    Light::set_color_temperature(temperature);
    set_needs_update(true);
}


int LightBase::get_id() const {
    
    CDReader cdata(_cycler);
    return cdata->_id;
}


void LightBase::set_id(int id) {
    
    CDWriter cdata(_cycler);
    cdata->_id = id;
}