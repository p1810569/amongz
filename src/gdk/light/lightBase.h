// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __LIGHTBASE_H__
#define __LIGHTBASE_H__


#include <lightLensNode.h>
#include "utils.h"


class LightAttribs;
class CallbackObject;


class LightBase: public LightLensNode {

    REGISTER_TYPE("LightBase", LightLensNode)

public:
    virtual ~LightBase() = default;

    virtual void add_for_draw(CullTraverser* trav, CullTraverserData& data);
    virtual bool is_renderable() const;
    virtual bool safe_to_combine() const;

    virtual void bind(GraphicsStateGuardianBase *gsg, 
                    const NodePath &light, int light_id);

    void set_color(const LColor &color);
    void set_color(float r, float g, float b);
    void set_color_temperature(PN_stdfloat temperature);

    float get_energy() const;
    void set_energy(float energy);

    bool needs_update() const;
    void set_needs_update(bool value);

    int get_id() const;
    void set_id(int id);

    void set_callback(CallbackObject* callback);

protected:
    LightBase(const std::string& name, Lens* lens);
    LightBase(const LightBase& copy);
    virtual void setup_shadow_map() override;
    void fillin(DatagramIterator &scan, BamReader *manager);

private:


    class CData : public CycleData {
    public:
        CData();
        CData(const CData& copy);

        virtual CycleData* make_copy() const;
        virtual void write_datagram(BamWriter *manager, Datagram &dg) const;
        virtual void fillin(DatagramIterator &scan, BamReader *manager);
        TypeHandle get_parent_type() const override {
            return LightBase::get_class_type();
        }
    
        float _energy;
        bool _needs_update;
        int _id;
    };


    PipelineCycler<CData> _cycler;
    typedef CycleDataReader<CData> CDReader;
    typedef CycleDataWriter<CData> CDWriter;
};


// class LightLenseBase: public LightLensNode, public LightBase {


// };

#endif // __LIGHTBASE_H__