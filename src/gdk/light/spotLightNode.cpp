#include "spotLightNode.h"

using namespace std;

DEFINE_TYPEHANDLE(SpotLightNode)


SpotLightNode::SpotLightNode(const string& name): 
    LightBase(name, new PerspectiveLens())
{
    
}