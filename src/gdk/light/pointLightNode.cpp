#include <boundingSphere.h>
#include "pointLightNode.h"
#include "lightManager.h"


using namespace std;

DEFINE_TYPEHANDLE(PointLightNode)


PointLightNode::CData::CData(const CData &copy):
    _radius(copy._radius)
{
}


CycleData* PointLightNode::CData::make_copy() const {
    
    return new CData(*this);
}


void PointLightNode::CData::fillin(DatagramIterator &scan, BamReader *manager) {
    
    _radius = scan.get_float32();
}


void PointLightNode::CData::write_datagram(BamWriter *manager, Datagram &dg) const {
    
    dg.add_float32(_radius);
}


TypedWritable* PointLightNode::make_from_bam(const FactoryParams &params) {
    
    PointLightNode* light = new PointLightNode("");
    DatagramIterator scan;
    BamReader *manager;

    parse_params(params, scan, manager);
    light->fillin(scan, manager);

    return light;
}


void PointLightNode::fillin(DatagramIterator &scan, BamReader *manager) {
    LightBase::fillin(scan, manager);

    manager->read_cdata(scan, _cycler);
}


PointLightNode::PointLightNode(const string& name):
    LightBase(name, new PerspectiveLens())
{

    LightManager* mgr = LightManager::get_global_ptr();
    mgr->add_light(this);

    set_internal_bounds(new BoundingSphere(LPoint3(0,0,0), 1.0));
}


PointLightNode::PointLightNode(const PointLightNode& copy): 
    LightBase(copy),
    _cycler(copy._cycler)
{
    LightManager* mgr = LightManager::get_global_ptr();
    mgr->add_light(this);
}


PointLightNode::~PointLightNode() {

    LightManager* mgr = LightManager::get_global_ptr();
    mgr->remove_light(this);
}


PointLightNode* PointLightNode::make_copy() const {
    
    return new PointLightNode(*this);
}


int PointLightNode::get_class_priority() const {

    return (int)CP_point_priority;
}


float PointLightNode::get_radius() const {

    CDReader cdata(_cycler);
    return cdata->_radius;
}


void PointLightNode::set_radius(float radius) {

    CDWriter cdata(_cycler);
    cdata->_radius = radius;

    CPT(BoundingSphere) bound = DCAST(BoundingSphere, get_internal_bounds());
    nassertv(bound != nullptr)

    BoundingSphere* sphere_bound = const_cast<BoundingSphere*>(bound.p());
    sphere_bound->set_radius(radius);

    set_needs_update(true);
}


void PointLightNode::register_with_read_factory() {

    BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}


void PointLightNode::write(ostream &out, int indent_level) const {

    indent(out, indent_level) << *this << ":\n";
    indent(out, indent_level + 2)
    << "color " << get_color() << "\n";

    indent(out, indent_level + 2)
    << "attenuation " << get_attenuation() << "\n";

    if (!cinf(get_radius())) {
    indent(out, indent_level + 2)
        << "radius " << get_radius() << "\n";
    }
}