// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <stack>
#include <vector>
#include <pnotify.h>


template<class T>
class SlotPool {

public:

    struct Slot {
        T _value;
        bool _used;
    };

    typedef std::stack<int> UnusedPoolContainer;
    typedef std::vector<Slot> PoolContainer;

    typedef typename PoolContainer::iterator iterator;
    typedef typename PoolContainer::const_iterator const_iterator;


    SlotPool(size_t size=1024): _num_items(0) {
        _pool.resize(size);
    }

    ~SlotPool() {}

    const_iterator begin() const {
        return const_iterator(_pool.begin());
    }

    const_iterator end() const {
        return const_iterator(_pool.begin() + _num_items + _unused.size() + 1);
    }

    iterator begin() {
        return iterator(_pool.begin());
    }

    iterator end() {
        return iterator(_pool.begin() + _num_items + _unused.size() + 1);
    }

    int acquire_slot() {
        nassertr(_num_items < _pool.size(), -1)
        
        int index;
        if (!_unused.empty()) {
            index = _unused.top();
            _unused.pop();
        } else
            index = _num_items;

        _pool[index]._used = true;
        _num_items++;

        return index;
    }

    void release_slot(int slot) {
        nassertv(slot >= 0 && slot < _pool.size());

        _pool[slot]._used = false;
        _unused.push(slot);
        _num_items--;
    }

    void resize(size_t size) {
        
        if (_pool.size() > size) {

            UnusedPoolContainer new_unused;
            size_t num_unused = _unused.size();

            while (!_unused.empty()) {
                int index = _unused.top();
                if (index < size)
                    new_unused.push(index);
                _unused.pop();
            }
            _num_items = std::min(_num_items, size - new_unused.size());
            _unused = new_unused;
        }

        _pool.resize(size);
    }

    size_t get_num_items() const {
        return _num_items;
    }

    T& at(int n) {
        return _pool[n]._value;
    }

    T& operator[](int n) {
        return _pool[n]._value;
    }

    T& operator[](int n) const {
        return _pool[n]._value;
    }

    size_t get_size() const {
        return _pool.size();
    }

    bool is_slot_used(int slot) const {
        nassertr(slot < _pool.size(), false)
        return _pool[slot]._used;
    }

    size_t get_contiguous_size() const {
        return _num_items + _unused.size();
    }

        
private:
    UnusedPoolContainer _unused;
    PoolContainer _pool;
    size_t _num_items;
};