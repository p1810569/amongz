// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#include <pandabase.h>
#include <pandaSystem.h>

#include <graphicsPipeSelection.h>
#include "framework.h"


using namespace std;


Framework::Framework(): 
    _exit_flag(ES_none), 
    _task_mgr(AsyncTaskManager::get_global_ptr())
{

#ifdef LINK_ALL_STATIC
    // If we're statically linking, we need to explicitly link with at least one
    // of the available renderers.
    #if defined(HAVE_GL)
    extern void init_libpandagl();
    init_libpandagl();
    #elif defined(HAVE_DX9)
    extern EXPCL_PANDADX void init_libpandadx9();
    init_libpandadx9();
    #elif defined(HAVE_TINYDISPLAY)
    extern EXPCL_TINYDISPLAY void init_libtinydisplay();
    init_libtinydisplay();
    #endif

    // Ensure the animation subsystem is available.
    extern EXPCL_PANDA_CHAR void init_libchar();
    init_libchar();

    // We also want the egg loader.
    #ifdef HAVE_EGG
    init_libpandaegg();
    #endif
#endif

    make_default_pipe();


    // extern EXPCL_PANDA_PNMIMAGETYPES void init_libpnmimagetypes();
    // init_libpnmimagetypes();
}


GraphicsOutput* Framework::open_window( FrameBufferProperties fb_prop,
                                WindowProperties win_prop, const string& name)
{
    GraphicsEngine* engine = GraphicsEngine::get_global_ptr();

    GraphicsOutput* window = engine->make_output(get_default_pipe(), 
                            name, 50, fb_prop, win_prop, 
                            GraphicsPipe::BF_require_window,
                            nullptr);

    engine->open_windows();

    if (window != nullptr && !window->is_valid()) {
        engine->remove_window(window);
        window = nullptr;
    }

    if (window == nullptr)
        nout << "Unable to create window." << endl;

    return window;
}


Framework::ExitStatus Framework::finish() {
    
    if (_exit_flag == ES_none)
        return ES_success;

    return _exit_flag;
}


void Framework::make_default_pipe() {

    auto selection = GraphicsPipeSelection::get_global_ptr();

    _default_pipe = selection->make_default_pipe();

    if (_default_pipe == nullptr) {
        nout << "No graphics pipe is available!\n"
            << "Your Config.prc file must name at least one valid panda display\n"
            << "library via load-display or aux-display.\n";
    }
}


Framework::~Framework() {

    Thread::prepare_for_exit();
    _default_pipe.clear();
}


void Framework::main_loop() const {

    Thread *current_thread = Thread::get_current_thread();

    while (_exit_flag == ES_none)
        do_frame(current_thread);
}


void Framework::do_frame(Thread* current_thread) const {

    _task_mgr->poll();
}


GraphicsPipe* Framework::get_default_pipe() {
    
    if (_default_pipe == nullptr)
        make_default_pipe();

    return _default_pipe;
}


Framework::ExitStatus Framework::get_exit_status() const {
    
    return _exit_flag;
}


void Framework::set_exit_status(ExitStatus status) {

    _exit_flag = status;
}