// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of projet.
// 
// projet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// projet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with projet.  If not, see <http://www.gnu.org/licenses/>.


#include <asyncTaskManager.h>
#include "callbacks.h"
#include "utils.h"


using namespace std;

DEFINE_TYPEHANDLE(LocalNodePath)


size_t unregister_task(const string& name, void* data) {

	AsyncTaskManager* task_mgr = AsyncTaskManager::get_global_ptr();
    size_t count = 0;

    AsyncTaskCollection tasks = task_mgr->find_tasks(name);

    for (int i=0; i<tasks.get_num_tasks(); ++i) {
        AsyncTask* task = tasks.get_task(i);
            
        if (data != nullptr) {

            if (task->is_of_type(GenericAsyncTask::get_class_type())) {
                if (((GenericAsyncTask*)task)->get_user_data() != data)
                    continue;
            } 
            else
                continue;
        }
        task_mgr->remove(task);
        ++count;
    }

    return count;
}


bool register_hook(EventHandler::EventCallbackFunction* func, 
                void* data, const string& name)
{    
    EventHandler* handler = EventHandler::get_global_event_handler();
    return handler->add_hook(name, func, data);
}


LocalNodePath::LocalNodePath(PandaNode* node): 
    LocalRef<NodePath>(node, node)
{
    
}

void LocalNodePath::do_remove_node() {
    
    remove_node();
    // static_cast<NodePath&>(*this) = NodePath::removed();
}


string get_unique_name(const string& name, pvector<string> names) {

    string new_name = name;
    int i = 0;

    while (find(names.begin(), names.end(), new_name) != names.end()) {
        stringstream ss;
        ss << name << '.' << setfill('0') << setw(4) << i++;
        new_name = ss.str();
    };
    return new_name;
}