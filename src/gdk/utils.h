// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of projet.
// 
// projet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// projet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with projet.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __UTILS_H__
#define __UTILS_H__

#include <genericAsyncTask.h>
#include <eventHandler.h>
#include <functional>
#include <nodePath.h>
#include "assetManager.h"
#include "localRef.h"


#ifdef _WIN32
    #define EXPORT_IMP __declspec(dllexport)
    #define IMPORT_IMP __declspec(dllimport)
#else
    #define EXPORT_IMP __attribute__((visibility("default")))
    #define IMPORT_IMP
#endif

#ifdef PLUGIN
    #define DL_EXPORT IMPORT_IMP
#else
    #define DL_EXPORT EXPORT_IMP
#endif



#define CAT(A, B) A ## B
#define SELECT(NAME, NUM) CAT(NAME ## _, NUM)
#define COMPOSE(NAME, ARGS) NAME ARGS

#define GET_COUNT(_0, _1, _2, _3, _4, _5, _6, COUNT, ...) COUNT
#define EXPAND() ,,,,,,

#define VA_SIZE(...) COMPOSE(GET_COUNT, (EXPAND __VA_ARGS__ (),0,6,5,4,3,2,1))
#define FOR_EACH(M, ...) SELECT(ARGS, VA_SIZE(__VA_ARGS__))(M, __VA_ARGS__)
#define VA_SELECT(M, ...) SELECT(M, VA_SIZE(__VA_ARGS__))(__VA_ARGS__)

#define ARGS_0(M, X)      
#define ARGS_1(M, X)      M(X)
#define ARGS_2(M, X, ...) M(X)ARGS_1(M, __VA_ARGS__)
#define ARGS_3(M, X, ...) M(X)ARGS_2(M, __VA_ARGS__)
#define ARGS_4(M, X, ...) M(X)ARGS_3(M, __VA_ARGS__)
#define ARGS_5(M, X, ...) M(X)ARGS_4(M, __VA_ARGS__)
#define ARGS_6(M, X, ...) M(X)ARGS_5(M, __VA_ARGS__)



#define INIT_TYPE(type) type::init_type();
#define TYPE_CLASS(type) ,type::get_class_type()


#define REGISTER_TYPE_WO_INIT \
public: \
    static TypeHandle get_class_type() { \
        return _type_handle; \
    } \
    virtual TypeHandle get_type() const { \
        return get_class_type(); \
    } \
    virtual TypeHandle force_init_type() { \
        init_type(); \
        return get_class_type(); \
    } \
private: \
    static TypeHandle _type_handle;


#define REGISTER_TYPE_POST_INIT(type, ...) \
public: \
    static void init_type() { \
        FOR_EACH(INIT_TYPE, __VA_ARGS__) \
        register_type(_type_handle, type \
            FOR_EACH(TYPE_CLASS, __VA_ARGS__)); \
        post_init(_type_handle); \
    } \
    REGISTER_TYPE_WO_INIT

#define REGISTER_TYPE(type, ...) \
public: \
    static void init_type() { \
        FOR_EACH(INIT_TYPE, __VA_ARGS__) \
        register_type(_type_handle, type \
            FOR_EACH(TYPE_CLASS, __VA_ARGS__)); \
    } \
    REGISTER_TYPE_WO_INIT


#define DEFINE_TYPEHANDLE(type) TypeHandle type::_type_handle;


size_t unregister_task(const std::string& name, void* data=nullptr);


bool register_hook(EventHandler::EventCallbackFunction* func,
                void* data, const std::string& name);




enum ShortSizePoT {
    SST_1, SST_2, SST_4, SST_8, SST_16, SST_32, SST_64,
    SST_128, SST_256, SST_512, SST_1024, SST_2048, SST_4096,
    SST_8192, SST_16384, SST_32768, SST_65536, SST_invalid,
};


inline size_t as_numeric(ShortSizePoT size) {

    if (size == SST_invalid)
        return 0;

    return 0x1 << size;
}


inline size_t get_floor_size(size_t n, ShortSizePoT size) {
    
    return n >> size;
}


inline size_t get_ceil_size(size_t n, ShortSizePoT size) {

    return get_floor_size(n + as_numeric(size) - 1, size);
}


template<class T>
T keep_lowest_bit_set(T value) {

    return static_cast<T>(value - (value & value - 1));
}


#if __cplusplus < 201703L

template<class T>
T clamp(T value, T min_value, T max_value) {

    return std::min(std::max(min_value, value), max_value);
}

#endif


#if __cplusplus < 202003L

template<class InputIt, class T>
T accumulate(InputIt first, InputIt last, T init)
{
    for (; first != last; ++first) {
        init = std::move(init) + *first; // std::move since C++20
    }
    return init;
}

#endif

inline float saturate(float value) {

    return clamp(value, 0.0f, 1.0f);
}

template<class T>
T lerp(T a, T b, float t) {
    return a + t * (b - a);
}

template<class T>
PT(T) make_asset(const std::string& name) {
    return DCAST(T, AssetManager::make_asset(T::get_class_type(), name));
}

template<class T>
T* get_asset(const std::string& name) {
    return DCAST(T, AssetManager::get_asset(T::get_class_type(), name));
}


inline double hash_d(double seed) {

    double value = sin(seed) * 43758.5453;
    return value - (long)value;
}


inline float smoothstep(float a, float b, float  x) {

    float t = x * x * (3 - 2 * x); 
    return lerp(a, b, t);
}


inline float noise(double t) {

    long a = (long)t;
    long b = a + 1;

    return smoothstep(hash_d(a), hash_d(b), t - a);
}


class LocalNodePath: public LocalRef<NodePath> {

    REGISTER_TYPE("LocalNodePath", NodePath);

public:
    LocalNodePath(PandaNode* node);
    ~LocalNodePath() = default;

    void do_remove_node();
};


template<class T>
class Singleton: public std::unique_ptr<T> {
public:
    Singleton() = default;
    Singleton(const Singleton<T>& copy) = default;
    Singleton(T* ptr): std::unique_ptr<T>(ptr) {}

    ~Singleton() = default;
    
    Singleton<T>& operator =(Singleton<T>&& copy) = default;

    inline Singleton<T>& operator =(T* ptr) {
        std::unique_ptr<T>::reset(ptr);
        return *this;
    }

    inline operator T*() const {
        return std::unique_ptr<T>::get();
    }
};


#endif // __UTILS_H__