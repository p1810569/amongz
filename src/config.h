// Copyright (C) 2021 Matthieu Jacquemet, Riyad Ennouara, Nicolas Lerray
// 
// This file is part of Among Z.
// 
// Among Z is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Among Z is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Among Z.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CONFIG_H__
#define __CONFIG_H__


#define DATA_PATH "data/"
#define GDK_INSTALL_DIR DATA_PATH "gdk/"


#define USE_RENDER_PIPELINE 1
#define BULLET_LIFESPAN 3.0
#define THREADED_PHYSIC 1
#define THREADED_AI 0

#define PRC_PATH DATA_PATH
#ifdef NDEBUG
    #define COMMON_PRC PRC_PATH "release.prc"
#else
    #define COMMON_PRC PRC_PATH "debug.prc"
#endif


#endif // __CONFIG_H__