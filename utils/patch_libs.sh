#!/usr/bin/env bash

FOLDER=$1

for FILE in extern/linux64/*/lib/*.so
do
    NEW_FILE=$(echo $FILE | sed -r 's/\.[0-9]+//g')
    mv $FILE $NEW_FILE 2>/dev/null
done

FILES=$(echo extern/linux64/*/lib/*.so)

for FILE in ${FILES[*]}
do
    DEPS=$(patchelf --print-needed $FILE)

    for DEP in ${DEPS[*]}
    do
        NEW_DEP=$(echo $DEP | sed -r 's/\.[0-9]+//g')
        if echo "$FILES" | grep -q $NEW_DEP
        then
            patchelf --replace-needed $DEP $NEW_DEP $FILE
        fi
    done 
    patchelf --set-soname $(basename $FILE) $FILE
    echo patched $FILE
done

